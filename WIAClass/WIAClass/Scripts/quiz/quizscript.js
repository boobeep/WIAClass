﻿const start_btn = document.querySelector(".start_btn button");
const info_box = document.querySelector(".info_box");
const exit_btn = info_box.querySelector(".buttons .quit");
const continue_btn = info_box.querySelector(".buttons .restart");
const quiz_box = document.querySelector(".quiz_box");
const option_list = document.querySelector(".option_list");
const timeCount = quiz_box.querySelector(".timer .timer_sec");

start_btn.onclick = () => {
    info_box.classList.add("activeInfo");
}

exit_btn.onclick = () => {
    info_box.classList.remove("activeInfo");
}

continue_btn.onclick = () => {
    info_box.classList.remove("activeInfo");
    quiz_box.classList.add("activeQuiz");
    showQuestions(0);
    startTimer(timeValue);
    shuffleQuestions();
}

let que_count = 0;
let que_numb = 1;
let userScore = 0;
let counter;
let timeValue = 15;
let shuffeled = [];

const timer_box = quiz_box.querySelector(".timer");
const next_btn = quiz_box.querySelector(".next_btn");
const finish_box = document.querySelector(".finish_box")
next_btn.onclick = () => {
    nextQuestion();
}

function showQuestions(index) {
    const que_text = document.querySelector(".que_text");
    const que_number = document.querySelector(".que_number");
    let que_num = '<span> No. ' + que_numb + '</span>';
    let que_tag = '<span>' + questions[index].question + '</span>';
    let option_tag = '<div class="option">' + questions[index].options[0] + '</div >' +
        '<div class="option">' + questions[index].options[1] + '</div >' +
        '<div class="option">' + questions[index].options[2] + '</div >' +
        '<div class="option">' + questions[index].options[3] + '</div >';
    que_number.innerHTML = que_num;
    que_text.innerHTML = que_tag;
    option_list.innerHTML = option_tag;

    const option = option_list.querySelectorAll(".option");
    for (let i = 0; i < option.length; i++) {
        option[i].setAttribute("onclick", "optionSelected(this)");
    }
}

let tickIcon = '<div class="icon tick"><i class="fas fa-check"></i></div>';
let crossIcon = '<div class="icon cross"><i class="fas fa-times"></i></div>';

function optionSelected(answer) {
    clearInterval(counter);
    let userAns = answer.textContent; 
    let correctAns = questions[que_count].answer;
    let allOptions = option_list.children.length;
    if (userAns == correctAns) {
        userScore += 1;
        console.log(userScore);
        answer.classList.add("correct");
        console.log("Answer is Correct");
        answer.insertAdjacentHTML("beforeend", tickIcon);
    } else {
        answer.classList.add("wrong");
        console.log("Answer is Wrong");
        answer.insertAdjacentHTML("beforeend", crossIcon);

        for (let i = 0; i < allOptions; i++) {
            if (option_list.children[i].textContent == correctAns) {
                option_list.children[i].setAttribute("class", "option correct");
                option_list.children[i].insertAdjacentHTML("beforeend", tickIcon);
            }
        }
    }

    for (let i = 0; i < allOptions; i++) {
        option_list.children[i].classList.add("disabled");
    }
    next_btn.style.display = "block";
}

function showFinishBox() {
    info_box.classList.remove("activeInfo");
    quiz_box.classList.remove("activeQuiz");
    finish_box.classList.add("actionFinish");
}

function startTimer(time) {
    counter = setInterval(timer, 1000);
    function timer() {
        timeCount.style.color = "#fff";
        timeCount.textContent = time;
        time--;
        if (time < 9) {
            let addZero = timeCount.textContent;
            timeCount.textContent = "0" + addZero;
            timeCount.style.color = "red";
        }
        if (time < 0) {
            clearInterval(counter);
            timeCount.textContent = "00";
            nextQuestion();
        }
    }
}

function nextQuestion() {
    if (que_count < questions.length - 1) {
        que_count++;
        que_numb++;
        const questionIndex = Math.floor(Math.random() * shuffeled.length);
        const removeQ = shuffeled.indexOf(questionIndex);
        if (removeQ > -1) {
            shuffeled.splice(removeQ, 1);
        }
        //showQuestions(questionIndex);
        showQuestions(que_count);
        //queCounter(que_numb);
        clearInterval(counter);
        startTimer(timeValue);
        next_btn.style.display = "none";
    } else {
        console.log("Questions completed");
        showFinishBox();
    }
}

function shuffleQuestions() {
    for (var i = 0; i < questions.length; i++) {
        shuffeled.push[i];
    }
}