﻿let questions = [
    {
        numb: 1,
        question: "2 + 2 = ?",
        options: [
            "1",
            "2",
            "3",
            "4"
        ],
        answer: "4" 
    },
    {
        numb: 2,
        question: "3 + 4 x 2 = ?",
        options: [
            "11",
            "12",
            "13",
            "14"
        ],
        answer: "11"
    },
    {
        numb: 3,
        question: "(3 + 5) x 2 = ?",
        options: [
            "11",
            "13",
            "16",
            "19"
        ],
        answer: "16"
    },
    {
        numb: 4,
        question: "4 x 3 + 2 / 2  = ?",
        options: [
            "7",
            "9",
            "11",
            "13"
        ],
        answer: "13"
    },
    {
        numb: 5,
        question: "5 x (2 + 4) / 2  = ?",
        options: [
            "7",
            "12",
            "15",
            "20"
        ],
        answer: "15"
    },
];