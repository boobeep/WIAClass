﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WIAClass.Models;

namespace WIAClass.Controllers
{
    public class LoginController : Controller
    {
        WiaClassEntities db = new WiaClassEntities();
        // GET: Login

        [HttpGet]
        public ActionResult Index()
        {
            TempData["notif"] = "0";
            return View();
        }

        // [Creating hashed password that combined with salt (method)]
        public static class Crypto
        {
            public static string Hash(string value)
            {
                return Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value)));
            }
        }
        //function to check if User is valid or not
        public Vw_User IsValidUser(LoginViewModel model)
        {
            using (db)
            {
                string modelPass = Crypto.Hash(model.Password);
                //Retireving the user details from DB based on username and password enetered by user.
                var user = db.Vw_User.Where(query => query.Email.Equals(model.Email) && query.Password.Equals(modelPass)).FirstOrDefault(); // ini admin sekolah
      
                //If user is present, then true is returned.
                if (user != null)
                {
                    Session["RoleId"] = user.Role;
                    Session["userId"] = user.FK_ID;
                    Session["email"] = user.Email;
                    Session["nama"] = user.Nama;
                    Session["image"] = user.Foto_Profil;
                    Session["sekolahId"] = user.Id_Sekolah;
                    return user;
                }

                //If user is not present false is returned.
                else
                    return null;
            }
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var isValidUser = IsValidUser(model);

                try
                {
                    //If user is valid & present in database, we are redirecting it to Welcome page.
                    if (isValidUser != null && isValidUser.Activated == true && isValidUser.Role == 1)
                    {
                        FormsAuthentication.SetAuthCookie(model.Email, false);
                        ViewBag.Message = "Login sukses";
                        return RedirectToAction("Index", "SA");
                    }
                    else if (isValidUser != null && isValidUser.Activated == true && isValidUser.Role == 2)
                    {
                        FormsAuthentication.SetAuthCookie(model.Email, false);
                        ViewBag.Message = "Login sukses";
                        return RedirectToAction("Index", "Admin");
                    }
                    else if (isValidUser != null && isValidUser.Activated == true && isValidUser.Role == 3)
                    {
                        FormsAuthentication.SetAuthCookie(model.Email, false);
                        ViewBag.Message = "Login sukses";
                        return RedirectToAction("Index", "Guru");
                    }
                    else if (isValidUser != null && isValidUser.Activated == true && isValidUser.Role == 4)
                    {
                        FormsAuthentication.SetAuthCookie(model.Email, false);
                        ViewBag.Message = "Login sukses";
                        return RedirectToAction("Index", "Siswa");
                    }
                    else if (isValidUser != null && isValidUser.Activated == false)
                    {
                        ViewBag.Message = "Maaf, login gagal! Aktivasi akun Anda terlebih dahulu";
                        TempData["notif"] = "0";
                        TempData.Keep();
                        return View();
                    }
                    else
                    {
                        //If the username and password combination is not present in DB then error message is shown.
                        ModelState.AddModelError("Gagal", "Salah Username atau Password!");
                        ViewBag.Message = "Maaf, login gagal. Username dan Password tidak cocok!";
                        TempData["notif"] = "0";
                        TempData.Keep();
                        return View();
                    }
                }
                catch (Exception msg)
                {
                    var Error_msg = msg;
                    string Error_action = "Login";
                    string Error_section = "User Login";
                    string User_login = model.Email;
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db.Entry(AuditTrail).State = EntityState.Added;
                    db.SaveChanges();
                    ViewBag.Message = msg;
                    return View();
                }
            }
            else
            {
                //If model state is not valid, the model with error message is returned to the View.
                ModelState.AddModelError("Gagal", "Salah Username atau Password!");
                ViewBag.Message = "Maaf, login gagal. Terjadi kesalahan";
                TempData["notif"] = "0";
                TempData.Keep();
                return View(model);
            }
        }
     
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            TempData["FP_notif"] = "0";
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string EmailID)
        {
            string resetCode = Guid.NewGuid().ToString();
            var verifyUrl = Url.Content("~/Login/ResetPassword/") + resetCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            using (var context = new WiaClassEntities())
            {
                var getUser = (from s in context.Tbl_Sekolah where s.Email == EmailID select s).FirstOrDefault();
                if (getUser != null)
                {
                    getUser.ResetPasswordCode = resetCode;

                    context.Configuration.ValidateOnSaveEnabled = false;
                    context.SaveChanges();

                    var subject = "Permintaan Reset Password";
                    var body = "Halo " + getUser.Nama_Sekolah + ", <br/> Anda baru-baru ini meminta untuk menyetel ulang password untuk akun Anda. Klik link di bawah untuk mengatur ulang password." +

                         " <br/><br/><a href='" + link + "'>" + link + "</a> <br/><br/>" +
                         "Jika Anda tidak meminta penyetelan ulang password, abaikan email ini atau balas untuk memberi tahu kami.<br/><br/> Terima kasih";

                    SendEmail(getUser.Email, body, subject);

                    ViewBag.Message = "Email sudah dikirim, silahkan cek email Anda";
                    TempData["FP_notif"] = "1";
                    TempData.Keep();
                }
                else
                {
                    ViewBag.Message = "Maaf, terjadi kesalahan";
                    TempData["FP_notif"] = "0";
                    TempData.Keep();
                    return View();
                }
            }

            return View();
        }

        private void SendEmail(string emailAddress, string body, string subject)
        {
            var toMail = new MailAddress(emailAddress);
            var fromMail = new MailAddress("wiaclass@gmail.com", "WIA Class");
            var wiaEmail = ConfigurationManager.AppSettings["wiaEmail"];
            var wiaPassword = ConfigurationManager.AppSettings["wiaPassword"];
            using (MailMessage mm = new MailMessage(fromMail, toMail))
            {
                mm.Subject = subject;
                mm.Body = body;

                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    EnableSsl = true,
                    //NetworkCredential NetworkCred = new NetworkCredential("youremail@gmail.com", "YourPassword");
                    UseDefaultCredentials = true,
                    Credentials = new NetworkCredential(wiaEmail, wiaPassword),
                    Port = 587
                };
                smtp.Send(mm);

            }
        }

        public ActionResult ResetPassword(string id)
        {
            //Verify the reset password link
            //Find account associated with this link
            //redirect to reset password page
            TempData["FP_notif"] = "0";

            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpNotFound();
            }

            using (var context = new WiaClassEntities())
            {
                var user = context.Tbl_Sekolah.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return View("~/Views/Home/NotFound.cshtml");
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            
            var message = "";
            if (ModelState.IsValid)
            {
                using (var context = new WiaClassEntities())
                {
                    var user = context.Tbl_Sekolah.Where(a => a.ResetPasswordCode == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        //you can encrypt password here, we are not doing it
                        string modelPass = Crypto.Hash(model.NewPassword);
                        user.Password = modelPass;
                        //make resetpasswordcode empty string now
                        user.ResetPasswordCode = "";
                        //to avoid validation issues, disable it
                        context.Configuration.ValidateOnSaveEnabled = false;
                        context.SaveChanges();
                        message = "Password berhasil diperbarui!";
                        TempData["FP_notif"] = "1";
                        TempData.Keep();
                    }
                }
            }
            else
            {
                message = "Error";
                TempData["FP_notif"] = "0";
                TempData.Keep();
            }
            ViewBag.Message = message;
            return View(model);
        }
    }
}