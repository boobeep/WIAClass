﻿using WIAClass.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Web.Security;
using System.Net.Mail;
using System.Configuration;
using System.Threading.Tasks;
using System.Text;

namespace WIAClass.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        // GET: LandingPage
        WiaClassEntities db = new WiaClassEntities();
        public ActionResult Index()
        {
            List<Tbl_Testimonial> testimoni = db.Tbl_Testimonial.Where(x => x.Status == true).ToList();
            List<Tbl_FAQ> faq = db.Tbl_FAQ.Where(x => x.Status == true).ToList();
            ViewData["listTestimoni"] = testimoni;
            ViewData["listFAQ"] = faq;

            return View();
        }
        
        public ActionResult UnAuthorized()
        {
            return View();
        }

        [HttpPost]
        public JsonResult KontakKami(KontakKamiModel kontak)
        {
            using (var transaction = db.Database.BeginTransaction())
            try
            {
                Tbl_Kontak_Kami tbl_kontak = new Tbl_Kontak_Kami()
                {
                    Nama = kontak.Nama,
                    Email = kontak.Email.ToLower(),
                    Subjek = kontak.Subjek,
                    Pesan = kontak.Pesan,
                    Waktu = DateTime.Now
                };

                db.Entry(tbl_kontak).State = EntityState.Added;
                db.SaveChanges();
                return Json("Pesan anda telah terkirim, terima kasih atas partisipasinya");
            }
            catch(Exception msg)
            {
                transaction.Rollback();
                WiaClassEntities db2 = new WiaClassEntities();

                var Error_msg = msg;
                string Error_action = "Belum ada action";
                string Error_section = "Feedback user";
                string User_login = "Hardcode user";
                Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                {
                    Error_msg = Error_msg.ToString(),
                    User_login = User_login,
                    Error_date = DateTime.Now,
                    Error_action = Error_action,
                    Error_section = Error_section
                };
                db2.Entry(AuditTrail).State = EntityState.Added;
                db2.SaveChanges();
                return Json("Terjadi kesalahan saat mengirim pesan");
            }
        }
    }
}
