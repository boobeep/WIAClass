﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WIAClass.Infrastructure;
using WIAClass.Models;

namespace WIAClass.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    [CustomAuthorize("3")]
    public class GuruController : Controller
    {
        WiaClassEntities db = new WiaClassEntities();
        // GET: Guru
        public ActionResult Index()
        {
            int sessionSekolahId = (int)Session["sekolahId"];
            int sessionGuruId = (int)Session["userId"];
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            Session["absenGuru"] = "checkout";
            

            var jekel = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == sessionGuruId).FirstOrDefault();
            if (jekel.Fk_Id_Jenis_Kelamin == 1)
            {
                ViewBag.DataJekel = "Bapak";
            }
            else if (jekel.Fk_Id_Jenis_Kelamin == 2)
            {
                ViewBag.DataJekel = "Ibu";
            }

            var header1 = (string)Session["nama"];
            ViewBag.NamaGuru = header1;
            var konfirmKehadiran = db.Tbl_Absensi.Where(x => x.Fk_Id_Sekolah == sessionSekolahId && x.Status_Absensi == false && x.Fk_Id_Guru != null).ToList();
            ViewBag.DataHeaderKonfirm = konfirmKehadiran.Count;

            var viewAdmin1 = db.Vw_DashboardGuru1.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).ToList();
            if (viewAdmin1 != null)
            {
                List<DataPointGuru1> dataPointGuru1 = new List<DataPointGuru1>();
                for (int i = 0; i < viewAdmin1.Count; i++)
                {
                    dataPointGuru1.Add(new DataPointGuru1(viewAdmin1[i].Nama_Kelas, Convert.ToDouble(viewAdmin1[i].Rata_rata)));
                }

                ViewBag.DataPoints1 = JsonConvert.SerializeObject(dataPointGuru1);
            }

            var viewAdmin2 = db.Vw_DashboardAdmin4.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).OrderByDescending(c => c.Nama_Kelas).ToList();
            List<DataPoint2> dataPoints2 = new List<DataPoint2>();
            for (int i = 0; i < viewAdmin2.Count; i++)
            {
                dataPoints2.Add(new DataPoint2(viewAdmin2[i].Nama_Kelas, Convert.ToDouble(viewAdmin2[i].Jumlah_Siswa)));
            }

            ViewBag.DataPoints2 = JsonConvert.SerializeObject(dataPoints2);

            var userid = Convert.ToInt32(Session["userId"]);
            var statAbsen = db.Tbl_Absensi.Where(x => x.Fk_Id_Guru == userid).OrderByDescending(x => x.Pk_Id_Absensi).FirstOrDefault();
            if (statAbsen != null)
            {
                if (statAbsen.Status == "Hadir")
                {
                    ViewBag.StatusAbsen = "Sudah";
                }
                else
                {
                    ViewBag.StatusAbsen = "Belum";
                }
            }
            var idguru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
            var tugas = db.Vw_Tugas.Where(x => x.Nama_Guru == idguru.Nama).ToList();
            var header2 = db.Vw_DashboardAdmin.Where(x => x.Pk_Id_Sekolah == sessionSekolahId).FirstOrDefault();
            ViewBag.DataHeader = header2;

            ViewBag.JumlahTugas = tugas.Count;

            return View();
        }

        public ActionResult Profil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var guru = db.Vw_Data_Guru.Where(x => x.Pk_Id_Guru == profilId).FirstOrDefault();
            return View(guru);
        }

        public ActionResult EditProfil()
        {
            int sessionSekolahId = (int)Session["sekolahId"];

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            try
            {
                List<Tbl_Mata_Pelajaran> mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).ToList();
                ViewData["listMapel"] = mapel;
            }
            catch (Exception)
            {

                ViewData["listMapel"] = "";
            }

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == profilId).FirstOrDefault();
            var viewModel = new ProfileModelGuru()
            {
                Pk_Id_Guru = guru.Pk_Id_Guru,
                NIP = guru.NIP,
                Nama = guru.Nama,
                Foto_Profil = guru.Foto_Profil,
                No_Telp = guru.No_Telp,
                Email = guru.Email,
                Fk_Id_Sekolah = guru.Fk_Id_Sekolah,
                Fk_Id_Mata_Pelajaran = guru.Fk_Id_Mata_Pelajaran
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditProfil(CrudGuruModel modelEditGuru)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == modelEditGuru.Pk_Id_Guru).FirstOrDefault();
                    guru.NIP = modelEditGuru.NIP;
                    guru.Nama = modelEditGuru.Nama;
                    guru.No_Telp = modelEditGuru.No_Telp;
                    guru.Email = modelEditGuru.Email;
                    guru.Fk_Id_Mata_Pelajaran = modelEditGuru.Fk_Id_Mata_Pelajaran;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Sekolah";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        [HttpPost]
        public ActionResult EditFotoProfil(editFotoProfilModel model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var oldPicture = model.oldPicture;
                var newPicture = (Session["userId"] + "_" + file.FileName);
                try
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    file.SaveAs(Server.MapPath("~/Upload/profile/" + Session["userId"] + "_" + file.FileName));
                    //Delete old Picture
                    if (newPicture != oldPicture)
                    {
                        string oldPictPath = Request.MapPath("~/Upload/profile/" + oldPicture);
                        if (System.IO.File.Exists(oldPictPath))
                        {
                            System.IO.File.Delete(oldPictPath);
                        }
                    }

                    int profilId = Convert.ToInt32(Session["userId"].ToString());
                    var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == profilId).FirstOrDefault();
                    guru.Foto_Profil = (Session["userId"] + "_" + file.FileName);
                    db.SaveChanges();
                    transaction.Commit();
                    Session["image"] = guru.Foto_Profil;
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Foto Profil";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                }
                return View();
            }
        }

        public ActionResult Testimonial()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }

        [HttpPost]
        public ActionResult Testimonial(cmsTestimonial test)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var sessionEmail = ((string)Session["email"]);
                    var namaGuru = db.Tbl_Guru.Where(x => x.Email == sessionEmail).FirstOrDefault();
                    Tbl_Testimonial testi = new Tbl_Testimonial
                    {
                        Nama = namaGuru.Nama,
                        Posisi_Role = "Guru",
                        Testimoni = test.Testimoni,
                        Status = false,
                        Foto = (string)Session["image"]
                    };
                    db.Entry(testi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Testimonial");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        public ActionResult Video()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }

        public ActionResult Kehadiran()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }

        public ActionResult JadwalKelas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }

        [HttpGet]
        public ActionResult BeriTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            var userId = (int)Session["userId"];
            var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == userId).FirstOrDefault();
            List<Tbl_Kelas> kls = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == guru.Fk_Id_Sekolah).ToList();
            ViewBag.list = new SelectList(kls, "Pk_Id_Kelas", "Nama_Kelas");
            return View();
        }

        [HttpGet]
        public ActionResult MenilaiTugas(int? id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            var tugas = db.Tbl_Tugas.Where(x => x.Id_Tugas == id).FirstOrDefault();
            
            if (tugas == null || id == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                Session["tugasId"] = tugas.Id_Tugas;
                var kumpul = db.Tbl_Pengumpulan_Tugas.Where(x => x.Fk_Id_Tugas == id).ToList();
                if (kumpul == null)
                {
                    return View("~/Views/Home/NotFound.cshtml");
                }
                else
                {
                    Session["namaTugas"] = tugas.Nama_Tugas;
                    Session["waktuTugas"] = tugas.Batas_Pengumpulan;
                    return View();
                }
            }
        }

        /*public ActionResult BeriQuiz()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }*/

        [HttpGet]
        public ActionResult TambahKategori()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            var userId = (int)Session["userId"];
            var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == userId).FirstOrDefault();
            List<Tbl_Kelas> kls = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == guru.Fk_Id_Sekolah).ToList();
            ViewBag.list = new SelectList(kls, "Pk_Id_Kelas", "Nama_Kelas");

            /* var userId = (int)Session["userId"];
             int guruid = Convert.ToInt32(Session["userId"].ToString());
             List<Tbl_Kategori> li = db.Tbl_Kategori.Where(x => x.Fk_Id_Guru == guruid).OrderByDescending(x => x.Pk_Id_Kategori).ToList();
             ViewData["list"] = li;*/
            return View();
        }

        public JsonResult DataKategori()
        {

            db.Configuration.ProxyCreationEnabled = false;
            var userId = (int)Session["userId"];
            //TempData["sidebar"] = "SidebarGuru";
            int guruid = Convert.ToInt32(Session["userId"].ToString());
            var kategori = db.Vw_KategoriQuiz.Where(x => x.IDGuru == guruid).ToList();
            return Json(new { data = kategori }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetKategoriID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditKategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori.Equals(ID)).FirstOrDefault();
            return Json(EditKategori, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TambahDataKategori(CrudQuizModel modelKategori)
        {
            var userId = (int)Session["userId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Kategori tbl_kategori = new Tbl_Kategori();

                    tbl_kategori.KatName = modelKategori.KatName;
                    tbl_kategori.Fk_Id_Guru = userId;
                    tbl_kategori.Fk_Id_Kelas = modelKategori.Fk_Id_Kelas;
                    tbl_kategori.StatusQuiz = modelKategori.StatusQuiz;
                    tbl_kategori.MulaiQuiz = modelKategori.MulaiQuiz;
                    tbl_kategori.BatasQuiz = modelKategori.BatasQuiz;
                    tbl_kategori.WaktuPengerjaan = modelKategori.WaktuPengerjaan;

                    db.Tbl_Kategori.Add(tbl_kategori);
                    db.Entry(tbl_kategori).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("TambahKategori");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Kategori";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        [HttpPost]
        public ActionResult DeleteKategori(int id, Tbl_Kategori kategori)
        {
            try
            {
                kategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == id).FirstOrDefault();
                var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == id);

                db.Tbl_Pertanyaan.RemoveRange(pertanyaan);
                db.Tbl_Kategori.Remove(kategori);
                db.SaveChanges();
                return RedirectToAction("TambahKategori");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult EditKategori(CrudQuizModel modelEditKategori)
        {
            var userId = (int)Session["userId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var kategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == modelEditKategori.Pk_Id_Kategori).FirstOrDefault();
                    kategori.KatName = modelEditKategori.KatName;
                    kategori.Fk_Id_Guru = userId;
                    kategori.Fk_Id_Kelas = modelEditKategori.Fk_Id_Kelas;
                    kategori.StatusQuiz = modelEditKategori.StatusQuiz;
                    kategori.MulaiQuiz = modelEditKategori.MulaiQuiz;
                    kategori.BatasQuiz = modelEditKategori.BatasQuiz;
                    kategori.WaktuPengerjaan = modelEditKategori.WaktuPengerjaan;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("TambahKategori");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Kategori";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        [HttpGet]
        public ActionResult ViewAllQuestions(int id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            var kategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == id).FirstOrDefault();
            if (kategori == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                Session["katId"] = kategori.Pk_Id_Kategori;
                var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == id).ToList();
                if (pertanyaan == null)
                {
                    return View("~/Views/Home/NotFound.cshtml");
                }
                else
                {
                    return View();
                }
            }
        }

        public JsonResult DataPertanyaan()
        {

            db.Configuration.ProxyCreationEnabled = false;
            var katId = (int)Session["katId"];
            //TempData["sidebar"] = "SidebarGuru";
            //int guruid = Convert.ToInt32(Session["userId"].ToString());
            var kategori = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == katId).ToList();
            return Json(new { data = kategori }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPertanyaanID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditPertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan.Equals(ID)).FirstOrDefault();
            return Json(EditPertanyaan, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TambahDataPertanyaan(CrudQuestionModel modelPertanyaan)
        {
            var katId = (int)Session["katId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Pertanyaan tbl_pertanyaan = new Tbl_Pertanyaan();

                    tbl_pertanyaan.Pertanyaan = modelPertanyaan.Pertanyaan;
                    tbl_pertanyaan.OptA = modelPertanyaan.OptA;
                    tbl_pertanyaan.OptB = modelPertanyaan.OptB;
                    tbl_pertanyaan.OptC = modelPertanyaan.OptC;
                    tbl_pertanyaan.OptD = modelPertanyaan.OptD;
                    tbl_pertanyaan.CorrectOpt = modelPertanyaan.CorrectOpt;
                    tbl_pertanyaan.Fk_Id_Kategori = katId;

                    db.Tbl_Pertanyaan.Add(tbl_pertanyaan);
                    db.Entry(tbl_pertanyaan).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("ViewAllQuestions", new { id = katId });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Kategori";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Tbl_AuditTrail.Add(AuditTrail);
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        [HttpPost]
        public ActionResult DeletePertanyaan(int id, Tbl_Pertanyaan pertanyaan)
        {
            try
            {
                pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan == id).FirstOrDefault();
                db.Tbl_Pertanyaan.Remove(pertanyaan);
                db.SaveChanges();
                return RedirectToAction("TambahKategori");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult EditPertanyaan(CrudQuestionModel modelEditPertanyaan)
        {
            var katId = (int)Session["katId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan == modelEditPertanyaan.Pk_Id_Pertanyaan).FirstOrDefault();
                    pertanyaan.Pertanyaan = modelEditPertanyaan.Pertanyaan;
                    pertanyaan.OptA = modelEditPertanyaan.OptA;
                    pertanyaan.OptB = modelEditPertanyaan.OptB;
                    pertanyaan.OptC = modelEditPertanyaan.OptC;
                    pertanyaan.OptD = modelEditPertanyaan.OptD;
                    pertanyaan.CorrectOpt = modelEditPertanyaan.CorrectOpt;
                    pertanyaan.Fk_Id_Kategori = katId;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("ViewAllQuestions", new { id = katId });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Kategori";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Tbl_AuditTrail.Add(AuditTrail);
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        public JsonResult DataTugas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idguru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
            var tugas = db.Vw_Tugas.Where(x => x.Nama_Guru == idguru.Nama).ToList();
            return Json(new { data = tugas }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetTugasID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditTugas = db.Tbl_Tugas.Where(x => x.Id_Tugas.Equals(ID)).FirstOrDefault();
            return Json(EditTugas, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult TambahTugas(CrudBeriTugas modelTugas)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Tugas tugas = new Tbl_Tugas();
                    tugas.Nama_Tugas = modelTugas.Nama_Tugas;
                    tugas.Waktu_Pemberian = modelTugas.Waktu_Pemberian;
                    tugas.Batas_Pengumpulan = modelTugas.Batas_Pengumpulan;
                    tugas.Fk_Id_Kelas = modelTugas.Fk_Id_Kelas;
                    tugas.Fk_Id_Guru = Convert.ToInt32(Session["userId"]);
                    db.Tbl_Tugas.Add(tugas);
                    db.Entry(tugas).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("BeriTugas");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Tugas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
            }
        }



        [HttpPost]
        public ActionResult DeleteTugas(int id, Tbl_Tugas tugas)
        {
            try
            {
                // TODO: Add delete logic here
                tugas = db.Tbl_Tugas.Where(x => x.Id_Tugas == id).FirstOrDefault();
                db.Tbl_Tugas.Remove(tugas);
                db.SaveChanges();
                return RedirectToAction("BeriTugas");
            }
            catch
            {
                return View();
            }
        }



        [HttpPost]
        public ActionResult EditTugas(CrudBeriTugas modelEditTugas)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    int userid = Convert.ToInt32(Session["userId"]);
                    var tugas = db.Tbl_Tugas.Where(x => x.Id_Tugas == modelEditTugas.Id_Tugas).FirstOrDefault();
                    tugas.Nama_Tugas = modelEditTugas.Nama_Tugas;
                    tugas.Waktu_Pemberian = modelEditTugas.Waktu_Pemberian;
                    tugas.Batas_Pengumpulan = modelEditTugas.Batas_Pengumpulan;
                    tugas.Fk_Id_Kelas = modelEditTugas.Fk_Id_Kelas;
                    tugas.Fk_Id_Guru = userid;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("BeriTugas");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Tugas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        //[HttpPost]
        //public ActionResult MemberiNilaiHarian(CrudBeriTugas model, int id_tugas)
        //{
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        int userid = Convert.ToInt32(Session["userId"]);
        //        var guruid = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
        //        var idSekolah = Convert.ToInt32(guruid.Fk_Id_Sekolah);
        //        var mapel = guruid.Fk_Id_Mata_Pelajaran.ToString();
        //        var siswa = db.Tbl_Siswa.Where(x => x.Fk_Id_Sekolah == (idSekolah)).ToList();

        //        List<int?> li_siswa = db.Tbl_Siswa.Select(x => x.Fk_Id_Sekolah).ToList();
        //        foreach (int? i in siswa)
        //        {

        //        }

        //        try
        //        {
        //            Tbl_Nilai tbl_nilai = new Tbl_Nilai();
        //            tbl_nilai.Nilai = model.Nilai;
        //            tbl_nilai.Status = model.Status;
        //            tbl_nilai.Fk_Id_Sekolah = idSekolah;
        //            tbl_nilai.Fk_Id_Siswa = siswa;
        //            tbl_nilai.Fk_Id_Mata_Pelajaran = mapel;
        //            tbl_nilai.Fk_Id_Guru = userid;
        //            tbl_nilai.Fk_Id_Tugas = id_tugas;

        //        }
        //        catch (Exception msg)
        //        {
        //            return View(msg);
        //        }
        //    }
        //}

        public ActionResult Evaluasi()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }

        public JsonResult DataTugasKumpul()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var tugasId = (int)Session["tugasId"];
            var tugas = db.Vw_TugasKumpul.Where(x => x.Id_Tugas == tugasId && x.Status_Nilai == false).ToList();
            return Json(new { data = tugas }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadFile(int id)
        {
            var idkumpul = db.Tbl_Pengumpulan_Tugas.Where(x => x.Pk_Id_Pengumpulan_Tugas == id).FirstOrDefault();
            string FileVirtualPath = "~/Upload/" + idkumpul.Url_File;
            return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
        }

        [HttpPost]
        public JsonResult InputNilai(CrudBeriNilai modelNilai)
        {
            var userId = (int)Session["userId"];
            var sekolah = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == userId).FirstOrDefault();
            var idTugas = modelNilai.Temporary;
            var tugas = db.Tbl_Pengumpulan_Tugas.Where(x => x.Pk_Id_Pengumpulan_Tugas == idTugas).FirstOrDefault();
            using (var transaction = db.Database.BeginTransaction())
            try
            {
                Tbl_Nilai nilai = new Tbl_Nilai();

                nilai.Nilai = modelNilai.Nilai;
                nilai.Status = modelNilai.Status;
                nilai.Eval_Per_Hari = modelNilai.Cat;
                nilai.Fk_Id_Sekolah = sekolah.Fk_Id_Sekolah;
                nilai.Fk_Id_Siswa = tugas.Fk_Id_Siswa;
                nilai.Fk_Id_Mata_Pelajaran = sekolah.Fk_Id_Mata_Pelajaran;
                nilai.Fk_Id_Guru = sekolah.Pk_Id_Guru;
                nilai.Fk_Id_Tugas = tugas.Fk_Id_Tugas;
                nilai.Fk_Id_Pengumpulan_Tugas = tugas.Pk_Id_Pengumpulan_Tugas;
                nilai.Fk_Id_Kelas = tugas.Fk_Id_Kelas;
                

                db.Tbl_Nilai.Add(nilai);
                db.Entry(nilai).State = EntityState.Added;
                db.SaveChanges();
                transaction.Commit();
                
                try
                {
                    var statusTugas = db.Tbl_Pengumpulan_Tugas.Where(x => x.Pk_Id_Pengumpulan_Tugas == nilai.Fk_Id_Pengumpulan_Tugas).FirstOrDefault();

                    statusTugas.Status_Nilai = true;
                    db.SaveChanges();
                    return Json("MenilaiTugas");
                }
                catch (Exception msg)
                {

                        transaction.Rollback();
                        WiaClassEntities db2 = new WiaClassEntities();

                        var Error_msg = msg;
                        string Error_action = "Belum ada action";
                        string Error_section = "Rubah status kumpul tugas";
                        string User_login = "Hardcode user";
                        Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                        {
                            Error_msg = Error_msg.ToString(),
                            User_login = User_login,
                            Error_date = DateTime.Now,
                            Error_action = Error_action,
                            Error_section = Error_section
                        };
                        db2.Entry(AuditTrail).State = EntityState.Added;
                        db2.SaveChanges();
                        ViewBag.Message = msg;
                        return Json(msg);
                }
            }
            catch (Exception msg)
            {
                transaction.Rollback();
                WiaClassEntities db2 = new WiaClassEntities();

                var Error_msg = msg;
                string Error_action = "Belum ada action";
                string Error_section = "Tambah data Nilai";
                string User_login = "Hardcode user";
                Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                {
                    Error_msg = Error_msg.ToString(),
                    User_login = User_login,
                    Error_date = DateTime.Now,
                    Error_action = Error_action,
                    Error_section = Error_section
                };
                db2.Entry(AuditTrail).State = EntityState.Added;
                db2.SaveChanges();
                ViewBag.Message = msg;
                return Json(msg);
            }
        }

        [HttpPost]
        public ActionResult AbsenGuruHadir()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var userid = Convert.ToInt32(Session["userId"]);
                var idGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
                int idSekolah = Convert.ToInt32(idGuru.Fk_Id_Sekolah);
                int idMapel = Convert.ToInt32(idGuru.Fk_Id_Mata_Pelajaran);

                try
                {
                    Tbl_Absensi tbl_absensi = new Tbl_Absensi();
                    tbl_absensi.Tanggal = DateTime.Now;
                    tbl_absensi.Status = "Hadir";
                    tbl_absensi.Fk_Id_Sekolah = idSekolah;
                    tbl_absensi.Fk_Id_Siswa = null;
                    tbl_absensi.Fk_Id_Mata_Pelajaran = idMapel;
                    tbl_absensi.Fk_Id_Guru = userid;
                    tbl_absensi.Status_Absensi = true;

                    db.Tbl_Absensi.Add(tbl_absensi);
                    db.Entry(tbl_absensi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    Session["absenGuru"] = "checkin";
                    Session["absenGuru"] = Session["absenGuru"].ToString();

                    return RedirectToAction("Kehadiran");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Absen Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return View(msg);
                }
            }
        }

        [HttpPost]
        public ActionResult AbsenGuruPulang()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var userid = Convert.ToInt32(Session["userId"]);
                var idGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
                int idSekolah = Convert.ToInt32(idGuru.Fk_Id_Sekolah);
                int idMapel = Convert.ToInt32(idGuru.Fk_Id_Mata_Pelajaran);

                try
                {
                    Tbl_Absensi tbl_absensi = new Tbl_Absensi();
                    tbl_absensi.Tanggal = DateTime.Now;
                    tbl_absensi.Status = "Pulang";
                    tbl_absensi.Fk_Id_Sekolah = idSekolah;
                    tbl_absensi.Fk_Id_Siswa = null;
                    tbl_absensi.Fk_Id_Mata_Pelajaran = idMapel;
                    tbl_absensi.Fk_Id_Guru = userid;
                    tbl_absensi.Status_Absensi = true;

                    db.Tbl_Absensi.Add(tbl_absensi);
                    db.Entry(tbl_absensi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    Session["absenGuru"] = "checkout";
                    Session["absenGuru"] = Session["absenGuru"].ToString();

                    return RedirectToAction("Kehadiran");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Absensi Pulang";
                    string Error_section = "Absensi Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return View(msg);
                }
            }
        }

        public JsonResult DataKehadiranGuru()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var namaGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
            var kehadiran = db.Vw_KehadiranGuru.Where(x => x.Nama_Guru.Equals(namaGuru.Nama) && x.Status_Absensi == true).ToList();
            return Json(new { data = kehadiran }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public ActionResult RequestApprovalGuru(RequestApprovalModel model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var userid = Convert.ToInt32(Session["userId"]);
                var idGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
                int idSekolah = Convert.ToInt32(idGuru.Fk_Id_Sekolah);
                var idMapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran == idGuru.Fk_Id_Mata_Pelajaran).FirstOrDefault();
                var kodeMapel = idMapel.Kode_Mata_Pelajaran;
                int Mapelid = Convert.ToInt32(idGuru.Fk_Id_Mata_Pelajaran);

                if(file != null)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    string path = Server.MapPath("~/Upload/LampiranAbsensi/Guru/" + kodeMapel + "/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    file.SaveAs(path + (Session["userId"] + "_" + file.FileName));
                    var url_file = ("LampiranAbsensi/Guru/" + kodeMapel + "/" + Session["userId"] + "_" + file.FileName).ToString();

                    Tbl_Absensi tbl_absensi = new Tbl_Absensi();
                    tbl_absensi.Tanggal = model.Waktu_Absensi;
                    tbl_absensi.Status = model.Stats.ToString();
                    tbl_absensi.Fk_Id_Sekolah = idSekolah;
                    tbl_absensi.Fk_Id_Siswa = null;
                    tbl_absensi.Fk_Id_Mata_Pelajaran = Mapelid;
                    tbl_absensi.Fk_Id_Guru = userid;
                    tbl_absensi.Status_Absensi = false;
                    tbl_absensi.Url_File = url_file;

                    db.Tbl_Absensi.Add(tbl_absensi);
                    db.Entry(tbl_absensi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("Kehadiran");
                }
                else
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = "Error!";
                    string Error_action = "Request Absensi";
                    string Error_section = "Absensi Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult ValidasiKehadiranSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };

            return View();
        }

        public ActionResult DownloadRequestSiswa(int id)
        {
            var idlampiran = db.Vw_KehadiranSiswa.Where(x => x.Pk_Id_Absensi == id).FirstOrDefault();
            string FileVirtualPath = "~/Upload/" + idlampiran.Url_File;
            return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
        }

        [HttpPost]
        public ActionResult TerimaRequest(int id)
        {
            var req = db.Tbl_Absensi.Where(x => x.Pk_Id_Absensi == id).FirstOrDefault();
            if (req.Status_Absensi == true)
            {
                req.Status_Absensi = false;
            }
            else if (req.Status_Absensi == false)
            {
                req.Status_Absensi = true;
            }
            db.Entry(req).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("ValidasiKehadiranSiswa");
        }

        public JsonResult DataRequestAbsenSiswa()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
            var idSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == idGuru.Fk_Id_Sekolah).FirstOrDefault();
            if(idSekolah != null){
                var namaSekolah = idSekolah.Nama_Sekolah;
                var kehadiran = db.Vw_KehadiranSiswa.Where(x => x.Nama_Sekolah == namaSekolah && x.Status_Absensi == false).ToList();
                return Json(new { data = kehadiran }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var kehadiran = db.Vw_KehadiranSiswa.Where(x => x.Nama_Sekolah == "").ToList();
                return Json(new { data = kehadiran }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult KelasEvaluasi()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
            var kelasEval = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == idGuru.Fk_Id_Sekolah).ToList();
            return Json(new { data = kelasEval }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult MulaiEvaluasi(int? id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            var kls = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == id).FirstOrDefault();
            if (kls == null || id == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                Session["idkelas"] = id;
                return View();
            }
                      
        }

        public JsonResult SiswaEvaluasi()
        {
            db.Configuration.ProxyCreationEnabled = false;
            int kelasSiswa = Convert.ToInt32(Session["idkelas"]);
            var siswas = db.Tbl_Siswa.Where(x => x.Fk_Id_Kelas == kelasSiswa).ToList();
            return Json(new { data = siswas }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult BeriEvaluasi(int? id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
            var siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == id).FirstOrDefault();
            if (siswa == null || id == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                Session["idsiswa"] = id;
                return View();
            }
        }

        public JsonResult DataTGSQUIZ()
        {
            db.Configuration.ProxyCreationEnabled = false;
            int idSiswa = Convert.ToInt32(Session["idsiswa"]);
            int userid = Convert.ToInt32(Session["userId"]);
            var siswas = db.Vw_EvaluasiTotal.Where(x => x.Fk_Id_Siswa == idSiswa && x.Fk_Id_Guru == userid).ToList();
            return Json(new { data = siswas }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MengirimEvaluasi(CrudEvaluasi model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                
                try
                {
                    Tbl_Evaluasi tbl_eval = new Tbl_Evaluasi();
                    tbl_eval.Nilai = model.Nilai;
                    tbl_eval.Evaluasi = model.Evaluasi;
                    tbl_eval.Fk_Id_Guru = Convert.ToInt32(Session["userId"]);
                    tbl_eval.Fk_Id_Siswa = Convert.ToInt32(Session["idsiswa"]);
                    tbl_eval.Fk_Id_Sekolah = Convert.ToInt32(Session["sekolahId"]);
                    tbl_eval.Tanggal_Evaluasi = DateTime.Now;

                    db.Tbl_Evaluasi.Add(tbl_eval);
                    db.Entry(tbl_eval).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return Json("MulaiEvaluasi");
                }
                catch(Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = "Error!";
                    string Error_action = "Request Absensi";
                    string Error_section = "Absensi Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return Json(msg);
                }
            }
        }

        public ActionResult ResultQuiz()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarGuru",
                controllerName = "Guru"
            };
	        var userId = (int)Session["userId"];
            var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == userId).FirstOrDefault();
            List<Tbl_Kategori> kat = db.Tbl_Kategori.Where(x => x.Fk_Id_Guru == guru.Pk_Id_Guru).ToList();
            ViewBag.list = new SelectList(kat, "Pk_Id_Kategori", "KatName");

            return View();
        }

        public JsonResult DataNilaiQuiz()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == userid).FirstOrDefault();
            var nilai = db.Vw_NilaiKuis.Where(x => x.ID_Guru == guru.Pk_Id_Guru).ToList();
            return Json(new { data = nilai }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAbsensiSiswa(int id, Tbl_Absensi absen)
        {
            try
            {
                absen = db.Tbl_Absensi.Where(x => x.Pk_Id_Absensi == id).FirstOrDefault();
                db.Tbl_Absensi.Remove(absen);
                db.SaveChanges();
                return RedirectToAction("ValidasiKehadiranSiswa");
            }
            catch
            {
                return View();
            }
        }
    }
}