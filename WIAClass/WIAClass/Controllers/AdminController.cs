﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WIAClass.Models;
using System.Data.Entity;
using System.Text;
using WIAClass.Infrastructure;
using System.IO;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace WIAClass.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    [CustomAuthorize("2")]
    public class AdminController : Controller
    {
        WiaClassEntities db = new WiaClassEntities();
        // GET: Admin
        public ActionResult Index()
        {
            int sessionSekolahId = (int)Session["sekolahId"];
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            var header = db.Vw_DashboardAdmin.Where(x => x.Pk_Id_Sekolah == sessionSekolahId).FirstOrDefault();
            ViewBag.DataHeader = header;
            var konfirmKehadiran = db.Tbl_Absensi.Where(x => x.Fk_Id_Sekolah == sessionSekolahId && x.Status_Absensi == false && x.Fk_Id_Guru != null).ToList();
            ViewBag.DataHeaderKonfirm = konfirmKehadiran.Count;
            var viewAdmin1 = db.Vw_DashboardAdmin3.Where(x => x.Pk_Id_Sekolah == sessionSekolahId).ToList();
            List<DataPoint1> dataPoints1 = new List<DataPoint1>();
            for (int i = 0; i < viewAdmin1.Count ; i++)
            {
                dataPoints1.Add(new DataPoint1(viewAdmin1[i].Nama_Mata_Pelajaran, Convert.ToDouble(viewAdmin1[i].Jumlah_Guru)));
            }
            ViewBag.DataPoints1 = JsonConvert.SerializeObject(dataPoints1);

            var viewAdmin2 = db.Vw_DashboardAdmin4.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).OrderByDescending(c => c.Nama_Kelas).ToList();
            List<DataPoint2> dataPoints2 = new List<DataPoint2>();
            for (int i = 0; i < viewAdmin2.Count; i++)
            {
                dataPoints2.Add(new DataPoint2(viewAdmin2[i].Nama_Kelas, Convert.ToDouble(viewAdmin2[i].Jumlah_Siswa)));
            }

            ViewBag.DataPoints2 = JsonConvert.SerializeObject(dataPoints2);

            return View();
        }

        // [Creating hashed password that combined with salt (method)]
        public static class Crypto
        {
            public static string Hash(string value)
            {
                return Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value)));
            }
        }

        [HttpPost]
        public JsonResult TampilkanPengumuman(int ID)
        {

            var pgmn = db.Tbl_Pengumuman.Where(x => x.Pk_Id_Pengumuman == ID).FirstOrDefault();
            var msg = "";
            if (pgmn.status == true)
            {
                pgmn.status = false;
                msg = "Pengumuman tidak ditampilkan";
            }
            else if (pgmn.status == false)
            {
                pgmn.status = true;
                msg = "Pengumuman ditampilkan";
            }
            db.Entry(pgmn).State = EntityState.Modified;
            db.SaveChanges();
            return Json(new { message = msg });
        }

        public ActionResult LihatPengumuman(int id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };
            var pengumuman = db.Tbl_Pengumuman.Where(x => x.Pk_Id_Pengumuman == id).FirstOrDefault();
            if (pengumuman == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                return View(pengumuman);
            }
        }
        public ActionResult TampilkanGuru()
        {
            int sessionSekolahId = (int)Session["sekolahId"];

            try
            {
                List<Tbl_Mata_Pelajaran> mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).ToList();
                ViewData["listMapel"] = mapel;
            }
            catch (Exception)
            {

                ViewData["listMapel"] = "";
            }

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }
        public ActionResult TampilkanSiswa()
        {
            int sessionSekolahId = (int)Session["sekolahId"];
            try
            {
                List<Tbl_Kelas> kelas = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).ToList();
                ViewData["listKelas"] = kelas;
            }
            catch (Exception)
            {

                ViewData["listKelas"] = "";
            }

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        public ActionResult TampilkanMapel()
        {

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        public ActionResult TampilkanKelas()
        {

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        public ActionResult Profil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
            var viewModel = new ProfileModel()
            {
                Nama = sekolah.Nama_Sekolah,
                Image = sekolah.Logo,
                No_Telp = sekolah.No_Telp,
                Email = sekolah.Email,
                Alamat = sekolah.Alamat
            };
            return View(viewModel);
        }

        public ActionResult EditProfil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
            var viewModel = new ProfileModel()
            {
                Pk_Id_Sekolah = sekolah.Pk_Id_Sekolah,
                Nama = sekolah.Nama_Sekolah,
                Image = sekolah.Logo,
                No_Telp = sekolah.No_Telp,
                Email = sekolah.Email,
                Alamat = sekolah.Alamat
            };
            return View(viewModel);
        }

        public ActionResult ManagePengumuman()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        [HttpPost]
        public ActionResult EditProfil(CrudSekolahModel modelEditSekolah)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == modelEditSekolah.Pk_Id_Sekolah).FirstOrDefault();
                    sekolah.Nama_Sekolah = modelEditSekolah.Nama_Sekolah;
                    sekolah.No_Telp = modelEditSekolah.No_Telp;
                    sekolah.Email = modelEditSekolah.Email;
                    sekolah.Alamat = modelEditSekolah.Alamat;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Sekolah";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return RedirectToAction("Profil");
                }
        }

        [HttpPost]
        public ActionResult EditFotoProfil(editFotoProfilModel model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var oldPicture = model.oldPicture;
                var newPicture = (Session["userId"] + "_" + file.FileName);
                try
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    file.SaveAs(Server.MapPath("~/Upload/profile/" + Session["userId"] + "_" + file.FileName));
                    //Delete old Picture
                    if (newPicture != oldPicture)
                    {
                        string oldPictPath = Request.MapPath("~/Upload/profile/" + oldPicture);
                        if (System.IO.File.Exists(oldPictPath))
                        {
                            System.IO.File.Delete(oldPictPath);
                        }
                    }

                    int profilId = Convert.ToInt32(Session["userId"].ToString());
                    var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
                    sekolah.Logo = (Session["userId"] + "_" + file.FileName);
                    db.SaveChanges();
                    transaction.Commit();
                    Session["image"] = newPicture;
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Foto Profil";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return RedirectToAction("Profil");
                }
            }
        }

        public JsonResult DataPengumuman()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var pgmn = db.Tbl_Pengumuman.Where(x => x.Fk_Id_Sekolah == idSekolah).ToList();
                return Json(new { data = pgmn }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }

        public JsonResult DataGuru()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var guru = db.Vw_Data_Guru.Where(x => x.ID_Sekolah == idSekolah).ToList();
                return Json(new { data = guru }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataSiswa()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var siswa = db.Vw_Data_Siswa.Where(x => x.ID_Sekolah == idSekolah).ToList();
                return Json(new { data = siswa }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataMapel()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Fk_Id_Sekolah == idSekolah).ToList();
                return Json(new { data = mapel }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataKelas()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var kelas = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == idSekolah).ToList();
                return Json(new { data = kelas }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }

        public JsonResult GetGuruID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(ID)).FirstOrDefault();
            return Json(EditGuru, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSiswaID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditSiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(ID)).FirstOrDefault();
            return Json(EditSiswa, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMapelID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditMapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran.Equals(ID)).FirstOrDefault();
            return Json(EditMapel, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetKelasID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditKelas = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas.Equals(ID)).FirstOrDefault();
            return Json(EditKelas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckEmailSiswa(string Emailcek)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cekEmail = db.Tbl_Siswa.Where(x => x.Email.Equals(Emailcek)).FirstOrDefault();
            return Json(cekEmail, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmailGuru(string Emailcek)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cekEmail = db.Tbl_Guru.Where(x => x.Email.Equals(Emailcek)).FirstOrDefault();
            return Json(cekEmail, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TambahDataGuru(CrudGuruModel modelGuru)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Guru tbl_guru = new Tbl_Guru();

                    tbl_guru.NIP = modelGuru.NIP;
                    tbl_guru.Nama = modelGuru.Nama;
                    tbl_guru.No_Telp = modelGuru.No_Telp;
                    tbl_guru.Email = modelGuru.Email;
                    tbl_guru.Fk_Id_Sekolah = (int)Session["userId"];
                    tbl_guru.Password = Crypto.Hash(modelGuru.Password);
                    tbl_guru.Fk_Id_Mata_Pelajaran = modelGuru.Fk_Id_Mata_Pelajaran;
                    tbl_guru.Fk_Id_Jenis_Kelamin = modelGuru.Fk_Id_Jenis_Kelamin;
                    tbl_guru.Role = 3;
                    tbl_guru.Activated = true;
                    tbl_guru.Foto_Profil = "default.jpg";

                    db.Tbl_Guru.Add(tbl_guru);
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data guru" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult TambahDataSiswa(CrudSiswaModel modelSiswa)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Siswa tbl_siswa = new Tbl_Siswa();

                    tbl_siswa.Nomor_Induk = modelSiswa.Nomor_Induk;
                    tbl_siswa.Nama = modelSiswa.Nama;
                    tbl_siswa.Email = modelSiswa.Email.ToLower();
                    tbl_siswa.Fk_Id_Sekolah = (int)Session["userId"];
                    tbl_siswa.Password = Crypto.Hash(modelSiswa.Password);
                    tbl_siswa.Fk_Id_Kelas = modelSiswa.Fk_Id_Kelas;
                    tbl_siswa.Fk_Id_Jenis_Kelamin = modelSiswa.Fk_Id_Jenis_Kelamin;
                    tbl_siswa.Role = 4;
                    tbl_siswa.Activated = true;
                    tbl_siswa.Foto_Profil = "default.jpg";

                    db.Tbl_Siswa.Add(tbl_siswa);
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data siswa" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult TambahDataMapel(CrudMapelModel modelMapel)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Mata_Pelajaran tbl_mapel = new Tbl_Mata_Pelajaran();

                    tbl_mapel.Kode_Mata_Pelajaran = modelMapel.Kode_Mata_Pelajaran;
                    tbl_mapel.Nama_Mata_Pelajaran = modelMapel.Nama_Mata_Pelajaran;
                    tbl_mapel.Fk_Id_Sekolah = (int)Session["userId"];

                    db.Tbl_Mata_Pelajaran.Add(tbl_mapel);
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data mata pelajaran baru" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Mapel";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }
        [HttpPost]
        public JsonResult TambahDataKelas(CrudKelasModel modelKelas)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Kelas tbl_kelas = new Tbl_Kelas();

                    tbl_kelas.Nama_Kelas = modelKelas.Nama_Kelas;
                    tbl_kelas.Fk_Id_Sekolah = (int)Session["userId"];

                    db.Tbl_Kelas.Add(tbl_kelas);
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data kelas" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Kelas";
                    string User_login = "Hardcode user";
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi kesalahan : " + msg });
                }
        }

        [HttpPost]
        public JsonResult DeletePengumuman(int id, Tbl_Pengumuman pgmn)
        {
            try
            {
                // TODO: Add delete logic here
                pgmn = db.Tbl_Pengumuman.Where(x => x.Pk_Id_Pengumuman == id).FirstOrDefault();
                db.Tbl_Pengumuman.Remove(pgmn);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus pengumuman" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus pengumuman" });
            }
        }

        [HttpPost]
        public JsonResult DeleteGuru(int id, Tbl_Guru guru)
        {
            try
            {
                // TODO: Add delete logic here
                guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == id).FirstOrDefault();
                db.Tbl_Guru.Remove(guru);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data guru" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data guru" });
            }
        }

        [HttpPost]
        public JsonResult DeleteSiswa(int id, Tbl_Siswa siswa)
        {
            try
            {
                // TODO: Add delete logic here
                siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == id).FirstOrDefault();
                db.Tbl_Siswa.Remove(siswa);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data siswa" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data siswa" });
            }
        }

        [HttpPost]
        public JsonResult DeleteMapel(int id, Tbl_Mata_Pelajaran mapel)
        {
            try
            {
                mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran == id).FirstOrDefault();
                db.Tbl_Mata_Pelajaran.Remove(mapel);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data mata pelajaran" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }

        [HttpPost]
        public JsonResult DeleteKelas(int id, Tbl_Kelas kelas)
        {
            try
            {
                kelas = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == id).FirstOrDefault();
                db.Tbl_Kelas.Remove(kelas);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data kelas" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }

        [HttpPost]
        public JsonResult EditGuru(CrudGuruModel modelEditGuru)
        {
        using (var transaction = db.Database.BeginTransaction())
            try
            {
                var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == modelEditGuru.Pk_Id_Guru).FirstOrDefault();
                    
                if (modelEditGuru.Password == guru.Password)
                {
                    guru.NIP = modelEditGuru.NIP;
                    guru.Nama = modelEditGuru.Nama;
                    guru.No_Telp = modelEditGuru.No_Telp;
                    guru.Email = modelEditGuru.Email;
                    guru.Fk_Id_Jenis_Kelamin = modelEditGuru.Fk_Id_Jenis_Kelamin;
                    guru.Fk_Id_Mata_Pelajaran = modelEditGuru.Fk_Id_Mata_Pelajaran;
                }
                else
                {
                    guru.NIP = modelEditGuru.NIP;
                    guru.Nama = modelEditGuru.Nama;
                    guru.No_Telp = modelEditGuru.No_Telp;
                    guru.Email = modelEditGuru.Email;
                    guru.Password = Crypto.Hash(modelEditGuru.Password);
                    guru.Fk_Id_Jenis_Kelamin = modelEditGuru.Fk_Id_Jenis_Kelamin;
                    guru.Fk_Id_Mata_Pelajaran = modelEditGuru.Fk_Id_Mata_Pelajaran;
                }
                db.SaveChanges();
                transaction.Commit();
                return Json(new { message = "Berhasil mengubah data guru" });
                }
            catch (Exception msg)
            {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult EditSiswa(CrudSiswaModel modelEditSiswa)
        {
        using (var transaction = db.Database.BeginTransaction())
            try
            {
                var siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == modelEditSiswa.Pk_Id_Siswa).FirstOrDefault();
                if (modelEditSiswa.Password == siswa.Password)
                {
                    siswa.Nomor_Induk = modelEditSiswa.Nomor_Induk;
                    siswa.Nama = modelEditSiswa.Nama;
                    siswa.Email = modelEditSiswa.Email;
                    siswa.Fk_Id_Jenis_Kelamin = modelEditSiswa.Fk_Id_Jenis_Kelamin;
                    siswa.Fk_Id_Kelas = modelEditSiswa.Fk_Id_Kelas;
                }
                else
                {
                    siswa.Nomor_Induk = modelEditSiswa.Nomor_Induk;
                    siswa.Nama = modelEditSiswa.Nama;
                    siswa.Email = modelEditSiswa.Email;
                    siswa.Fk_Id_Jenis_Kelamin = modelEditSiswa.Fk_Id_Jenis_Kelamin;
                    siswa.Password = Crypto.Hash(modelEditSiswa.Password);
                    siswa.Fk_Id_Kelas = modelEditSiswa.Fk_Id_Kelas;
                }

                db.SaveChanges();
                transaction.Commit();
                return Json(new { message = "Berhasil mengubah data siswa" });
            }
            catch (Exception msg)
            {
                transaction.Rollback();
                WiaClassEntities db2 = new WiaClassEntities();

                var Error_msg = msg;
                string Error_action = "Belum ada action";
                string Error_section = "Edit Siswa";
                string User_login = (string)Session["nama"];
                Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                {
                    Error_msg = Error_msg.Message.ToString(),
                    User_login = User_login,
                    Error_date = DateTime.Now,
                    Error_action = Error_action,
                    Error_section = Error_section
                };
                db2.Entry(AuditTrail).State = EntityState.Added;
                db2.SaveChanges();
                return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
            }
        }

        [HttpPost]
        public JsonResult EditMapel(CrudMapelModel modelEditMapel)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran == modelEditMapel.Pk_Id_Mata_Pelajaran).FirstOrDefault();

                    mapel.Kode_Mata_Pelajaran = modelEditMapel.Kode_Mata_Pelajaran;
                    mapel.Nama_Mata_Pelajaran = modelEditMapel.Nama_Mata_Pelajaran;

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data mata pelajaran" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Mata Pelajaran";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }
        [HttpPost]
        public JsonResult EditKelas(CrudKelasModel modelEditKelas)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var kelas = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == modelEditKelas.Pk_Id_Kelas).FirstOrDefault();

                    kelas.Nama_Kelas = modelEditKelas.Nama_Kelas;

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data kelas" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Kelas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        public ActionResult ReportSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };
            
            string ssrsurl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1300);
            report.Height = Unit.Pixel(650);
            report.ServerReport.ReportServerUrl = new Uri(ssrsurl);
            report.ServerReport.ReportPath = "/WiaClass/DataSiswaSDNImam";
            
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult ReportGuru()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            string ssrsurl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1300);
            report.Height = Unit.Pixel(650);
            report.ServerReport.ReportServerUrl = new Uri(ssrsurl);
            report.ServerReport.ReportPath = "/WiaClass/DataGuruSDNImam";

            ViewBag.ReportViewer = report;
            return View();
        }

        [HttpGet]
        public ActionResult BuatPengumuman()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        [HttpPost]
        public JsonResult BuatPengumuman(CrudPengumuman PengumumanModel)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var sessionId = ((int)Session["userId"]);
                    Tbl_Pengumuman p = new Tbl_Pengumuman
                    {
                        Judul_Pengumuman = PengumumanModel.Judul_Pengumuman,
                        Pengumuman = PengumumanModel.Pengumuman,
                        Fk_Id_Sekolah = sessionId,
                        Waktu = DateTime.Now,
                        status = true,
                        Background = PengumumanModel.Background + ".png",
                        file_Tambahan = PengumumanModel.file_Tambahan

                    };
                    db.Entry(p).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil menambahkan pengumuman!" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Buat Pengumuman";
                    string User_login = ((string)Session["nama"]);
                    WiaClassEntities db2 = new WiaClassEntities();
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        public ActionResult RekapKehadiran()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        [HttpGet]
        public ActionResult RequestKehadiran()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarAdmin",
                controllerName = "Admin"
            };

            return View();
        }

        public ActionResult DownloadFile(int id)
        {
            var idlampiran = db.Vw_KehadiranGuru.Where(x => x.Pk_Id_Absensi == id).FirstOrDefault();
            string FileVirtualPath = "~/Upload/" + idlampiran.Url_File;
            return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
        }

        [HttpPost]
        public ActionResult TerimaRequest(int id)
        {
            var req = db.Tbl_Absensi.Where(x => x.Pk_Id_Absensi == id).FirstOrDefault();
            if (req.Status_Absensi == true)
            {
                req.Status_Absensi = false;
            }
            else if (req.Status_Absensi == false)
            {
                req.Status_Absensi = true;
            }
            db.Entry(req).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("RequestKehadiran");
        }

        public JsonResult DataRequestAbsenGuru()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var namaSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(userid)).FirstOrDefault();
            var kehadiran = db.Vw_KehadiranGuru.Where(x => x.Nama_Sekolah.Equals(namaSekolah.Nama_Sekolah) && x.Status_Absensi == false).ToList();
            return Json(new { data = kehadiran }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DataRekapAbsensi()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var namaSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(userid)).FirstOrDefault();
            var rekapAbsensi = db.Vw_RekapAbsensi.Where(x => x.Nama_Sekolah == namaSekolah.Nama_Sekolah && x.Status_Absensi == true).ToList();
            return Json(new { data = rekapAbsensi }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAbsensiGuru(int id, Tbl_Absensi absen)
        {
            try
            {
                absen = db.Tbl_Absensi.Where(x => x.Pk_Id_Absensi == id).FirstOrDefault();
                db.Tbl_Absensi.Remove(absen);
                db.SaveChanges();
                return RedirectToAction("RequestKehadiran");
            }
            catch
            {
                return View();
            }
        }
    }
}