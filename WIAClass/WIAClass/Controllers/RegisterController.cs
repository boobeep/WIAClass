﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WIAClass.Models;

namespace WIAClass.Controllers
{
    public class RegisterController : Controller
    {
        WiaClassEntities db = new WiaClassEntities();
        // GET: Register

        [HttpGet]
        public ActionResult Index()
        {
            TempData["notif"] = "0";
            return View();
        }

        // [Creating hashed password that combined with salt (method)]
        public static class Crypto
        {
            public static string Hash(string value)
            {
                return Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value)));
            }
        }

        // [Send Activation Email Method]
        public string sendActivationEmail(string targetEmail, string targetNama)
        {
            string activationCode = Guid.NewGuid().ToString();
            var scheme = string.Format("{0}://{1}/{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/Register/VerifyAccount/"));
            var verifyUrl = scheme + activationCode;
            var toMail = new MailAddress(targetEmail);
            var fromMail = new MailAddress("wiaclass@gmail.com", "WIA Class");
            string subject = "Aktivasi Akun WIA Class Anda";
            string body = "Halo " + targetNama + ",";
            body += "<br /><br /> Selamat datang di WIA Class! Terima kasih telah mendaftar, silahkan klik link di bawah untuk aktivasi akun anda: ";
            body += "<br /><a href='" + verifyUrl + "'>" + verifyUrl + "</a>";
            body += "<br /><br /> WIA Class Team";

            var wiaEmail = ConfigurationManager.AppSettings["wiaEmail"];
            var wiaPassword = ConfigurationManager.AppSettings["wiaPassword"];

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(wiaEmail, wiaPassword)

            };

            using (var message = new MailMessage(fromMail, toMail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
            return activationCode;
        }

        // POST: LandingPage/Register
        [HttpPost]
        public ActionResult Index(RegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                try
                {
                    if (model.Password == model.KonfirmasiPassword)
                    {
                        Tbl_Sekolah tbl_sekolah = new Tbl_Sekolah();

                        tbl_sekolah.Nama_Sekolah = model.Nama_Sekolah;
                        tbl_sekolah.Logo = "default.jpg";
                        tbl_sekolah.No_Telp = model.No_Telp;
                        tbl_sekolah.Email = model.Email;
                        tbl_sekolah.Alamat = model.Alamat;
                        tbl_sekolah.Password = Crypto.Hash(model.Password);
                        tbl_sekolah.Role = 2;
                        tbl_sekolah.Tanggal_Registrasi = DateTime.Now;

                        db.Tbl_Sekolah.Add(tbl_sekolah);
 
                        ViewBag.Message = "Berhasil Mendaftar! Cek email anda untuk aktivasi akun";
                        TempData["notif"] = "1";
                        TempData.Keep();
                        // Sending activation email
                        var code = sendActivationEmail(model.Email, model.Nama_Sekolah);
                        tbl_sekolah.ActivationCode = Guid.Parse(code);
                        db.SaveChanges();
                        transaction.Commit();

                        return View("../Login/Index");
                    }
                    else
                    {
                        ViewBag.Message = "Password dan Konfirmasi Password tidak cocok!";
                        TempData["notif"] = "0";
                        TempData.Keep();

                        return View();
                    }
                    
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    var Error_msg = msg;
                    string Error_action = "Sign Up";
                    string Error_section = "School Register";
                    string User_login = model.Email;
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db.Entry(AuditTrail).State = EntityState.Added;
                    db.SaveChanges();
                    ViewBag.Message = "Maaf, terjadi kesalahan saat proses berlangsung.";
                    TempData["notif"] = "0";
                    TempData.Keep();

                    return View();
                }
            }
            else
            {
                ViewBag.Message = "Semua data harus diisi";
                TempData["notif"] = "0";
                TempData.Keep();

                return View();
            }
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            using (db)
            {
                db.Configuration.ValidateOnSaveEnabled = false; // untuk menghindari masalah password tidak match saat save changes
                var verif = db.Tbl_Sekolah.Where(x => x.ActivationCode == new Guid(id)).FirstOrDefault();

                if (verif != null)
                {
                    verif.Activated = true;
                    db.SaveChanges();
                    ViewBag.Message = "Aktivasi akun sukses!";
                    TempData["notif"] = "1";
                    TempData.Keep();

                }

                else
                {
                    ViewBag.Message = "Invalid Requests";
                    TempData["notif"] = "0";
                    TempData.Keep();

                }
            }
            return View("../Login/Index");
        }
    }
}