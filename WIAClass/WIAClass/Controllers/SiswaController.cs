﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WIAClass.Infrastructure;
using WIAClass.Models;

namespace WIAClass.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    [CustomAuthorize("4")]

    public class SiswaController : Controller
    {
        WiaClassEntities db = new WiaClassEntities();
        // GET: Siswa
        public ActionResult Index()
        {
            int limitDitampilkan = 6;
            int sessionSekolahId = (int)Session["sekolahId"];
            List<Tbl_Pengumuman> pengumuman = db.Tbl_Pengumuman.Where(x => x.Fk_Id_Sekolah == sessionSekolahId && x.status == true).OrderByDescending(x => x.Waktu).Take(limitDitampilkan).ToList();
            ViewData["listPengumuman"] = pengumuman;
            if (pengumuman.Count != 0)
            {
                TempData["Message"]  = "Kamu punya beberapa pengumuman nih, Klik judul pengumumannya untuk melihat detailnya";
            }
            else
            {
                TempData["Message"] = "Belum ada pengumuman yang ditambahkan";
            }
            ViewData["nama"] = (string)Session["nama"];

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            Session["absenSiswa"] = "checkout";

            var userid = Convert.ToInt32(Session["userId"]);
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var kelasSiswa = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == idsiswa.Fk_Id_Kelas).FirstOrDefault();
            var namaKelas = kelasSiswa.Nama_Kelas.ToString();
            var tugas = db.Vw_Tugas.Where(x => x.Nama_Kelas.Equals(namaKelas)).ToList();

            var statAbsen = db.Tbl_Absensi.Where(x => x.Fk_Id_Siswa == userid).OrderByDescending(x => x.Pk_Id_Absensi).FirstOrDefault();
            if (statAbsen != null)
            {
                if (statAbsen.Status == "Hadir")
                {
                    ViewBag.StatusAbsen = "Sudah";
                }
                else
                {
                    ViewBag.StatusAbsen = "Belum";
                }
            }
            ViewBag.AdaTugas = tugas.Count;

            return View();
        }

        public ActionResult LihatPengumuman(int id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };
            var pengumuman = db.Tbl_Pengumuman.Where(x => x.Pk_Id_Pengumuman == id).FirstOrDefault();
            if (pengumuman == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                return View(pengumuman);
            }
        }

        public ActionResult Profil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var siswa = db.Vw_Data_Siswa.Where(x => x.Pk_Id_Siswa == profilId).FirstOrDefault();
            return View(siswa);
        }

        public ActionResult EditProfil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == profilId).FirstOrDefault();
            var viewModel = new ProfileModelSiswa()
            {
                Pk_Id_Siswa = siswa.Pk_Id_Siswa,
                Nomor_Induk = siswa.Nomor_Induk,
                Nama = siswa.Nama,
                Foto_Profil = siswa.Foto_Profil,
                Email = siswa.Email,
                Fk_Id_Sekolah = siswa.Fk_Id_Sekolah,
                Fk_Id_Kelas = siswa.Fk_Id_Kelas
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditFotoProfil(editFotoProfilModel model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var oldPicture = model.oldPicture;
                var newPicture = (Session["userId"] + "_" + file.FileName);
                try
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    file.SaveAs(Server.MapPath("~/Upload/profile/" + Session["userId"] + "_" + file.FileName));
                    //Delete old Picture
                    if (newPicture != oldPicture)
                    {
                        string oldPictPath = Request.MapPath("~/Upload/profile/" + oldPicture);
                        if (System.IO.File.Exists(oldPictPath))
                        {
                            System.IO.File.Delete(oldPictPath);
                        }
                    }

                    int profilId = Convert.ToInt32(Session["userId"].ToString());
                    var siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == profilId).FirstOrDefault();
                    siswa.Foto_Profil = (Session["userId"] + "_" + file.FileName);
                    db.SaveChanges();
                    transaction.Commit();
                    Session["image"] = siswa.Foto_Profil;
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Foto Profil";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                }
                return View();
            }
        }

        public ActionResult Testimonial()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            return View();
        }

        [HttpPost]
        public ActionResult Testimonial(cmsTestimonial test)
        {
        using (var transaction = db.Database.BeginTransaction())
            try
            {
                var sessionEmail = ((string)Session["email"]);
                var namaSiswa = db.Tbl_Siswa.Where(x => x.Email == sessionEmail).FirstOrDefault();
                Tbl_Testimonial testi = new Tbl_Testimonial
                {
                    Nama = namaSiswa.Nama,
                    Posisi_Role = "Siswa",
                    Testimoni = test.Testimoni,
                    Status = false,
                    Foto = (string)Session["image"],
                };
                db.Entry(testi).State = EntityState.Added;
                db.SaveChanges();
                transaction.Commit();
                return RedirectToAction("Testimonial");
            }
            catch (Exception msg)
            {
                transaction.Rollback();
                WiaClassEntities db2 = new WiaClassEntities();

                var Error_msg = msg;
                string Error_action = "Belum ada action";
                string Error_section = "Edit Siswa";
                string User_login = (string)Session["nama"];
                Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                {
                    Error_msg = Error_msg.Message.ToString(),
                    User_login = User_login,
                    Error_date = DateTime.Now,
                    Error_action = Error_action,
                    Error_section = Error_section
                };
                db2.Entry(AuditTrail).State = EntityState.Added;
                db2.SaveChanges();
                ViewBag.Message = msg;
                return View(msg);
            }
        }

        [HttpPost]
        public ActionResult EditProfil(CrudSiswaModel modelEditSiswa)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == modelEditSiswa.Pk_Id_Siswa).FirstOrDefault();
                    siswa.Nomor_Induk = modelEditSiswa.Nomor_Induk;
                    siswa.Nama = modelEditSiswa.Nama;
                    siswa.Email = modelEditSiswa.Email;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        public ActionResult KehadiranSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            ViewBag.Message = "Pencatatan Kehadiran";
            return View();
        }

        public ActionResult JoinVideo()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            ViewBag.Message = "Bergabung kelas virtual";
            return View();
        }

        [HttpGet]
        public ActionResult EvaluasiSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            ViewBag.Message = "Evaluasi Siswa";
            return View();
        }

        public ActionResult KirimTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            return View();
        }

        public ActionResult Quiz(int id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };
            
            Session["quizId"] = id;
            var kuis = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == id).FirstOrDefault();
            TempData["Waktu"] = kuis.WaktuPengerjaan;
            TempData.Keep();
            if (kuis == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                TempData["soal"] = 0;
                List<Tbl_Pertanyaan> li = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == id).ToList();
                Queue<Tbl_Pertanyaan> queue = new Queue<Tbl_Pertanyaan>();
                foreach (Tbl_Pertanyaan a in li)
                {
                    queue.Enqueue(a);
                    TempData["soal"] = Convert.ToInt32(TempData["soal"]) + 1;
                }
                TempData["pertanyaan"] = queue;
                TempData["nilai"] = 0;
                TempData.Keep();
                return RedirectToAction("QuizStart");          
            }

        }
        public ActionResult QuizStart()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };
            ViewBag.Waktu = TempData["Waktu"];
            TempData.Keep();
            Tbl_Pertanyaan p = null;
            if (TempData["pertanyaan"] != null)
            {
                Queue<Tbl_Pertanyaan> qlist = (Queue<Tbl_Pertanyaan>)TempData["pertanyaan"];
                if (qlist.Count > 0)
                {
                    p = qlist.Peek();
                    qlist.Dequeue();
                    TempData["pId"] = p.Pk_Id_Pertanyaan;
                    TempData["pertanyaan"] = qlist;
                    TempData.Keep();
                    return View(p);
                }
                else
                {
                    return RedirectToAction("EndQuiz");
                }
            }
            else
            {
                return RedirectToAction("SelectQuiz");
            }
        }

        [HttpPost]
        public ActionResult QuizStart(Tbl_Pertanyaan p)
        {
            int pId = (int)TempData["pId"];
            var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan == pId).FirstOrDefault();
            string studentanswer = null;
            
            if (p.OptA != null)
            {
                studentanswer = "A";
            }
            else if(p.OptB != null)
            {
                studentanswer = "B";
            }
            else if (p.OptC != null)
            {
                studentanswer = "C";
            }
            else if (p.OptD != null)
            {
                studentanswer = "D";
            } else
            {
                studentanswer = "kosong";
            }

            if (studentanswer.Equals(pertanyaan.CorrectOpt))
            {
                TempData["nilai"] = Convert.ToInt32(TempData["nilai"]) + 1;
            }
            
            TempData.Keep();

            return RedirectToAction("QuizStart");
        }

        public ActionResult SelectQuiz()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            ViewBag.Message = "Management data Quiz";
            return View();
        }

        public JsonResult DataKuis()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = (int)Session["userId"];
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var siswa_kelas = idsiswa.Fk_Id_Kelas;
            //var sudah = db.Tbl_QuizNilai.Where(x => x.Fk_Id_Siswa == userid).FirstOrDefault();
            List<Vw_KategoriQuiz> kuis = db.Vw_KategoriQuiz.Where(x => x.IDKelas == siswa_kelas && x.Status == true).ToList();
            return Json(new { data = kuis }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EndQuiz()
        {

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };
            int score = (int)TempData["nilai"];
            int soal = (int)TempData["soal"];
            float finalscore = 100 * score / soal;
            TempData["nilaiTotal"] = finalscore;
            TempData.Keep();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    int userId = (int)Session["userId"];
                    int quizId = (int)Session["quizId"];
                    Tbl_QuizNilai nilai = new Tbl_QuizNilai();
                    nilai.Tanggal = DateTime.Now;
                    nilai.NilaiQuiz = (float)TempData["nilaiTotal"];
                    nilai.Fk_Id_Kategori = quizId;
                    nilai.Fk_Id_Siswa = userId;
                    db.Tbl_QuizNilai.Add(nilai);
                    db.Entry(nilai).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return View();
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data nilai kuis";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
            }
        }

        public JsonResult DataTugas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var kelasSiswa = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == idsiswa.Fk_Id_Kelas).FirstOrDefault();
            var namaKelas = kelasSiswa.Nama_Kelas.ToString();
            var tugas = db.Vw_Tugas.Where(x => x.Nama_Kelas.Equals(namaKelas) && x.Id_Sekolah == idsiswa.Fk_Id_Sekolah).ToList();
            return Json(new { data = tugas }, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public ActionResult MengirimTugas(UploadViewModel model, int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var userid = (int)Session["userId"];
                var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
                var idsekolah = Convert.ToInt32(idsiswa.Fk_Id_Sekolah);
                int siswa_kelas = Convert.ToInt32(idsiswa.Fk_Id_Kelas);
                var tugas = db.Vw_Tugas.Where(x => x.Id_Tugas.Equals(id)).FirstOrDefault();
                if (file != null)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    string path = Server.MapPath("~/Upload/UploadTugas/" + siswa_kelas + "/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    file.SaveAs(path + (Session["userId"] + "_" + file.FileName));
                    var url_file = ("UploadTugas/" + siswa_kelas + "/" + Session["userId"] + "_" + file.FileName).ToString();

                    Tbl_Pengumpulan_Tugas tbl_kumpul_tugas = new Tbl_Pengumpulan_Tugas();                              
                    tbl_kumpul_tugas.Waktu_Pengumpulan = DateTime.Now;
                    tbl_kumpul_tugas.Url_File = url_file;
                    tbl_kumpul_tugas.Fk_Id_Sekolah = idsekolah;
                    tbl_kumpul_tugas.Fk_Id_Siswa = userid;
                    tbl_kumpul_tugas.Fk_Id_Tugas = Convert.ToInt32(tugas.Id_Tugas);
                    tbl_kumpul_tugas.Status_Nilai = false;
                    tbl_kumpul_tugas.Fk_Id_Kelas = idsiswa.Fk_Id_Kelas;

                    db.Tbl_Pengumpulan_Tugas.Add(tbl_kumpul_tugas);
                    db.Entry(tbl_kumpul_tugas).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                   
                    return RedirectToAction("KirimTugas");
                }
                else 
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = "Error";
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Foto Profil";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;
                }
                return View();
            }
        }

        [HttpGet]
        public ActionResult HistoriTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            ViewBag.Message = "Histori Pengiriman Tugas";
            return View();
        }

        public JsonResult DataKirimTugas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var kelasSiswa = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == idsiswa.Fk_Id_Kelas).FirstOrDefault();
            var namaKelas = kelasSiswa.Nama_Kelas.ToString();
            var hist_tugas = db.Vw_KirimTugas.Where(x => x.Nama_Kelas == namaKelas && x.Nama_Siswa == idsiswa.Nama).ToList();
            return Json(new { data = hist_tugas }, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public ActionResult AbsenSiswaHadir()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var userid = Convert.ToInt32(Session["userId"]);
                var idSiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
                int idSekolah = Convert.ToInt32(idSiswa.Fk_Id_Sekolah);

                try
                {

                    Tbl_Absensi tbl_absensi = new Tbl_Absensi();
                    tbl_absensi.Tanggal = DateTime.Now;
                    tbl_absensi.Status = "Hadir";
                    tbl_absensi.Fk_Id_Sekolah = idSekolah;
                    tbl_absensi.Fk_Id_Siswa = userid;
                    tbl_absensi.Fk_Id_Mata_Pelajaran = null;
                    tbl_absensi.Fk_Id_Guru = null;
                    tbl_absensi.Status_Absensi = true;

                    db.Tbl_Absensi.Add(tbl_absensi);
                    db.Entry(tbl_absensi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    Session["absenSiswa"] = "checkin";
                    Session["absenSiswa"] = Session["absenSiswa"].ToString();
                                                               
                    return RedirectToAction("KehadiranSiswa");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Absensi Masuk";
                    string Error_section = "Absensi Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return View(msg);
                }
            }
        }

        [HttpPost]
        public ActionResult AbsenSiswaPulang()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var userid = Convert.ToInt32(Session["userId"]);
                var idSiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
                int idSekolah = Convert.ToInt32(idSiswa.Fk_Id_Sekolah);

                try
                {
                    Tbl_Absensi tbl_absensi = new Tbl_Absensi();
                    tbl_absensi.Tanggal = DateTime.Now;
                    tbl_absensi.Status = "Pulang";
                    tbl_absensi.Fk_Id_Sekolah = idSekolah;
                    tbl_absensi.Fk_Id_Siswa = userid;
                    tbl_absensi.Fk_Id_Mata_Pelajaran = null;
                    tbl_absensi.Fk_Id_Guru = null;
                    tbl_absensi.Status_Absensi = true;

                    db.Tbl_Absensi.Add(tbl_absensi);
                    db.Entry(tbl_absensi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    Session["absenSiswa"] = "checkout";
                    Session["absenSiswa"] = Session["absenSiswa"].ToString();

                    return RedirectToAction("KehadiranSiswa");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = msg;
                    string Error_action = "Absensi Masuk";
                    string Error_section = "Absensi Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return View(msg);
                }
            }
        }

        public JsonResult DataKehadiranSiswa()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var namaSiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var kehadiran = db.Vw_KehadiranSiswa.Where(x => x.Nama_Siswa.Equals(namaSiswa.Nama) && x.Status_Absensi == true).ToList();
            return Json(new { data = kehadiran }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DataPengumuman()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var content = db.Tbl_Pengumuman.Where(x => x.Pk_Id_Pengumuman == 1).FirstOrDefault();
                return Json(content, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        [HttpPost]

        public ActionResult RequestApprovalSiswa(RequestApprovalModel model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var userid = Convert.ToInt32(Session["userId"]);
                var idSiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
                int idSekolah = Convert.ToInt32(idSiswa.Fk_Id_Sekolah);
                int idKelas = Convert.ToInt32(idSiswa.Fk_Id_Kelas);

                if (file != null)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    string path = Server.MapPath("~/Upload/LampiranAbsensi/Siswa/" + idKelas + "/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    file.SaveAs(path + (Session["userId"] + "_" + file.FileName));
                    var url_file = ("LampiranAbsensi/Siswa/" + idKelas + "/" + Session["userId"] + "_" + file.FileName).ToString();

                    Tbl_Absensi tbl_absensi = new Tbl_Absensi();
                    tbl_absensi.Tanggal = model.Waktu_Absensi;
                    tbl_absensi.Status = model.Stats.ToString();
                    tbl_absensi.Fk_Id_Sekolah = idSekolah;
                    tbl_absensi.Fk_Id_Siswa = userid;
                    tbl_absensi.Fk_Id_Mata_Pelajaran = null;
                    tbl_absensi.Fk_Id_Guru = null;
                    tbl_absensi.Status_Absensi = false;
                    tbl_absensi.Url_File = url_file;

                    db.Tbl_Absensi.Add(tbl_absensi);
                    db.Entry(tbl_absensi).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("KehadiranSiswa");
                }
                else
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = "Error";
                    string Error_action = "Request Absensi";
                    string Error_section = "Absensi Siswa";
                    string User_login = "Hardcode user";
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;

                    return View();
                }
            }
        }

        public JsonResult DataEvaluasi()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = (int)Session["userId"];
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            List<Vw_HasilEvaluasi> evaluasi = db.Vw_HasilEvaluasi.Where(x => x.Id_Siswa == idsiswa.Pk_Id_Siswa && x.Id_Sekolah == idsiswa.Fk_Id_Sekolah).ToList();
            return Json(new { data = evaluasi }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EvaluasiPerTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSiswa",
                controllerName = "Siswa"
            };

            ViewBag.Message = "Evaluasi Per Tugas";
            return View();
        }

        public JsonResult DataEvalPerTugas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = (int)Session["userId"];
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            List<Vw_EvaluasiPerTugas> evaluasi = db.Vw_EvaluasiPerTugas.Where(x => x.Fk_Id_Siswa == userid && x.Fk_Id_Kelas == idsiswa.Fk_Id_Kelas && x.Fk_Id_Sekolah == idsiswa.Fk_Id_Sekolah).ToList();
            return Json(new { data = evaluasi }, JsonRequestBehavior.AllowGet);
        }

    }
}