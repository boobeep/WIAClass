﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WIAClass.Models;
using System.Text;
using System.Data.Entity;
using WIAClass.Infrastructure;
using System.IO;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace WIAClass.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    [CustomAuthorize("1")]
    public class SAController : Controller
    {
        WiaClassEntities db = new WiaClassEntities();

        // GET: SA
        public ActionResult Index()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            double totalSekolah = db.Tbl_Sekolah.Count();
            double totalGuru = db.Tbl_Guru.Count();
            double totalSiswa = db.Tbl_Siswa.Count();
            double totalTestimoni = db.Tbl_Testimonial.Count();
            ViewBag.totalSekolah = totalSekolah;
            ViewBag.totalGuru = totalGuru;
            ViewBag.totalSiswa = totalSiswa;
            ViewBag.totalTestimoni = totalTestimoni;

            double persentaseGuru = (totalGuru / (totalGuru + totalSiswa) * 100);
            double persentaseSiswa = (totalSiswa / (totalGuru + totalSiswa) * 100);

            List<DataPoint3> dataPoints3 = new List<DataPoint3>();

            dataPoints3.Add(new DataPoint3("Guru", persentaseGuru));
            dataPoints3.Add(new DataPoint3("Siswa", persentaseSiswa));

            ViewBag.DataPoints1 = JsonConvert.SerializeObject(dataPoints3);

            var viewSA1 = db.Vw_DashboardSA1.ToList();
            List<DataPoint2> dataPointsXY = new List<DataPoint2>();
            for (int i = 0; i < viewSA1.Count; i++)
            {
                int a = (int)viewSA1[i].mmYYYY;
                int c = (int)viewSA1[i].mmYYYY;
                string b = "";
                if (a >= 10000) a /= 10000;
                if (a == 1) { b = "Januari " + (c % 10000); }
                else if (a == 2) { b = "Februari " + (c % 10000); }
                else if (a == 3) { b = "Maret " + (c % 10000); }
                else if (a == 4) { b = "April " + (c % 10000); }
                else if (a == 5) { b = "Mei " + (c % 10000); }
                else if (a == 6) { b = "Juni " + (c % 1000); }
                else if (a == 7) { b = "Juli " + (c % 10000); }
                else if (a == 8) { b = "Agustus " + (c % 10000); }
                else if (a == 9) { b = "September " + (c % 10000); }
                else if (a == 10) { b = "Oktober " + (c % 10000); }
                else if (a == 11) { b = "November " + (c % 10000); }
                else if (a == 12) { b = "Desember " + (c % 10000); }
                dataPointsXY.Add(new DataPoint2(Convert.ToString(b), Convert.ToDouble(viewSA1[i].Jumlah_Sekolah)));
            }
            ViewBag.DataPoints2 = JsonConvert.SerializeObject(dataPointsXY);
            return View();
        }

        public ActionResult TampilkanSekolah()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }
        public ActionResult TampilkanGuru()
        {
            int sessionSekolahId = (int)Session["sekolahId"];
            try
            {
                List<Tbl_Sekolah> sekolah = db.Tbl_Sekolah.ToList();
                ViewData["listSekolah"] = sekolah;
            }
            catch (Exception)
            {
                ViewData["listSekolah"] = "";
            }

            try
            {
                List<Tbl_Mata_Pelajaran> mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).ToList();
                ViewData["listMapel"] = mapel;
            }
            catch (Exception)
            {

                ViewData["listMapel"] = "";
            }

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }
        public ActionResult TampilkanSiswa()
        {
            int sessionSekolahId = (int)Session["sekolahId"];

            try
            {
                List<Tbl_Sekolah> sekolah = db.Tbl_Sekolah.ToList();
                ViewData["listSekolah"] = sekolah;
            }
            catch (Exception)
            {
                ViewData["listSekolah"] = "";
            }

            try
            {
                List<Tbl_Kelas> kelas = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == sessionSekolahId).ToList();
                ViewData["listKelas"] = kelas;
            }
            catch (Exception)
            {

                ViewData["listKelas"] = "";
            }
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult TampilkanAuditTrail()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult Testimonial()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult FAQ()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult KontakKami()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult Profil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
            var viewModel = new ProfileModel()
            {
                Nama = sekolah.Nama_Sekolah,
                Image = sekolah.Logo,
                No_Telp = sekolah.No_Telp,
                Email = sekolah.Email,
                Alamat = sekolah.Alamat
            };
            return View(viewModel);
        }

        public ActionResult EditProfil()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };
            return View();
        }

        public ActionResult TampilkanMapel()
        {

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult TampilkanKelas()
        {

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public JsonResult DataSekolah()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var sekolah = db.Tbl_Sekolah.ToList();
                return Json(new { data = sekolah }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataGuru()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var guru = db.Vw_Data_Guru.ToList();
                return Json(new { data = guru }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataSiswa()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var siswa = db.Vw_Data_Siswa.ToList();
                return Json(new { data = siswa }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataJekel()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var jekel = db.Tbl_Jenis_Kelamin.ToList();
                return Json(new { data = jekel }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataTestimoni()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var testi = db.Tbl_Testimonial.ToList();
                return Json(new { data = testi }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataKontakKami()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var kontak = db.Tbl_Kontak_Kami.ToList();
                return Json(new { data = kontak }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataMapel()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var mapel = db.Tbl_Mata_Pelajaran.ToList();
                return Json(new { data = mapel }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult DataKelas()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                int idSekolah = (int)Session["userId"];
                var kelas = db.Tbl_Kelas.ToList();
                return Json(new { data = kelas }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }

        public JsonResult DataAuditTrail()
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var auditTrail = db.Tbl_AuditTrail.ToList();
                return Json(new { data = auditTrail }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }

        public JsonResult GetSekolahID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(ID)).FirstOrDefault();
            return Json(EditSekolah, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGuruID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditGuru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(ID)).FirstOrDefault();
            return Json(EditGuru, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSiswaID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditSiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(ID)).FirstOrDefault();
            return Json(EditSiswa, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetMapelID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditMapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran.Equals(ID)).FirstOrDefault();
            return Json(EditMapel, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetKelasID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditKelas = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas.Equals(ID)).FirstOrDefault();
            return Json(EditKelas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAuditTrailID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var AT = db.Tbl_AuditTrail.Where(x => x.Pk_Id_AuditTrail.Equals(ID)).FirstOrDefault();
            return Json(AT, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CheckEmailSiswa(string Emailcek)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cekEmail = db.Tbl_Siswa.Where(x => x.Email.Equals(Emailcek)).FirstOrDefault();
            return Json(cekEmail, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmailGuru(string Emailcek)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cekEmail = db.Tbl_Guru.Where(x => x.Email.Equals(Emailcek)).FirstOrDefault();
            return Json(cekEmail, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmailSekolah(string Emailcek)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cekEmail = db.Tbl_Sekolah.Where(x => x.Email.Equals(Emailcek)).FirstOrDefault();
            return Json(cekEmail, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIDSekolahGuru(int ID)
        {
            try
            {
                List<Tbl_Mata_Pelajaran> mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Fk_Id_Sekolah == ID).ToList();
                db.Configuration.ProxyCreationEnabled = false;
                var IDSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(ID)).FirstOrDefault();
                var namaSekolah = "";
                if(IDSekolah != null){
                    namaSekolah = IDSekolah.Nama_Sekolah;
                }
                else
                {
                    namaSekolah = "(Sekolah Tidak Ditemukan)";
                }
                var listMapel = new SelectList(mapel, "Pk_Id_Mata_Pelajaran", "Nama_Mata_Pelajaran");
                var result = new { NamaSekolah = namaSekolah, listMapel = listMapel };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }

        public JsonResult GetIDSekolahSiswa(int ID)
        {
            try
            {
                List<Tbl_Kelas> kelas = db.Tbl_Kelas.Where(x => x.Fk_Id_Sekolah == ID).ToList();
                db.Configuration.ProxyCreationEnabled = false;
                var IDSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(ID)).FirstOrDefault();
                var namaSekolah = "";
                if (IDSekolah != null)
                {
                    namaSekolah = IDSekolah.Nama_Sekolah;
                }
                else
                {
                    namaSekolah = "(Sekolah Tidak Ditemukan)";
                }
                var listKelas = new SelectList(kelas, "Pk_Id_Kelas", "Nama_Kelas");
                var result = new { NamaSekolah = namaSekolah, listKelas = listKelas };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult GetIDSekolahMapel(int ID)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var IDSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(ID)).FirstOrDefault();
                return Json(IDSekolah, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("No Data");
                throw;
            }
        }
        public JsonResult GetIDSekolahKelas(int ID)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var IDSekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah.Equals(ID)).FirstOrDefault();
                return Json(IDSekolah, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("No Data");
                throw ex;
            }
        }

        public static class Crypto
        {
            public static string Hash(string value)
            {
                return Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value)));
            }
        }

        [HttpPost]
        public ActionResult TampilkanTesti(int ID)
        {
            var testimoni = db.Tbl_Testimonial.Where(x => x.Pk_Id_Testimonial == ID).FirstOrDefault();
            if (testimoni.Status == true )
            {
                testimoni.Status = false;
            }else if (testimoni.Status == false)
            {
                testimoni.Status = true;
            }
            db.Entry(testimoni).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Testimonial");
        }

        [HttpPost]
        public JsonResult TambahDataSekolah(CrudSekolahModel modelSekolah)
        {
        using (var transaction = db.Database.BeginTransaction())
            try
            {
                Tbl_Sekolah tbl_sekolah = new Tbl_Sekolah();

                tbl_sekolah.Nama_Sekolah = modelSekolah.Nama_Sekolah;
                tbl_sekolah.Logo = "default.jpg";
                tbl_sekolah.No_Telp = modelSekolah.No_Telp;
                tbl_sekolah.Email = modelSekolah.Email;
                tbl_sekolah.Alamat = modelSekolah.Alamat;
                tbl_sekolah.Password = Crypto.Hash(modelSekolah.Password);
                tbl_sekolah.Role = 2;
                tbl_sekolah.Activated = true;
                tbl_sekolah.Tanggal_Registrasi = DateTime.Now;

                db.Tbl_Sekolah.Add(tbl_sekolah);
                db.Entry(tbl_sekolah).State = EntityState.Added;
                db.SaveChanges();
                transaction.Commit();
                return Json(new { message = "Berhasil menambahkan data sekolah" });

            }
            catch (Exception msg)
            {
                transaction.Rollback();
                WiaClassEntities db2 = new WiaClassEntities();

                var Error_msg = msg;
                string Error_action = "Belum ada action";
                string Error_section = "Tambah data Sekolah";
                string User_login = (string)Session["nama"];
                Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                {
                    Error_msg = Error_msg.Message.ToString(),
                    User_login = User_login,
                    Error_date = DateTime.Now,
                    Error_action = Error_action,
                    Error_section = Error_section
                };
                db2.Entry(AuditTrail).State = EntityState.Added;
                db2.SaveChanges();
                return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
            }
        }

        [HttpPost]
        public JsonResult TambahDataGuru(CrudGuruModel modelGuru)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Guru tbl_guru = new Tbl_Guru();

                    tbl_guru.NIP = modelGuru.NIP;
                    tbl_guru.Nama = modelGuru.Nama;
                    tbl_guru.No_Telp = modelGuru.No_Telp;
                    tbl_guru.Email = modelGuru.Email;
                    tbl_guru.Fk_Id_Sekolah = modelGuru.Fk_Id_Sekolah;
                    tbl_guru.Password = Crypto.Hash(modelGuru.Password);
                    tbl_guru.Fk_Id_Mata_Pelajaran = modelGuru.Fk_Id_Mata_Pelajaran;
                    tbl_guru.Fk_Id_Jenis_Kelamin = modelGuru.Fk_Id_Jenis_Kelamin;
                    tbl_guru.Foto_Profil = "default.jpg";
                    tbl_guru.Role = 3;
                    tbl_guru.Activated = true;

                    db.Tbl_Guru.Add(tbl_guru);
                    db.Entry(tbl_guru).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data guru" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult TambahDataSiswa(CrudSiswaModel modelSiswa)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Siswa tbl_siswa = new Tbl_Siswa();

                    tbl_siswa.Nomor_Induk = modelSiswa.Nomor_Induk;
                    tbl_siswa.Nama = modelSiswa.Nama;
                    tbl_siswa.Email = modelSiswa.Email.ToLower();
                    tbl_siswa.Fk_Id_Sekolah = modelSiswa.Fk_Id_Sekolah;
                    tbl_siswa.Password = Crypto.Hash(modelSiswa.Password);
                    tbl_siswa.Fk_Id_Kelas = modelSiswa.Fk_Id_Kelas;
                    tbl_siswa.Fk_Id_Jenis_Kelamin = modelSiswa.Fk_Id_Jenis_Kelamin;
                    tbl_siswa.Role = 4;
                    tbl_siswa.Activated = true;
                    tbl_siswa.Foto_Profil = "default.jpg";

                    db.Tbl_Siswa.Add(tbl_siswa);
                    db.Entry(tbl_siswa).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data siswa" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult TambahDataMapel(CrudMapelModel modelMapel)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Mata_Pelajaran tbl_mapel = new Tbl_Mata_Pelajaran();

                    tbl_mapel.Kode_Mata_Pelajaran = modelMapel.Kode_Mata_Pelajaran;
                    tbl_mapel.Nama_Mata_Pelajaran = modelMapel.Nama_Mata_Pelajaran;
                    tbl_mapel.Fk_Id_Sekolah = modelMapel.Fk_Id_Sekolah;

                    db.Tbl_Mata_Pelajaran.Add(tbl_mapel);
                    db.Entry(tbl_mapel).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data mata pelajaran baru" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Mapel";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }
        [HttpPost]
        public JsonResult TambahDataKelas(CrudKelasModel modelKelas)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Kelas tbl_kelas = new Tbl_Kelas();

                    tbl_kelas.Nama_Kelas = modelKelas.Nama_Kelas;
                    tbl_kelas.Fk_Id_Sekolah = modelKelas.Fk_Id_Sekolah;

                    db.Tbl_Kelas.Add(tbl_kelas);
                    db.Entry(tbl_kelas).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return Json(new { message = "Berhasil menambahkan data kelas" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Kelas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult DeleteSekolah(int id, Tbl_Sekolah sekolah)
        {
            try
            {
                // TODO: Add delete logic here
                sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == id).FirstOrDefault();
                db.Tbl_Sekolah.Remove(sekolah);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data sekolah" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }

        [HttpPost]
        public JsonResult DeleteGuru(int id, Tbl_Guru guru)
        {
            try
            {
                // TODO: Add delete logic here
                guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == id).FirstOrDefault();
                db.Tbl_Guru.Remove(guru);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data guru" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }

        [HttpPost]
        public JsonResult DeleteSiswa(int id, Tbl_Siswa siswa)
        {
            try
            {
                // TODO: Add delete logic here
                siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == id).FirstOrDefault();
                db.Tbl_Siswa.Remove(siswa);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data siswa" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data siswa" });
            }
        }

        [HttpPost]
        public JsonResult DeleteTestimonial(int id, Tbl_Testimonial testimoni)
        {
            try
            {
                // TODO: Add delete logic here
                testimoni = db.Tbl_Testimonial.Where(x => x.Pk_Id_Testimonial == id).FirstOrDefault();
                db.Tbl_Testimonial.Remove(testimoni);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data Testimonial" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data testimonial" });
            }
        }

        [HttpPost]
        public JsonResult DeleteFeedback(int id)
        {
            try
            {
                // TODO: Add delete logic here
                var kontak = db.Tbl_Kontak_Kami.Where(x => x.Pk_Id_Kontak_Kami == id).FirstOrDefault();
                db.Tbl_Kontak_Kami.Remove(kontak);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data Kontak Center" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data kontak center" });
            }
        }

        [HttpPost]
        public JsonResult DeleteMapel(int id, Tbl_Mata_Pelajaran mapel)
        {
            try
            {
                mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran == id).FirstOrDefault();
                db.Tbl_Mata_Pelajaran.Remove(mapel);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data mata pelajaran" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }

        [HttpPost]
        public JsonResult DeleteKelas(int id, Tbl_Kelas kelas)
        {
            try
            {
                kelas = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == id).FirstOrDefault();
                db.Tbl_Kelas.Remove(kelas);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data kelas" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }

        [HttpPost]
        public JsonResult DeleteAuditTrail(int id, Tbl_AuditTrail AT)
        {
            try
            {
                AT = db.Tbl_AuditTrail.Where(x => x.Pk_Id_AuditTrail == id).FirstOrDefault();
                db.Tbl_AuditTrail.Remove(AT);
                db.SaveChanges();
                return Json(new { message = "Berhasil menghapus data Audit Trail" });
            }
            catch
            {
                return Json(new { message = "Terjadi kesalahan saat menghapus data" });
            }
        }


        [HttpPost]
        public JsonResult EditSekolah(CrudSekolahModel modelEditSekolah)
        {
            using (var transaction = db.Database.BeginTransaction())
            try
            {
                var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == modelEditSekolah.Pk_Id_Sekolah).FirstOrDefault();
                    if (modelEditSekolah.Password == sekolah.Password)
                    {
                        sekolah.Nama_Sekolah = modelEditSekolah.Nama_Sekolah;
                        sekolah.No_Telp = modelEditSekolah.No_Telp;
                        sekolah.Email = modelEditSekolah.Email;
                        sekolah.Alamat = modelEditSekolah.Alamat;
                    }
                    else
                    {
                        sekolah.Nama_Sekolah = modelEditSekolah.Nama_Sekolah;
                        sekolah.No_Telp = modelEditSekolah.No_Telp;
                        sekolah.Email = modelEditSekolah.Email;
                        sekolah.Password = Crypto.Hash(modelEditSekolah.Password);
                        sekolah.Alamat = modelEditSekolah.Alamat;
                    }

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data sekolah" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Sekolah";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult EditGuru(CrudGuruModel modelEditGuru)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var guru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru == modelEditGuru.Pk_Id_Guru).FirstOrDefault();

                    if (modelEditGuru.Password == guru.Password)
                    {
                        guru.NIP = modelEditGuru.NIP;
                        guru.Nama = modelEditGuru.Nama;
                        guru.No_Telp = modelEditGuru.No_Telp;
                        guru.Email = modelEditGuru.Email;
                        guru.Fk_Id_Jenis_Kelamin = modelEditGuru.Fk_Id_Jenis_Kelamin;
                        guru.Fk_Id_Mata_Pelajaran = modelEditGuru.Fk_Id_Mata_Pelajaran;
                    }
                    else
                    {
                        guru.NIP = modelEditGuru.NIP;
                        guru.Nama = modelEditGuru.Nama;
                        guru.No_Telp = modelEditGuru.No_Telp;
                        guru.Email = modelEditGuru.Email;
                        guru.Password = Crypto.Hash(modelEditGuru.Password);
                        guru.Fk_Id_Jenis_Kelamin = modelEditGuru.Fk_Id_Jenis_Kelamin;
                        guru.Fk_Id_Mata_Pelajaran = modelEditGuru.Fk_Id_Mata_Pelajaran;
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data guru" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Guru";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult EditSiswa(CrudSiswaModel modelEditSiswa)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var siswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa == modelEditSiswa.Pk_Id_Siswa).FirstOrDefault();
                    if (modelEditSiswa.Password == siswa.Password)
                    {
                        siswa.Nomor_Induk = modelEditSiswa.Nomor_Induk;
                        siswa.Nama = modelEditSiswa.Nama;
                        siswa.Email = modelEditSiswa.Email;
                        siswa.Fk_Id_Jenis_Kelamin = modelEditSiswa.Fk_Id_Jenis_Kelamin;
                        siswa.Fk_Id_Kelas = modelEditSiswa.Fk_Id_Kelas;
                    }
                    else
                    {
                        siswa.Nomor_Induk = modelEditSiswa.Nomor_Induk;
                        siswa.Nama = modelEditSiswa.Nama;
                        siswa.Email = modelEditSiswa.Email;
                        siswa.Fk_Id_Jenis_Kelamin = modelEditSiswa.Fk_Id_Jenis_Kelamin;
                        siswa.Password = Crypto.Hash(modelEditSiswa.Password);
                        siswa.Fk_Id_Kelas = modelEditSiswa.Fk_Id_Kelas;
                    }

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data siswa" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }

        [HttpPost]
        public JsonResult EditAuditTrail(AuditTrailModel ATModel)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var auditTrail = db.Tbl_AuditTrail.Where(x => x.Pk_Id_AuditTrail == ATModel.Pk_Id_AuditTrail).FirstOrDefault();
                    auditTrail.Error_action = ATModel.Error_action;

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil menambahkan Action" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Siswa";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }


        public ActionResult Report_JmlSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            string ssrsurl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1300);
            report.Height = Unit.Pixel(650);
            report.ServerReport.ReportServerUrl = new Uri(ssrsurl);
            report.ServerReport.ReportPath = "/WiaClass/JumlahSiswaWIA";
            ViewBag.ReportViewer = report;

            return View();
        }
        // Admin Sekolah Controller

        public ActionResult ProfilAdminSekolah()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
            var viewModel = new ProfileModel()
            {
                Nama = sekolah.Nama_Sekolah,
                Image = sekolah.Logo,
                No_Telp = sekolah.No_Telp,
                Email = sekolah.Email,
                Alamat = sekolah.Alamat
            };
            return View(viewModel);
        }

        public ActionResult EditProfilAdminSekolah()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            var userId = (int)Session["userId"];
            int profilId = Convert.ToInt32(Session["userId"].ToString());
            var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
            var viewModel = new ProfileModel()
            {
                Pk_Id_Sekolah = sekolah.Pk_Id_Sekolah,
                Nama = sekolah.Nama_Sekolah,
                Image = sekolah.Logo,
                No_Telp = sekolah.No_Telp,
                Email = sekolah.Email,
                Alamat = sekolah.Alamat
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditProfilSA(CrudSekolahModel modelEditSekolah)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == modelEditSekolah.Pk_Id_Sekolah).FirstOrDefault();
                    sekolah.Nama_Sekolah = modelEditSekolah.Nama_Sekolah;
                    sekolah.No_Telp = modelEditSekolah.No_Telp;
                    sekolah.Email = modelEditSekolah.Email;
                    sekolah.Alamat = modelEditSekolah.Alamat;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Profil Admin";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return RedirectToAction("Profil");
                }
        }

        [HttpPost]
        public ActionResult EditFotoProfilSA(editFotoProfilModel model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var oldPicture = model.oldPicture;
                var newPicture = (Session["userId"] + "_" + file.FileName);
                try
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    file.SaveAs(Server.MapPath("~/assets/img/profile/" + Session["userId"] + "_" + file.FileName));
                    //Delete old Picture
                    if (newPicture != oldPicture)
                    {
                        string oldPictPath = Request.MapPath("~/assets/img/profile/" + oldPicture);
                        if (System.IO.File.Exists(oldPictPath))
                        {
                            System.IO.File.Delete(oldPictPath);
                        }
                    }

                    int profilId = Convert.ToInt32(Session["userId"].ToString());
                    var sekolah = db.Tbl_Sekolah.Where(x => x.Pk_Id_Sekolah == profilId).FirstOrDefault();
                    sekolah.Logo = (Session["userId"] + "_" + file.FileName);
                    db.SaveChanges();
                    transaction.Commit();
                    Session["image"] = newPicture;
                    return RedirectToAction("Profil");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Kelas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return RedirectToAction("Profil");
                }
            }
        }

        // Guru Controller

        [HttpPost]
        public JsonResult EditMapel(CrudMapelModel modelEditMapel)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var mapel = db.Tbl_Mata_Pelajaran.Where(x => x.Pk_Id_Mata_Pelajaran == modelEditMapel.Pk_Id_Mata_Pelajaran).FirstOrDefault();

                    mapel.Kode_Mata_Pelajaran = modelEditMapel.Kode_Mata_Pelajaran;
                    mapel.Nama_Mata_Pelajaran = modelEditMapel.Nama_Mata_Pelajaran;

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data mata pelajaran" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Mata Pelajaran";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }
        [HttpPost]
        public JsonResult EditKelas(CrudKelasModel modelEditKelas)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var kelas = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == modelEditKelas.Pk_Id_Kelas).FirstOrDefault();

                    kelas.Nama_Kelas = modelEditKelas.Nama_Kelas;

                    db.SaveChanges();
                    transaction.Commit();
                    return Json(new { message = "Berhasil mengubah data kelas" });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Kelas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return Json(new { message = "Terjadi Kesalahan : " + Error_msg.Message.ToString() });
                }
        }
        public ActionResult TestimonialGuru()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult Video()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult Kehadiran()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult JadwalKelas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        [HttpGet]
        public ActionResult BeriTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        /*public ActionResult MenilaiTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }*/

        /*public ActionResult BeriQuiz()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }*/

        [HttpGet]
        public ActionResult TambahKategori()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            /* var userId = (int)Session["userId"];
             int guruid = Convert.ToInt32(Session["userId"].ToString());
             List<Tbl_Kategori> li = db.Tbl_Kategori.Where(x => x.Fk_Id_Guru == guruid).OrderByDescending(x => x.Pk_Id_Kategori).ToList();
             ViewData["list"] = li;*/
            return View();
        }

        public JsonResult DataKategori()
        {

            db.Configuration.ProxyCreationEnabled = false;
            var userId = (int)Session["userId"];
            //TempData["sidebar"] = "SidebarGuru";
            int guruid = Convert.ToInt32(Session["userId"].ToString());
            var kategori = db.Tbl_Kategori.Where(x => x.Fk_Id_Guru == guruid).ToList();
            return Json(new { data = kategori }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetKategoriID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditKategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori.Equals(ID)).FirstOrDefault();
            return Json(EditKategori, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TambahDataKategori(CrudQuizModel modelKategori)
        {
            var userId = (int)Session["userId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Kategori tbl_kategori = new Tbl_Kategori();

                    tbl_kategori.KatName = modelKategori.KatName;
                    tbl_kategori.Fk_Id_Guru = userId;
                    tbl_kategori.Fk_Id_Kelas = modelKategori.Fk_Id_Kelas;
                    tbl_kategori.StatusQuiz = modelKategori.StatusQuiz;
                    tbl_kategori.MulaiQuiz = modelKategori.MulaiQuiz;
                    tbl_kategori.BatasQuiz = modelKategori.BatasQuiz;

                    db.Tbl_Kategori.Add(tbl_kategori);
                    db.Entry(tbl_kategori).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("TambahKategori");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data kategori";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return RedirectToAction("TambahKategori");
                }
        }

        [HttpPost]
        public ActionResult DeleteKategori(int id, Tbl_Kategori kategori)
        {
            try
            {
                kategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == id).FirstOrDefault();
                var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == id);

                db.Tbl_Pertanyaan.RemoveRange(pertanyaan);
                db.Tbl_Kategori.Remove(kategori);
                db.SaveChanges();
                return RedirectToAction("TambahKategori");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult EditKategori(CrudQuizModel modelEditKategori)
        {
            var userId = (int)Session["userId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var kategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == modelEditKategori.Pk_Id_Kategori).FirstOrDefault();
                    kategori.KatName = modelEditKategori.KatName;
                    kategori.Fk_Id_Guru = userId;
                    kategori.Fk_Id_Kelas = modelEditKategori.Fk_Id_Kelas;
                    kategori.StatusQuiz = modelEditKategori.StatusQuiz;
                    kategori.MulaiQuiz = modelEditKategori.MulaiQuiz;
                    kategori.BatasQuiz = modelEditKategori.BatasQuiz;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("TambahKategori");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit kategori";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    return RedirectToAction("TambahKategori");
                }
        }

        [HttpGet]
        public ActionResult ViewAllQuestions(int id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };
            var kategori = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == id).FirstOrDefault();
            if (kategori == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                Session["katId"] = kategori.Pk_Id_Kategori;
                var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == id).ToList();
                if (pertanyaan == null)
                {
                    return View("~/Views/Home/NotFound.cshtml");
                }
                else
                {
                    return View();
                }
            }
        }

        public JsonResult DataPertanyaan()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var katId = (int)Session["katId"];
            //TempData["sidebar"] = "SidebarGuru";
            //int guruid = Convert.ToInt32(Session["userId"].ToString());
            var kategori = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == katId).ToList();
            return Json(new { data = kategori }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPertanyaanID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditPertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan.Equals(ID)).FirstOrDefault();
            return Json(EditPertanyaan, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TambahDataPertanyaan(CrudQuestionModel modelPertanyaan)
        {
            var katId = (int)Session["katId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    Tbl_Pertanyaan tbl_pertanyaan = new Tbl_Pertanyaan();

                    tbl_pertanyaan.Pertanyaan = modelPertanyaan.Pertanyaan;
                    tbl_pertanyaan.OptA = modelPertanyaan.OptA;
                    tbl_pertanyaan.OptB = modelPertanyaan.OptB;
                    tbl_pertanyaan.OptC = modelPertanyaan.OptC;
                    tbl_pertanyaan.OptD = modelPertanyaan.OptD;
                    tbl_pertanyaan.CorrectOpt = modelPertanyaan.CorrectOpt;
                    tbl_pertanyaan.Fk_Id_Kategori = katId;

                    db.Tbl_Pertanyaan.Add(tbl_pertanyaan);
                    db.Entry(tbl_pertanyaan).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("ViewAllQuestions", new { id = katId });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Kategori";
                    string User_login = "Hardcode user";
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        [HttpPost]
        public ActionResult DeletePertanyaan(int id, Tbl_Pertanyaan pertanyaan)
        {
            try
            {
                pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan == id).FirstOrDefault();
                db.Tbl_Pertanyaan.Remove(pertanyaan);
                db.SaveChanges();
                return RedirectToAction("TambahKategori");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult EditPertanyaan(CrudQuestionModel modelEditPertanyaan)
        {
            var katId = (int)Session["katId"];
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan == modelEditPertanyaan.Pk_Id_Pertanyaan).FirstOrDefault();
                    pertanyaan.Pertanyaan = modelEditPertanyaan.Pertanyaan;
                    pertanyaan.OptA = modelEditPertanyaan.OptA;
                    pertanyaan.OptB = modelEditPertanyaan.OptB;
                    pertanyaan.OptC = modelEditPertanyaan.OptC;
                    pertanyaan.OptD = modelEditPertanyaan.OptD;
                    pertanyaan.CorrectOpt = modelEditPertanyaan.CorrectOpt;
                    pertanyaan.Fk_Id_Kategori = katId;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("ViewAllQuestions", new { id = katId });
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Kategori";
                    string User_login = "Hardcode user";
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        public JsonResult DataTugas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idguru = db.Tbl_Guru.Where(x => x.Pk_Id_Guru.Equals(userid)).FirstOrDefault();
            var tugas = db.Vw_Tugas.Where(x => x.Nama_Guru == idguru.Nama).ToList();
            return Json(new { data = tugas }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTugasID(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var EditTugas = db.Tbl_Tugas.Where(x => x.Id_Tugas.Equals(ID)).FirstOrDefault();
            return Json(EditTugas, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TambahTugas(CrudBeriTugas modelTugas)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Tugas tugas = new Tbl_Tugas();
                    tugas.Nama_Tugas = modelTugas.Nama_Tugas;
                    tugas.Waktu_Pemberian = modelTugas.Waktu_Pemberian;
                    tugas.Batas_Pengumpulan = modelTugas.Batas_Pengumpulan;
                    tugas.Fk_Id_Kelas = modelTugas.Fk_Id_Kelas;
                    tugas.Fk_Id_Guru = Convert.ToInt32(Session["userId"]);
                    db.Tbl_Tugas.Add(tugas);
                    db.Entry(tugas).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("BeriTugas");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data Tugas";
                    string User_login = "Hardcode user";
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
            }
        }



        [HttpPost]
        public ActionResult DeleteTugas(int id, Tbl_Tugas tugas)
        {
            try
            {
                // TODO: Add delete logic here
                tugas = db.Tbl_Tugas.Where(x => x.Id_Tugas == id).FirstOrDefault();
                db.Tbl_Tugas.Remove(tugas);
                db.SaveChanges();
                return RedirectToAction("BeriTugas");
            }
            catch
            {
                return View();
            }
        }



        [HttpPost]
        public ActionResult EditTugas(CrudBeriTugas modelEditTugas)
        {
            using (var transaction = db.Database.BeginTransaction())
                try
                {
                    int userid = Convert.ToInt32(Session["userId"]);
                    var tugas = db.Tbl_Tugas.Where(x => x.Id_Tugas == modelEditTugas.Id_Tugas).FirstOrDefault();
                    tugas.Nama_Tugas = modelEditTugas.Nama_Tugas;
                    tugas.Waktu_Pemberian = modelEditTugas.Waktu_Pemberian;
                    tugas.Batas_Pengumpulan = modelEditTugas.Batas_Pengumpulan;
                    tugas.Fk_Id_Kelas = modelEditTugas.Fk_Id_Kelas;
                    tugas.Fk_Id_Guru = userid;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("BeriTugas");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Tugas";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
        }

        public ActionResult NilaiHarian()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        //public ActionResult MemberiNilaiHarian(CrudNilaiTugas model, int id_tugas)
        //{
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            Tbl_Nilai tbl_nilai = new Tbl_Nilai();
        //            tbl_nilai.Nilai = Convert.ToDouble(model.Nilai);
        //            tbl_nilai.Status = model.Status;
        //            tbl_nilai.Fk_Id_Sekolah = 0;

        //        }
        //        catch (Exception msg)
        //        {

        //        }
        //    }
        //}

        public ActionResult Evaluasi()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        // Siswa Controller

        public ActionResult KehadiranSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            ViewBag.Message = "Pencatatan Kehadiran";
            return View();
        }

        public ActionResult JoinVideo()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            ViewBag.Message = "Bergabung kelas virtual";
            return View();
        }

        public ActionResult EvaluasiSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            ViewBag.Message = "Evaluasi Siswa";
            return View();
        }

        public ActionResult KirimTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            return View();
        }

        public ActionResult Quiz(int id)
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            Session["quizId"] = id;
            var kuis = db.Tbl_Kategori.Where(x => x.Pk_Id_Kategori == id).FirstOrDefault();
            if (kuis == null)
            {
                return View("~/Views/Home/NotFound.cshtml");
            }
            else
            {
                List<Tbl_Pertanyaan> li = db.Tbl_Pertanyaan.Where(x => x.Fk_Id_Kategori == id).ToList();
                Queue<Tbl_Pertanyaan> queue = new Queue<Tbl_Pertanyaan>();
                foreach (Tbl_Pertanyaan a in li)
                {
                    queue.Enqueue(a);
                }
                TempData["pertanyaan"] = queue;
                TempData["nilai"] = 0;
                TempData.Keep();
                return RedirectToAction("QuizStart");
            }

        }
        public ActionResult QuizStart()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };
            TempData.Keep();
            Tbl_Pertanyaan p = null;
            if (TempData["pertanyaan"] != null)
            {
                Queue<Tbl_Pertanyaan> qlist = (Queue<Tbl_Pertanyaan>)TempData["pertanyaan"];
                if (qlist.Count > 0)
                {
                    p = qlist.Peek();
                    qlist.Dequeue();
                    TempData["pId"] = p.Pk_Id_Pertanyaan;
                    TempData["pertanyaan"] = qlist;
                    TempData.Keep();
                    return View(p);
                }
                else
                {
                    return RedirectToAction("EndQuiz");
                }
            }
            else
            {
                return RedirectToAction("SelectQuiz");
            }
        }

        [HttpPost]
        public ActionResult QuizStart(Tbl_Pertanyaan p)
        {
            int pId = (int)TempData["pId"];
            var pertanyaan = db.Tbl_Pertanyaan.Where(x => x.Pk_Id_Pertanyaan == pId).FirstOrDefault();
            string studentanswer = null;

            if (p.OptA != null)
            {
                studentanswer = "A";
            }
            else if (p.OptB != null)
            {
                studentanswer = "B";
            }
            else if (p.OptC != null)
            {
                studentanswer = "C";
            }
            else if (p.OptD != null)
            {
                studentanswer = "D";
            }

            if (studentanswer.Equals(pertanyaan.CorrectOpt))
            {
                TempData["nilai"] = Convert.ToInt32(TempData["nilai"]) + 1;
            }

            TempData.Keep();

            return RedirectToAction("QuizStart");
        }

        public ActionResult SelectQuiz()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            ViewBag.Message = "Management data Quiz";
            return View();
        }

        public JsonResult DataKuis()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = (int)Session["userId"];
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var siswa_kelas = idsiswa.Fk_Id_Kelas;
            //var sudah = db.Tbl_QuizNilai.Where(x => x.Fk_Id_Siswa == userid).FirstOrDefault();
            List<Tbl_Kategori> kuis = db.Tbl_Kategori.Where(x => x.Fk_Id_Kelas == siswa_kelas && x.StatusQuiz == true).ToList();
            return Json(new { data = kuis }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EndQuiz()
        {

            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    int userId = (int)Session["userId"];
                    int quizId = (int)Session["quizId"];
                    Tbl_QuizNilai nilai = new Tbl_QuizNilai();
                    nilai.Tanggal = DateTime.Now;
                    nilai.NilaiQuiz = (int)TempData["nilai"];
                    nilai.Fk_Id_Kategori = quizId;
                    nilai.Fk_Id_Siswa = userId;
                    db.Tbl_QuizNilai.Add(nilai);
                    db.Entry(nilai).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return View();
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();
                    var Error_msg = msg;
                    string Error_action = "Belum ada action";
                    string Error_section = "Tambah data nilai kuis";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.Message.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = msg;
                    return View(msg);
                }
            }
        }

        public JsonResult DataTugasSiswa()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var kelasSiswa = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == idsiswa.Fk_Id_Kelas).FirstOrDefault();
            var namaKelas = kelasSiswa.Nama_Kelas.ToString();
            var tugas = db.Vw_Tugas.Where(x => x.Nama_Kelas.Equals(namaKelas)).ToList();
            return Json(new { data = tugas }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MengirimTugas(UploadViewModel model, int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var file = model.ImageFile;
                var userid = (int)Session["userId"];
                var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
                var idsekolah = Convert.ToInt32(idsiswa.Fk_Id_Sekolah);
                int siswa_kelas = Convert.ToInt32(idsiswa.Fk_Id_Kelas);
                var tugas = db.Vw_Tugas.Where(x => x.Id_Tugas.Equals(id)).FirstOrDefault();
                if (file != null)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var extention = Path.GetExtension(file.FileName);
                    var filenamewithoutextension = Path.GetFileNameWithoutExtension(file.FileName);
                    file.SaveAs(Server.MapPath("~/UploadTugas/" + siswa_kelas + "/" + Session["userId"] + "_" + file.FileName));
                    var url_file = ("UploadTugas/" + siswa_kelas + "/" + Session["userId"] + "_" + file.FileName).ToString();

                    Tbl_Pengumpulan_Tugas tbl_kumpul_tugas = new Tbl_Pengumpulan_Tugas();
                    tbl_kumpul_tugas.Waktu_Pengumpulan = DateTime.Now;
                    tbl_kumpul_tugas.Url_File = url_file;
                    tbl_kumpul_tugas.Fk_Id_Sekolah = idsekolah;
                    tbl_kumpul_tugas.Fk_Id_Siswa = userid;
                    tbl_kumpul_tugas.Fk_Id_Tugas = Convert.ToInt32(tugas.Id_Tugas);

                    db.Tbl_Pengumpulan_Tugas.Add(tbl_kumpul_tugas);
                    db.Entry(tbl_kumpul_tugas).State = EntityState.Added;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("KirimTugas");
                }
                else
                {
                    transaction.Rollback();
                    WiaClassEntities db2 = new WiaClassEntities();

                    var Error_msg = "Error";
                    string Error_action = "Belum ada action";
                    string Error_section = "Edit Foto Profil";
                    string User_login = (string)Session["nama"];
                    Tbl_AuditTrail AuditTrail = new Tbl_AuditTrail
                    {
                        Error_msg = Error_msg.ToString(),
                        User_login = User_login,
                        Error_date = DateTime.Now,
                        Error_action = Error_action,
                        Error_section = Error_section
                    };
                    db2.Entry(AuditTrail).State = EntityState.Added;
                    db2.SaveChanges();
                    ViewBag.Message = Error_msg;
                }
                return View();
            }
        }

        [HttpGet]
        public ActionResult HistoriTugas()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            ViewBag.Message = "Histori Pengiriman Tugas";
            return View();
        }

        public JsonResult DataKirimTugas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = Convert.ToInt32(Session["userId"]);
            var idsiswa = db.Tbl_Siswa.Where(x => x.Pk_Id_Siswa.Equals(userid)).FirstOrDefault();
            var kelasSiswa = db.Tbl_Kelas.Where(x => x.Pk_Id_Kelas == idsiswa.Fk_Id_Kelas).FirstOrDefault();
            var namaKelas = kelasSiswa.Nama_Kelas.ToString();
            var hist_tugas = db.Vw_KirimTugas.Where(x => x.Nama_Kelas == namaKelas && x.Nama_Siswa == idsiswa.Nama).ToList();
            return Json(new { data = hist_tugas }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ReportGuru()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            string ssrsurl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1300);
            report.Height = Unit.Pixel(650);
            report.ServerReport.ReportServerUrl = new Uri(ssrsurl);
            report.ServerReport.ReportPath = "/WiaClass/DataGuruSD";

            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult ReportSiswa()
        {
            ViewBag.sidebar = new SidebarModel()
            {
                Email = (string)Session["email"],
                Img = (string)Session["image"],
                Nama = (string)Session["nama"],
                sb = "SidebarSA",
                controllerName = "SA"
            };

            string ssrsurl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1300);
            report.Height = Unit.Pixel(650);
            report.ServerReport.ReportServerUrl = new Uri(ssrsurl);
            report.ServerReport.ReportPath = "/WiaClass/DataSiswaSD";

            ViewBag.ReportViewer = report;
            return View();
        }
    }
}