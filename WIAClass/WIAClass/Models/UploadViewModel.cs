﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class UploadViewModel
    {
        public HttpPostedFileBase ImageFile { get; set; }
        
        public string oldFile { get; set; }

    }
}