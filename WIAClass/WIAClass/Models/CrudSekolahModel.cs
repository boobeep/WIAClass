﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class CrudSekolahModel
    {
        public int Pk_Id_Sekolah { get; set; }
        public string Nama_Sekolah { get; set; }
        public string No_Telp { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Alamat { get; set; }
    }

    public class CrudGuruModel
    {
        public int Pk_Id_Guru { get; set; }
        public string NIP { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }
        public string No_Telp { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public string Password { get; set; }
        public Nullable<int> Fk_Id_Mata_Pelajaran { get; set; }
        public Nullable<int> Fk_Id_Jenis_Kelamin { get; set; }
        public virtual Tbl_Jenis_Kelamin Tbl_Jenis_Kelamin { get; set; }
    }

    public class CrudSiswaModel
    {
        public int Pk_Id_Siswa { get; set; }
        public string Nomor_Induk { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public Nullable<int> Fk_Id_Kelas { get; set; }
        public Nullable<int> Fk_Id_Jenis_Kelamin { get; set; }
        public virtual Tbl_Jenis_Kelamin Tbl_Jenis_Kelamin { get; set; }
    }

    public class CrudMapelModel
    {
        public int Pk_Id_Mata_Pelajaran { get; set; }
        public string Kode_Mata_Pelajaran { get; set; }
        public string Nama_Mata_Pelajaran { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
    }

    public class CrudKelasModel
    {
        public int Pk_Id_Kelas { get; set; }
        public string Nama_Kelas { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
    }

    public class cmsTestimonial
    {
        public string Testimoni { get; set; }
    }

    public class KontakKamiModel
    {
        public int Pk_Id_Kontak_Kami { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }
        public string Subjek { get; set; }
        public string Pesan { get; set; }
    }
}