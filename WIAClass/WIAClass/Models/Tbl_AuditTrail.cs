//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WIAClass.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_AuditTrail
    {
        public int Pk_Id_AuditTrail { get; set; }
        public string Error_msg { get; set; }
        public string User_login { get; set; }
        public Nullable<System.DateTime> Error_date { get; set; }
        public string Error_section { get; set; }
        public string Error_action { get; set; }
    }
}
