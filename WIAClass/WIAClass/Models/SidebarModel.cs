﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class SidebarModel
    {
        public string Img { get; set; }
        public string Email { get; set; }
        public string sb { get; set; }
        public string Nama { get; set; }
        public string controllerName { get; set; }
    }
}