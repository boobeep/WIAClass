﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class CrudEvaluasi
    {
        [Required]
        public int Nilai { get; set; }

        [Required]
        public string Evaluasi { get; set; }

        public Nullable<System.DateTime> Tanggal_Evaluasi { get; set; }
    }
}