//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WIAClass.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Mata_Pelajaran
    {
        public int Pk_Id_Mata_Pelajaran { get; set; }
        public string Kode_Mata_Pelajaran { get; set; }
        public string Nama_Mata_Pelajaran { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
    }
}
