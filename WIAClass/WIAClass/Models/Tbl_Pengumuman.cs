//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WIAClass.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Pengumuman
    {
        public int Pk_Id_Pengumuman { get; set; }
        public string Judul_Pengumuman { get; set; }
        public string Pengumuman { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public Nullable<System.DateTime> Waktu { get; set; }
        public string Background { get; set; }
        public Nullable<bool> status { get; set; }
        public string file_Tambahan { get; set; }
    }
}
