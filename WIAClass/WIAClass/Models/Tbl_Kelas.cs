//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WIAClass.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Kelas
    {
        public int Pk_Id_Kelas { get; set; }
        public string Nama_Kelas { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
    }
}
