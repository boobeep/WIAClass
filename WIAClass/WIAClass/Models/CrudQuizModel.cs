﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class CrudQuizModel
    {
        public int Pk_Id_Kategori { get; set; }
        public string KatName { get; set; }
        public Nullable<int> Fk_Id_Guru { get; set; }
        public Nullable<int> Fk_Id_Kelas { get; set; }
        public Nullable<bool> StatusQuiz { get; set; }
        public Nullable<DateTime> MulaiQuiz { get; set; }
        public Nullable<DateTime> BatasQuiz { get; set; }
        public Nullable<int> WaktuPengerjaan { get; set; }
    }

    public class CrudQuestionModel
    {
        public int Pk_Id_Pertanyaan { get; set; }
        public string Pertanyaan { get; set; }
        public string OptA { get; set; }
        public string OptB { get; set; }
        public string OptC { get; set; }
        public string OptD { get; set; }
        public string CorrectOpt { get; set; }
        public Nullable<int> Fk_Id_Kategori { get; set; }

        public virtual Tbl_Kategori Tbl_Kategori { get; set; }
    }

}