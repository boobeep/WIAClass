﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class ProfileModel
    {
        public int Pk_Id_Sekolah { get; set; }
        public string Nama { get; set; }
        public string Image { get; set; }
        public string No_Telp { get; set; }
        public string Email { get; set; }
        public string Alamat { get; set; }
    }

    public class ProfileModelGuru
    {
        public int Pk_Id_Guru { get; set; }
        public string NIP { get; set; }
        public string Nama { get; set; }
        public string Foto_Profil { get; set; }
        public string Email { get; set; }
        public string No_Telp { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public Nullable<int> Fk_Id_Mata_Pelajaran { get; set; }
        public string Jenis_Kelamin { get; set; }
    }

    public class ProfileModelSiswa
    {
        public int Pk_Id_Siswa { get; set; }
        public string Nomor_Induk { get; set; }
        public string Nama { get; set; }
        public string Foto_Profil { get; set; }
        public string Email { get; set; }
        public Nullable<int> Fk_Id_Kelas { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public string Jenis_Kelamin { get; set; }
        public string sekolah { get; set; }
    }


    public class editFotoProfilModel
    {
        public HttpPostedFileBase ImageFile { get; set; }
        public string oldPicture { get; set; }
    }
}