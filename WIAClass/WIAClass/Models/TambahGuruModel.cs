﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class TambahGuruModel
    {
        public string NIP { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string No_Telp { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public string Fk_Id_Mata_Pelajaran { get; set; }
        public Nullable<int> Fk_Id_Jenis_Kelamin { get; set; }
        public Nullable<int> Role { get; set; }

        public virtual Tbl_Jenis_Kelamin Tbl_Jenis_Kelamin { get; set; }
    }

    public class TambahSiswaModel
    {
        public string Nomor_Induk { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public Nullable<int> Fk_Id_Kelas { get; set; }
        public Nullable<int> Fk_Id_Jenis_Kelamin { get; set; }
        public Nullable<int> Role { get; set; }

        public virtual Tbl_Jenis_Kelamin Tbl_Jenis_Kelamin { get; set; }
    }

    public class TambahMapelModel
    {
        public string Pk_Id_Mata_Pelajaran { get; set; }
        public string Nama_Mata_Pelajaran { get; set; }
    }

}