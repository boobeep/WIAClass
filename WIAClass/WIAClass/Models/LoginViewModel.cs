using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class LoginViewModel
    {

        [Required]
        public string Email
        {
            set;
            get;
        }
        [Required]
        [DataType(DataType.Password)]
        public string Password
        {
            set;
            get;
        }
        public int RoleId 
        { 
            get; 
            set; 
        }

    }
}