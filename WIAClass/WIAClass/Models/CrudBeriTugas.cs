﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class CrudBeriTugas
    {
        public int Id_Tugas { get; set; }
        public string Nama_Tugas { get; set; }
        public DateTime Waktu_Pemberian { get; set; }
        public DateTime Batas_Pengumpulan { get; set; }
        public int Fk_Id_Kelas { get; set; }
        public int Pk_Id_Nilai { get; set; }
        public double Nilai { get; set; }
        public string Status { get; set; }
        public int Fk_Id_Sekolah { get; set; }
        public int Fk_Id_Siswa { get; set; }
        public string Fk_Id_Mata_Pelajaran { get; set; }
        public int Fk_Id_Guru { get; set; }
    }

    public class CrudBeriNilai
    {
        public int Pk_Id_Pengumpulan_Tugas { get; set; }
        public Nullable<System.DateTime> Waktu_Pengumpulan { get; set; }
        public string Url_File { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public Nullable<int> Fk_Id_Tugas { get; set; }
        public Nullable<int> Fk_Id_Siswa { get; set; }
        public Nullable<double> Nilai { get; set; }
        public string Status { get; set; }
        public string Cat { get; set; }
        public int Temporary { get; set; }
    }
}