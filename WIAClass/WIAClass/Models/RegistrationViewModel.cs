﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class RegistrationViewModel
    {
        public int Id_Sekolah { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        public string Nama_Sekolah { get; set; }
        public string Logo { get; set; }
        public string No_Telp { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        public string Alamat { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        public string KonfirmasiPassword { get; set; }

        public Nullable<int> Role { get; set; }
        public bool Activated { get; set; }
        public Guid ActivationCode { get; set; }
    }
}