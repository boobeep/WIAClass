﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class CrudPengumuman
    {
        public int Pk_Id_Pengumuman { get; set; }
        public string Judul_Pengumuman { get; set; }
        public string Pengumuman { get; set; }
        public string Background { get; set; }
        public Nullable<int> Fk_Id_Sekolah { get; set; }
        public string file_Tambahan { get; set; }
    }
}