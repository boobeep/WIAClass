﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIAClass.Models
{
    public class RequestApprovalModel
    {
        public HttpPostedFileBase ImageFile { get; set; }
        public DateTime Waktu_Absensi { get; set; }
        public status Stats { get; set; }
    }

    public enum status
    {
        Hadir,
        Pulang,
        Sakit,
        Izin,
        Alpha
    }
}