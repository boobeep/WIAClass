﻿using System.Web;
using System.Web.Optimization;

namespace WIAClass
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.bundle.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/assets/adminlte/plugins/fontawesome-free/css/all.min.css",
                      "~/assets/adminlte/css/adminlte.min.css",
                      "~/Content/DataTables/css/dataTables.bootstrap4.min.css",
                      "~/Content/site.css",
                      "~/assets/adminlte/css/StyleCustom.css"));

            bundles.Add(new ScriptBundle("~/assets/adminlte/js").Include(
             "~/assets/adminlte/js/adminlte.min.js"));

            // Landing Page
            bundles.Add(new StyleBundle("~/LandingPage/css").Include(
                      "~/assets/LandingPage/bootstrap/css/bootstrap.min.css",
                      "~/assets/LandingPage/icofont/icofont.min.css",
                      "~/assets/LandingPage/boxicons/css/boxicons.min.css",
                      "~/assets/LandingPage/owl.carousel/assets/owl.carousel.min.css",
                      "~/assets/LandingPage/venobox/venobox.css",
                      "~/assets/LandingPage/aos/aos.css",
                      "~/assets/LandingPage/css/style.css"));

            bundles.Add(new ScriptBundle("~/LandingPage/js").Include(
                      "~/assets/LandingPage/jquery/jquery.min.js",
                      "~/assets/LandingPage/bootstrap/js/bootstrap.bundle.min.js",
                      "~/assets/LandingPage/jquery.easing/jquery.easing.min.js",
                      "~/assets/LandingPage/php-email-form/validate.js",
                      "~/assets/LandingPage/owl.carousel/owl.carousel.min.js",
                      "~/assets/LandingPage/venobox/venobox.min.js",
                      "~/assets/LandingPage/aos/aos.js",
                      "~/Scripts/Lp/Landing_main.js"));

            bundles.Add(new ScriptBundle("~/Scripts/DataTables").Include(
                    "~/Scripts/DataTables/jquery.dataTables.min.js",
                    "~/Scrips/DataTables/dataTables.bootstrap4.min.js"
                ));
        }
    }
}
