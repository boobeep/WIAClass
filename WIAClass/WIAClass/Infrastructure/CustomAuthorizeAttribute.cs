﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WIAClass.Models;

namespace WIAClass.Infrastructure
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] allowedroles;
        public CustomAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            var userId = Convert.ToString(httpContext.Session["RoleId"]);
            if (!string.IsNullOrEmpty(userId))
                using (var context = new WiaClassEntities())
                {
                    int ID = int.Parse(userId);
                    var userRole = context.Vw_User.Where(x => x.Role == ID).FirstOrDefault();
                    foreach (var role in allowedroles)
                    {
                        if (role == userRole.Role.ToString()) return true;
                    }
                }


            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
               new RouteValueDictionary
               {
                    { "controller", "Home" },
                    { "action", "UnAuthorized" }
               });
        }
    }
}