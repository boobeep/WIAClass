restore database WiaClass
from disk = 'D:\Wildan\CodingID\Project_WIAClass\WIAClass\WIAClass\assets\DbBackup\WiaClass.bak'

use WiaClass
select * from Tbl_Sekolah

alter table Tbl_Sekolah
alter column Password varchar(300)

delete from Tbl_Sekolah
where Pk_Id_Sekolah = 46;

alter table Tbl_Sekolah
add Activated bit not null
default 0

alter table Tbl_Sekolah
add ActivationCode uniqueidentifier not null

alter table Tbl_Sekolah
drop constraint [DF__Tbl_Sekol__Activ__02084FDA]

alter table Tbl_Sekolah
drop column ActivationCode

------- Update Table Database dari Agung Tanggal 04/08/2021 ------

DROP TABLE Tbl_User_Role
DROP TABLE Tbl_User_Menu
DROP TABLE Tbl_User_SubMenu

DROP TABLE Tbl_Absensi
CREATE TABLE Tbl_Absensi (
	Pk_Id_Absensi int identity not null primary key,
	Tanggal datetime,
	Status varchar (20),
	Fk_Id_Sekolah int,
	Fk_Id_Siswa int,
	Fk_Id_Mata_Pelajaran varchar (50),
	Fk_Id_Guru int
);

DROP TABLE Tbl_Nilai
CREATE TABLE Tbl_Nilai (
	Pk_Id_Nilai int identity not null primary key,
	Nilai Float,
	Status varchar (20),
	Fk_Id_Sekolah int,
	Fk_Id_Siswa int,
	Fk_Id_Mata_Pelajaran varchar (50),
	Fk_Id_Guru int,
	Fk_Id_Tugas int
);

DROP TABLE Tbl_Guru
CREATE TABLE Tbl_Guru (
	Pk_Id_Guru int identity not null primary key,
	NIP int,
	Nama varchar (50),
	Email varchar (100),
	Password varchar (300),
	No_Telp int,
	Fk_Id_Sekolah int,
	Fk_Id_Mata_Pelajaran varchar (50),
	Fk_Id_Jenis_Kelamin INT FOREIGN KEY REFERENCES Tbl_Jenis_Kelamin(Id_Jenis_Kelamin),
	Role int
);

DROP TABLE Tbl_Pengumpulan_Tugas
CREATE TABLE Tbl_Pengumpulan_Tugas (
	Pk_Id_Pengumpulan_Tugas int identity not null primary key,
	Waktu_Pengumpulan datetime,
	Url_File varchar (100),
	Fk_Id_Sekolah int,
	Fk_Id_Tugas int,
	Fk_Id_Siswa int,
);

DROP TABLE Tbl_Siswa
CREATE TABLE Tbl_Siswa (
	Pk_Id_Siswa int identity not null primary key,
	Nomor_Induk int,
	Nama varchar (50),
	Email varchar (100),
	Password varchar (300),
	Fk_Id_Sekolah int,
	Fk_Id_Kelas int,
	Fk_Id_Jenis_Kelamin INT FOREIGN KEY REFERENCES Tbl_Jenis_Kelamin(Id_Jenis_Kelamin),
	Role int
);

sp_RENAME 'Tbl_Sekolah.Id_Sekolah' , 'Pk_Id_Sekolah', 'COLUMN'
sp_RENAME 'Tbl_Mata_Pelajaran.Id_Mata_Pelajaran' , 'Pk_Id_Mata_Pelajaran', 'COLUMN'
sp_RENAME 'Tbl_Kelas.Id_Kelas' , 'Pk_Id_Kelas', 'COLUMN'
sp_RENAME 'Tbl_Jenis_Kelamin.Id_Jenis_Kelamin' , 'Pk_Id_Jenis_Kelamin', 'COLUMN'

INSERT INTO Tbl_Jenis_Kelamin (Jenis_Kelamin) VALUES
	('Laki-Laki'),
	('Perempuan');

--------- Update, membuat view untuk login setiap role (Wildan 06/08/2021)----------
alter table Tbl_Guru
add Activated bit not null
default 1
with values

alter table Tbl_Siswa
add Activated bit not null
default 1
with values

Create View Vw_User AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Email )) AS int), 0) AS ID, Email, Password, Role, Activated
FROM(
    Select Email, Password, Role, Activated From Tbl_Sekolah
    Union 
    Select Email, Password, Role, Activated From Tbl_Guru 
	Union
	Select Email, Password, Role, Activated From Tbl_Siswa) AS MyResult

select * from Vw_User

--------------Update, alter table sekolah reset password code (Imam 09/08/2021)--------------------

ALTER TABLE Tbl_Sekolah
ADD ResetPasswordCode varchar(500) DEFAULT NULL

ALTER TABLE Tbl_Guru
ALTER COLUMN NIP varchar(50)

ALTER TABLE Tbl_Guru
ALTER COLUMN No_Telp varchar(50)

ALTER TABLE Tbl_Sekolah
ALTER COLUMN No_Telp varchar(50)

ALTER TABLE Tbl_Siswa
ALTER COLUMN Nomor_Induk varchar(50)

-------------- Akun untuk tiap role (Imam 12/08/2021) -------------------------

---- Super Admin : Email = wiaclassteam@gmail.com, Password = wiaclass ----
insert into Tbl_Sekolah (Nama_Sekolah, Logo, No_Telp, Email, Alamat, Password, Role, Activated, ActivationCode) VALUES 
('WiaClass', 'default.jpg', '1', 'wiaclassteam@gmail.com', 'Tangerang', 'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 1, 1, '00000000-0000-0000-0000-000000000000')

---- Admin Sekolah : Email = wiaschoolteam@gmail.com, Password = wiacalss ----
insert into Tbl_Sekolah (Nama_Sekolah, Logo, No_Telp, Email, Alamat, Password, Role, Activated, ActivationCode) VALUES 
('WiaSchool', 'default.jpg', '0212371675', 'wiaschoolteam@gmail.com', 'Tangerang', 'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 2, 1, '00000000-0000-0000-0000-000000000000')

---- Guru : Email = wiateacher@wiateacher.com, Password = wiaclass ----
insert into Tbl_Guru (NIP, Nama, Email, Password, No_Telp, Fk_Id_Sekolah, Fk_Id_Mata_Pelajaran, Fk_Id_Jenis_Kelamin, Role, Activated) VALUES
('196312241989032006', 'WiaTeacher', 'wiateacher@wiateacher.com', 'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', '08564836432', 30, 'Matematika1', 1, 3, 1)

---- Siswa : Email = wiastudent@wiastudent.com, Password = wiaclass ----
insert into Tbl_Siswa (Nomor_Induk, Nama, Email, Password, Fk_Id_Sekolah, Fk_Id_Kelas, Fk_Id_Jenis_Kelamin, Role, Activated) VALUES
('4673658348', 'WiaStudent', 'wiastudent@wiastudent.com', 'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 30, 5, 1, 4, 1)


------------ Tambah table untuk Kuis (Imam 13/08/2021) ------------------------

create table Tbl_Quiz
(
	Pk_Id_Quiz int identity primary key,
	QuizName varchar (300),
	QuizDate datetime,
	Quiz_Score INT,
	Fk_Id_Siswa int foreign key references Tbl_Siswa
)

create table Tbl_Pertanyaan
(
	Pk_Id_Pertanyaan int identity primary key,
	Pertanyaan varchar(500),
	OptA varchar(20),
	OptB varchar(20),
	OptC varchar(20),
	OptD varchar(20),
	CorrectOpt varchar(20),
)

create table Tbl_Kategori
(
	Pk_Id_Kategori int identity primary key,
	KatName varchar(50) not null,
	Fk_Id_Guru int foreign key references Tbl_Guru(Pk_Id_Guru)
)

alter table Tbl_Pertanyaan
add Fk_Id_Kategori int foreign key references Tbl_Kategori(Pk_Id_Kategori)


---------------- Edit View Vw_User (Imam 13/08/2021) ----------------

drop view Vw_User

Create View Vw_User AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Email )) AS int), 0) AS ID, Email, Password, Role, Activated, FK_ID
FROM(
Select Email, Password, Role, Activated, Pk_Id_Sekolah as FK_ID From Tbl_Sekolah
Union
Select Email, Password, Role, Activated, Pk_Id_Guru as FK_ID From Tbl_Guru
Union
Select Email, Password, Role, Activated, Pk_Id_Siswa as FK_ID From Tbl_Siswa) AS MyResult

select * from Vw_User


-------------------------Mengisi Tabel Master Kelas (Wildan 14/08/2021)-------------------------------

sp_RENAME 'Tbl_Tugas.Waktu_Pengumpulan' , 'Batas_Pengumpulan', 'COLUMN'

alter table Tbl_Tugas
add Fk_Id_Kelas int not null

alter table Tbl_Tugas
add Fk_Id_Guru int not null

------------------------ Membuat table Testimonial dan FAQ (Agung 16/08/2021) -------------------------

CREATE TABLE Tbl_Testimonial (
    Pk_Id_Testimonial int identity not null primary key,
    Nama varchar(100),
	Foto Varchar (50),
	Posisi_Role varchar(50),
    Testimoni varchar(800),
	Status bit
);

CREATE TABLE Tbl_FAQ (
	Pk_Id_FAQ int identity not null primary key,
    Pertanyaan varchar(500),
    Jawaban varchar(500),
	Status bit
);

------------------------ Mengisi data dummy tabel testimonial dan FAQ (Agung 16/08/2021) ---------------------------------
INSERT INTO Tbl_Testimonial VALUES 
('I Made Agung','default.jpg', 'Siswa', 'TERIMA KASIH WIA CLASS, MEMPERMUDAH BELAJAR', 1),
('Solihin, S.Pd','default.jpg', 'Guru', 'Dengan Wia Class saya menjadi lebih mudah mengajar murid-murid saya yang masih SD karena tombol-tombolnya mudah dimengerti',1),
('Erika Putri','default.jpg', 'Siswa', 'Lebih mudah daripada menggunakan banyak aplikasi', 1),
('Eti Haryani, S.Pd','default.jpg','Guru', 'Aplikasi yang sangat simpel namun mempermudah dalam mengajar', 1 );

INSERT INTO Tbl_FAQ VALUES
('Bagaimana cara menggunakan aplikasi WiaClass ?', 'Untuk cara penggunaan aplikasi Wiaclass dapat dilihat pada foto cara penggunaan pada halaman ini, namun apabila ada pertanyaan lebih lanjut dapat menghubungi kami pada halaman Contact', 1),
('Bagaimana jika saya seorang siswa ingin mendaftar WiaClass ?', 'Untuk memulai menggunakan aplikasi WiaClass, sekolah anda perlu untuk terdaftar terlebih dahulu pada aplikasi kami, anda dapat menghubungi guru maupun admin pada sekolah anda', 1),
('Apakah murid dan guru perlu untuk mendaftar ?', 'Untuk guru serta murid tidak perlu mendaftar karena data akan dimasukkan terlebih dahulu oleh admin sekolah anda', 1),
('Jika Saya mengalami error, kemana saya dapat menghubungi ?', 'Jika ada hal yang perlu diberitahukan seputar aplikasi WiaClass anda dapat menghubungi developer melalui Contact yang tertera pada halaman ini', 1),
('Apakah saya perlu membayar untuk menggunakan aplikasi ini ?', 'Aplikasi WiaClass tidak memungut biaya apapun dari seluruh kegiatan kelas yang anda lakukan, Terima Kasih sudah menggunakan Wia Class', 1);


---------------------- Edit Vw_User dan alter Tbl_Tugas (Wildan 17/08/2021) ----------------------------------------------

drop view Vw_User

Create View Vw_User AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Email )) AS int), 0) AS ID, Nama, Email, Password, Role, Activated, FK_ID, Foto_Profil
FROM(
Select Nama_Sekolah as Nama, Email, Password, Role, Activated, Pk_Id_Sekolah as FK_ID, Logo as Foto_Profil From Tbl_Sekolah
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Guru as FK_ID, Foto_Profil From Tbl_Guru
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Siswa as FK_ID, Foto_Profil From Tbl_Siswa) AS MyResult

alter table Tbl_Tugas
alter column Nama_Tugas varchar(500)

---------------------- Edit Tbl_Pertanyaan (Imam 18/08/2021) -----------------------------------

alter table Tbl_Pertanyaan
alter column Pertanyaan varchar(max)

alter table Tbl_Pertanyaan
alter column OptA varchar(max)

alter table Tbl_Pertanyaan
alter column OptB varchar(max)

alter table Tbl_Pertanyaan
alter column OptC varchar(max)

alter table Tbl_Pertanyaan
alter column OptD varchar(max)

alter table Tbl_Pertanyaan
alter column CorrectOpt varchar(max)

---------------------- Add kelas di Tbl_Kategori (Imam 19/08/2021) ----------------------------

alter table Tbl_Kategori
add Fk_Id_Kelas int

-----------------------Membuat view baru untuk laman siswa (Wildan 17/08/2021)------------------------------------

create view Vw_Tugas as 
select Id_Tugas, Nama_Tugas, Waktu_Pemberian, Batas_Pengumpulan, Fk_Id_Kelas as Kelas, Nama as Nama_Guru, Fk_Id_Mata_Pelajaran as Kode_Mapel
from Tbl_Tugas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru

create view Vw_KirimTugas as
select Id_Tugas, Nama_Tugas, Kode_Mapel, Batas_Pengumpulan, Waktu_Pengumpulan, Nama, Fk_Id_Kelas as Kelas, Url_File
from Vw_Tugas
inner join Tbl_Siswa
on Vw_Tugas.Kelas = Tbl_Siswa.Fk_Id_Kelas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Vw_Tugas.Id_Tugas

------------------------- Menambahkan Tabel Kontak Kami dan alter table untuk image profil (Agung 19/08/2021) ---------------
create table Tbl_Kontak_Kami (
    Pk_Id_Kontak_Kami int identity not null primary key,
    Nama varchar(50),
    Email varchar(100),
    Subjek varchar(100),
    Pesan varchar(800),
	Waktu datetime
);

ALTER TABLE Tbl_Guru
ADD Image varchar(100);

ALTER TABLE Tbl_Siswa
ADD Image varchar(100);

UPDATE Tbl_Guru
SET Image = 'default.jpg'
WHERE Image IS NULL;

UPDATE Tbl_Siswa
SET Image = 'default.jpg'
WHERE Image IS NULL;

------------------------ Edit table kategori, drop table quiz (Imam 20/08/2021) ------------------------

drop table Tbl_Quiz

alter table Tbl_Kategori
add StatusQuiz bit

alter table Tbl_Kategori
add MulaiQuiz datetime 

alter table Tbl_Kategori
add BatasQuiz datetime

----------------------- Tambah table nilai kuis (Imam 21/08/2021) ---------------------------------------

CREATE TABLE Tbl_QuizNilai (
	Pk_Id_QuizNilai int identity not null primary key,
	Tanggal datetime,
	NilaiQuiz int,
	Fk_Id_Kategori int,
	Fk_Id_Siswa int)

------------------------- Menambahkan Foto_Profil pada View User (Agung 23/08/2021) -------------------------

sp_RENAME 'Tbl_Guru.Image' , 'Foto_Profil', 'COLUMN'
sp_RENAME 'Tbl_Siswa.Image' , 'Foto_Profil', 'COLUMN'

drop view Vw_User

Create View Vw_User AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Email )) AS int), 0) AS ID, Nama, Email, Password, Role, Activated, FK_ID, Foto_Profil
FROM(
Select Nama_Sekolah as Nama, Email, Password, Role, Activated, Pk_Id_Sekolah as FK_ID, Logo as Foto_Profil From Tbl_Sekolah
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Guru as FK_ID, Foto_Profil From Tbl_Guru
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Siswa as FK_ID, Foto_Profil From Tbl_Siswa) AS MyResult


ALTER TABLE Tbl_Sekolah ALTER COLUMN Logo VARCHAR (100);

CREATE TABLE Tbl_AuditTrail_History (
	Pk_Id_AuditTrail_History int identity not null primary key,
	Tanggal Datetime,
	History_Data varchar (500)
);


---------------------------Edit Vw_Kirim_Tugas(Wildan 23/08/2021)-----------------------------------------
drop view Vw_KirimTugas

create view Vw_KirimTugas as
select Id_Tugas, Nama_Tugas, Batas_Pengumpulan, Waktu_Pengumpulan, Fk_Id_Mata_Pelajaran as Kode_Mapel, Tbl_Guru.Nama as Nama_Guru, Tbl_Siswa.Fk_Id_Kelas as Kelas, Tbl_Siswa.Nama as Nama_Siswa, Url_File
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Tugas.Id_Tugas = Tbl_Pengumpulan_Tugas.Fk_Id_Tugas
inner join Tbl_Guru
on Tbl_Guru.Pk_Id_Guru = Tbl_Tugas.Fk_Id_Guru
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Pengumpulan_Tugas.Fk_Id_Siswa


---------------------------Membuat view untuk di SSRS(Wildan 24/08/2021)----------------------------

create view Vw_Student as
select Fk_Id_Kelas as Kelas, COUNT(*) as "Jumlah Murid", Nama_Sekolah as "Nama Sekolah"
from Tbl_Siswa
inner join Tbl_Sekolah
on Tbl_Siswa.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
group by Fk_Id_Kelas, Nama_Sekolah

--------------------------- Membuat view data Guru dan Siswa (Agung 23/08/2021) -------------------------

drop table Tbl_Mata_Pelajaran

CREATE TABLE Tbl_Mata_Pelajaran (
	Pk_Id_Mata_Pelajaran int identity not null primary key,
	Kode_Mata_Pelajaran Varchar (50),
	Nama_Mata_Pelajaran varchar (100),
	Fk_Id_Sekolah int
);

INSERT INTO Tbl_Mata_Pelajaran VALUES
('A1', 'Bahasa Indonesia', 31),
('A2', 'Bahasa Inggris', 31),
('A3', 'Matematika', 31),
('B1', 'Olahraga', 31),
('B2', 'Pendidikan Kewarganegaraan', 31);


CREATE VIEW Vw_Data_Guru AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tbl_Guru.Pk_Id_Guru )) AS int), 0) AS ID, Pk_Id_Guru, NIP, Nama, Tbl_Guru.Email, Tbl_Guru.No_Telp, Tbl_Guru.Password, Tbl_Guru.Fk_Id_Sekolah as ID_Sekolah, Tbl_Sekolah.Nama_Sekolah as Sekolah, Tbl_Mata_Pelajaran.Nama_Mata_Pelajaran as 'Mata_Pelajaran', Tbl_Jenis_Kelamin.Jenis_Kelamin as 'Jenis_Kelamin', Tbl_Guru.Activated as 'Status_Aktivasi', Tbl_Guru.Foto_Profil as 'Foto_Profil'
FROM Tbl_Guru
LEFT JOIN Tbl_Sekolah ON Fk_Id_Sekolah = Pk_Id_Sekolah
LEFT JOIN Tbl_Jenis_Kelamin ON Fk_Id_Jenis_Kelamin = Pk_Id_Jenis_Kelamin
LEFT JOIN Tbl_Mata_Pelajaran ON Fk_Id_Mata_Pelajaran = Pk_Id_Mata_Pelajaran


CREATE VIEW Vw_Data_Siswa AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tbl_Siswa.Pk_Id_Siswa )) AS int), 0) AS ID, Pk_Id_Siswa, Nomor_Induk, Nama, Tbl_Siswa.Email, Tbl_Siswa.Password, Tbl_Siswa.Fk_Id_Sekolah as ID_Sekolah, Tbl_Sekolah.Nama_Sekolah as Sekolah, Fk_Id_Kelas as ID_Kelas, Tbl_Kelas.Nama_Kelas as 'Kelas', Tbl_Jenis_Kelamin.Jenis_Kelamin as 'Jenis_Kelamin', Tbl_Siswa.Activated as 'Status_Aktivasi', Tbl_Siswa.Foto_Profil as 'Foto_Profil'
FROM Tbl_Siswa
LEFT JOIN Tbl_Sekolah ON Fk_Id_Sekolah = Pk_Id_Sekolah
LEFT JOIN Tbl_Jenis_Kelamin ON Fk_Id_Jenis_Kelamin = Pk_Id_Jenis_Kelamin
LEFT JOIN Tbl_Kelas ON Fk_Id_Kelas = Pk_Id_Kelas

drop table Tbl_Kelas

CREATE TABLE Tbl_Kelas (
	Pk_Id_Kelas int identity not null primary key,
	Nama_Kelas Varchar (50),
	Fk_Id_Sekolah int
);

INSERT INTO Tbl_Kelas VALUES
('Kelas 1-A', 31),
('Kelas 1-B', 31),
('Kelas 1-C', 31),
('Kelas 2-A', 31),
('Kelas 2-B', 31),
('Kelas 2-C', 31),
('Kelas 3-A', 31),
('Kelas 3-B', 31),
('Kelas 3-C', 31),
('Kelas 4-A', 31),
('Kelas 4-B', 31),
('Kelas 4-C', 31);

update Tbl_Jenis_Kelamin
set Jenis_Kelamin = 'Laki-Laki' where Jenis_Kelamin = 'Laki_Laki'


------------------- Tambah query baru (Imam 25/08/2021) ---------------------------

INSERT INTO Tbl_Kelas VALUES
('Kelas 5-A', 31),
('Kelas 5-B', 31),
('Kelas 5-C', 31),
('Kelas 6-A', 31),
('Kelas 6-B', 31),
('Kelas 6-C', 31)

INSERT INTO Tbl_Mata_Pelajaran VALUES
('B3', 'Ilmu Pengetahuan Alam', 31)

alter table Tbl_Tugas
add Fk_Id_Sekolah int

------------------------------ Edit Tbl_Absensi(Wildan 25/08/2021) ------------------------------------------
alter table Tbl_Absensi
alter column Fk_Id_Mata_Pelajaran int;

------------------- Menambah ID sekolah di view user (Agung 25/08/2021) ---------------------------
drop view Vw_User

Create View Vw_User AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Email )) AS int), 0) AS ID, Nama, Email, Password, Role, Activated, FK_ID, Foto_Profil, Id_Sekolah
FROM(
Select Nama_Sekolah as Nama, Email, Password, Role, Activated, Pk_Id_Sekolah as FK_ID, Logo as Foto_Profil, Pk_Id_Sekolah as Id_Sekolah From Tbl_Sekolah
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Guru as FK_ID, Foto_Profil, Fk_Id_Sekolah as Id_Sekolah From Tbl_Guru
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Siswa as FK_ID, Foto_Profil, Fk_Id_Sekolah as Id_Sekolah From Tbl_Siswa) AS MyResult

----------------------------- Alter Tbl_Pengumpulan_Tugas (Imam 26/08/2021) -----------------------------------

alter table Tbl_Pengumpulan_Tugas
add Status_Nilai bit

---------------------------- Add table pengumuman (Imam 26/08/2021) -------------------------------------

create table Tbl_Pengumuman (
	Pk_Id_Pengumuman int identity primary key not null,
	Pengumuman varchar (max),
	Fk_Id_Sekolah int,
)

------------------------ Membuat View Untuk Absen Guru (Wildan 26/08/2021) -------------------------------------------

create view Vw_KehadiranGuru as
select Pk_Id_Absensi, Tanggal, Nama_Sekolah, Nama_Mata_Pelajaran, Nama as 'Nama_Guru', Status
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Absensi.Fk_Id_Mata_Pelajaran
inner join Tbl_Guru
on Tbl_Absensi.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru

create view Vw_KehadiranSiswa as
select Pk_Id_Absensi, Tanggal, Nama_Sekolah, Nama as 'Nama_Siswa', Nama_Kelas as 'Kelas', Status
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Siswa
on Tbl_Absensi.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Siswa.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas

---------------------- Alter table guru id mapel(Agung 26/08/2021) ----------------------------------------------
ALTER TABLE Tbl_Guru
ALTER COLUMN Fk_Id_Mata_Pelajaran int;

-------------------------- Alter table nilai id mapel (Imam 26/08/2021) -----------------------------------------
ALTER TABLE Tbl_Nilai
Alter Column Fk_Id_Mata_Pelajaran int;


-------------------------- Alter Vw_Tugas dan Vw_KirimTugas (Imam 27/08/2021) -----------------------------------------------------

drop view Vw_Tugas

create view Vw_Tugas as 
select Id_Tugas, Nama_Tugas, Waktu_Pemberian, Batas_Pengumpulan, Fk_Id_Kelas as Kelas, Nama as Nama_Guru, Fk_Id_Mata_Pelajaran as Id_Mapel
from Tbl_Tugas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru

drop view Vw_KirimTugas

create view Vw_KirimTugas as
select Id_Tugas, Nama_Tugas, Kode_Mapel, Batas_Pengumpulan, Waktu_Pengumpulan, Nama, Fk_Id_Kelas as Kelas, Url_File
from Vw_Tugas
inner join Tbl_Siswa
on Vw_Tugas.Kelas = Tbl_Siswa.Fk_Id_Kelas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Vw_Tugas.Id_Tugas


------------------------ Alter Vw_KirimTugas (Imam 28/08/2021) -------------------------------------------------
drop view Vw_KirimTugas
create view Vw_KirimTugas as
select Id_Tugas, Nama_Tugas, Kode_Mapel, Batas_Pengumpulan, Waktu_Pengumpulan, Nama as Nama_Siswa, Fk_Id_Kelas as Kelas, Url_File
from Vw_Tugas
inner join Tbl_Siswa
on Vw_Tugas.Kelas = Tbl_Siswa.Fk_Id_Kelas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Vw_Tugas.Id_Tugas

------------------------ Create table pengumuman (Agung 29/08/2021) ----------------------------------------------
drop table Tbl_Pengumuman

CREATE TABLE Tbl_Pengumuman (
	Pk_Id_Pengumuman int identity primary key not null,
	Judul_Pengumuman varchar(100),
	Pengumuman varchar (max),
	Fk_Id_Sekolah int,
	Waktu datetime,
	Background varchar (20),
	status bit,
	file_Tambahan varchar (300)
);



------------------------ Perbaikan View Pertugasan dan View untuk Validasi Absensi (Wildan 29/08/2021) ----------------------------------

drop view Vw_Tugas

create view Vw_Tugas as 
select Id_Tugas, Nama_Tugas, Waktu_Pemberian, Batas_Pengumpulan, Nama_Kelas, Nama as Nama_Guru, Nama_Mata_Pelajaran
from Tbl_Tugas
inner join Tbl_Kelas
on Tbl_Tugas.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Guru.Fk_Id_Mata_Pelajaran


drop view Vw_KirimTugas

create view Vw_KirimTugas as
select Id_Tugas, Nama_Tugas, Nama_Mata_Pelajaran, Batas_Pengumpulan, Waktu_Pengumpulan, Tbl_Siswa.Nama as Nama_Siswa, Nama_Kelas, Url_File
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Pengumpulan_Tugas.Fk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Guru.Fk_Id_Mata_Pelajaran


create view Vw_TugasKumpul as
select Pk_Id_Pengumpulan_Tugas, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Url_File )) AS int), 0) AS No, Nama_Sekolah, Nama_Tugas, Tbl_Siswa.Nama as Nama_Siswa, Waktu_Pengumpulan, Url_File, Fk_Id_Tugas as Id_Tugas, Status_Nilai
from Tbl_Pengumpulan_Tugas
inner join Tbl_Tugas
on Tbl_Tugas.Id_Tugas = Tbl_Pengumpulan_Tugas.Fk_Id_Tugas
inner join Tbl_Sekolah
on Tbl_Sekolah.Pk_Id_Sekolah = Tbl_Pengumpulan_Tugas.Fk_Id_Sekolah
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Pengumpulan_Tugas.Fk_Id_Siswa

alter table Tbl_Absensi
add Status_Absensi bit;

alter table Tbl_Absensi
add Url_File varchar(100);

drop view Vw_KehadiranGuru

create view Vw_KehadiranGuru as
select Pk_Id_Absensi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama_Mata_Pelajaran, Nama as 'Nama_Guru', Status, Status_Absensi, Url_File
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Absensi.Fk_Id_Mata_Pelajaran
inner join Tbl_Guru
on Tbl_Absensi.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru

drop view Vw_KehadiranSiswa

create view Vw_KehadiranSiswa as
select Pk_Id_Absensi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama as 'Nama_Siswa', Nama_Kelas as 'Kelas', Status, Status_Absensi, Url_File
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Siswa
on Tbl_Absensi.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Siswa.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas



---------------------------------- Create view kategori kuis (Imam 29/08/2021)-----------------------------------------

create view Vw_KategoriQuiz as
select Pk_Id_Kategori, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kategori )) AS int), 0) AS No, KatName as 'Nama_Kategori', Fk_Id_Guru as 'IDGuru', Fk_Id_Kelas as 'IDKelas', Nama as 'Nama_Guru', Nama_Kelas as 'Kelas', StatusQuiz as 'Status' , MulaiQuiz as 'Mulai_Kuis', BatasQuiz as 'Batas_Kuis'
from Tbl_Kategori
inner join Tbl_Guru
on Tbl_Kategori.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kategori.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas

select * from Vw_KategoriQuiz

----------Alter view kehadiran untuk rekap (Wildan 29/08/2021)----------------

CREATE TABLE Tbl_Role (
	Pk_Id_Role int identity not null primary key,
	Nama_Role Varchar (100),
);

INSERT INTO Tbl_Role (Nama_Role) VALUES
	('Super Admin'),
	('Admin Sekolah'),
	('Guru'),
	('Siswa');

alter view Vw_KehadiranGuru as
select Pk_Id_Absensi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama_Mata_Pelajaran, Nama as 'Nama_Guru', Status, Status_Absensi, Url_File, Nama_Role as Role
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Absensi.Fk_Id_Mata_Pelajaran
inner join Tbl_Guru
on Tbl_Absensi.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Role
on Tbl_Role.Pk_Id_Role = Tbl_Guru.Role

alter view Vw_KehadiranSiswa as
select Pk_Id_Absensi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama as 'Nama_Siswa', Nama_Kelas as 'Kelas', Status, Status_Absensi, Url_File, Nama_Role as Role
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Siswa
on Tbl_Absensi.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Siswa.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
inner join Tbl_Role
on Tbl_Role.Pk_Id_Role = Tbl_Siswa.Role

create view Vw_RekapAbsensi as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama, Role, Status, Status_Absensi
FROM(
Select Pk_Id_Absensi, Tanggal, Nama_Sekolah, Nama_Siswa as Nama, Role, Status, Status_Absensi From Vw_KehadiranSiswa
Union
Select Pk_Id_Absensi, Tanggal, Nama_Sekolah, Nama_Guru as Nama, Role, Status, Status_Absensi From Vw_KehadiranGuru
) AS MyResult

------------------------------------------ Alter Vw_Student (Imam 29/08/2021) -----------------------

drop view Vw_Student

create view Vw_Student as
select Nama_Kelas as "Nama Kelas", COUNT(*) as "Jumlah Murid", Nama_Sekolah as "Nama Sekolah"
from Tbl_Siswa
inner join Tbl_Sekolah
on Tbl_Siswa.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Kelas
on Tbl_Siswa.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
group by Nama_Kelas, Nama_Sekolah


----------------------------------------- Add view evaluasi kuis, tugas, total dan view nilai kuis, alter nilai kuis (Imam 30/08/2021) ----------------------------

create view Vw_EvaluasiKuis as
select * from (
select
KatName,
Tbl_Siswa.Nama as Nama_Siswa,
Nama_Kelas,
NilaiQuiz,
Tanggal,
BatasQuiz,
Tbl_Guru.Nama as Nama_Guru,
Fk_Id_Guru,
Fk_Id_Siswa,
Tbl_Siswa.Fk_Id_Kelas,
row_number() over(partition by KatName, Tbl_Siswa.Nama order by Tanggal desc) as terbaru
from Tbl_Kategori
inner join Tbl_QuizNilai
on Tbl_Kategori.Pk_Id_Kategori = Tbl_QuizNilai.Fk_Id_Kategori
inner join Tbl_Siswa
on Tbl_QuizNilai.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Guru
on Tbl_Guru.Pk_Id_Guru = Tbl_Kategori.Fk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Siswa.Fk_Id_Kelas
) t
where t.terbaru = 1


create view Vw_EvaluasiTugas as
select * from (
select
Nama_Tugas,
Tbl_Siswa.Nama as Nama_Siswa,
Nilai,
Waktu_Pengumpulan as Tanggal,
Batas_Pengumpulan,
Tbl_Guru.Nama as Nama_Guru,
Nama_Kelas,
Tbl_Nilai.Eval_Per_Hari,
Tbl_Tugas.Fk_Id_Guru,
Tbl_Nilai.Fk_Id_Siswa,
Tbl_Tugas.Fk_Id_Kelas,
row_number() over(partition by Nama_Tugas, Tbl_Siswa.Nama order by Waktu_Pengumpulan desc) as terbaru
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Nilai
on Tbl_Pengumpulan_Tugas.Pk_Id_Pengumpulan_Tugas = Tbl_Nilai.Fk_Id_Pengumpulan_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Nilai.Fk_Id_Siswa
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
) t
where t.terbaru = 1


create view Vw_EvaluasiTotal as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tanggal )) AS int), 0) AS No, Tugas_Kuis, Nama_Siswa, Nilai, Tanggal, Batas_Pengumpulan, Nama_Guru, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru
FROM(
Select KatName as Tugas_Kuis, Nama_Siswa, NilaiQuiz as Nilai, Tanggal, BatasQuiz as Batas_Pengumpulan, Nama_Guru, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru From Vw_EvaluasiKuis
Union
Select Nama_Tugas as Tugas_Kuis, Nama_Siswa, Nilai, Tanggal, Batas_Pengumpulan, Nama_Guru, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru From Vw_EvaluasiTugas
) AS MyResult

create view Vw_NilaiKuis as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_QuizNilai )) AS int), 0) AS No, q.Tanggal, q.NilaiQuiz, s.Nama as 'Nama_Siswa', kel.Nama_Kelas, k.KatName as 'Kategori', g.Nama as 'Nama_Guru', g.Pk_Id_Guru as'ID_Guru'
from Tbl_QuizNilai q
inner join Tbl_Siswa s
on q.Fk_Id_Siswa = s.Pk_Id_Siswa
inner join Tbl_Kategori k
on q.Fk_Id_Kategori = k.Pk_Id_Kategori
inner join Tbl_Kelas kel
on k.Fk_Id_Kelas = kel.Pk_Id_Kelas
inner join Tbl_Guru g
on k.Fk_Id_Guru = g.Pk_Id_Guru

ALTER TABLE Tbl_QuizNilai
ALTER COLUMN NilaiQuiz float(10);
------------------------------------------ Create View buat Dashboard (Agung, 30/08/2021) ----------------------------------

create view Vw_DashboardAdmin as
SELECT Pk_Id_Sekolah, Nama_Sekolah, (SELECT COUNT(*) FROM Tbl_Guru g WHERE g.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah) AS 'Jumlah_Guru',
(SELECT COUNT(*) FROM Tbl_Siswa s WHERE s.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah) AS 'Jumlah_Siswa',
(SELECT COUNT(*) FROM Tbl_Kelas s WHERE s.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah) AS 'Jumlah_Kelas'
from Tbl_Sekolah

create view Vw_DashboardAdmin2 as
select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Sekolah )) AS int), 0) AS No, Pk_Id_Sekolah, Nama_Sekolah, Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran, Tbl_Mata_Pelajaran.Nama_Mata_Pelajaran
from Tbl_Guru
left join Tbl_Mata_Pelajaran
on Tbl_Guru.Fk_Id_Mata_Pelajaran = Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran
left join Tbl_Sekolah
on Tbl_Guru.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah

create view Vw_DashboardAdmin3 as
select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Vw_DashboardAdmin2.Pk_Id_Sekolah )) AS int), 0) AS No, Tbl_Sekolah.Pk_Id_Sekolah, Vw_DashboardAdmin2.Nama_Mata_Pelajaran, COUNT(*) as "Jumlah_Guru"
from Vw_DashboardAdmin2
join Tbl_Sekolah on Vw_DashboardAdmin2.Pk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
group by Vw_DashboardAdmin2.Pk_Id_Sekolah, Tbl_Sekolah.Pk_Id_Sekolah, Vw_DashboardAdmin2.Nama_Mata_Pelajaran

select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kelas )) AS int), 0) AS No, Pk_Id_Kelas, Tbl_Kelas.Fk_Id_Sekolah, COUNT(*) as "Jumlah_Siswa"
from Tbl_Kelas
right join Tbl_Siswa
on Tbl_Kelas.Fk_Id_Sekolah = Tbl_Siswa.Fk_Id_Sekolah
Group by Pk_Id_Kelas, Tbl_Kelas.Fk_Id_Sekolah

create view Vw_DashboardAdmin4 as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kelas )) AS int), 0) AS No, Fk_Id_Sekolah, Pk_Id_Kelas, Nama_Kelas,
(SELECT COUNT(*) FROM Tbl_Siswa s WHERE s.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas) AS 'Jumlah_Siswa'
from Tbl_Kelas

------------------ add table evaluasi (Wildan 30/8/2021)------------------
create table Tbl_Evaluasi(
	Pk_Id_Evaluasi int identity not null primary key,
	Nilai int,
	Fk_Id_Sekolah int,
	Fk_Id_Siswa int,
	Fk_Id_Guru int,
	Evaluasi Varchar (max),
);

----------------------Alter column di Tbl_Nilai (Wildan 31/08/2021)--------------------

alter table Tbl_Nilai
add Fk_Id_Pengumpulan_Tugas int 

drop view Vw_EvaluasiTugas

create view Vw_EvaluasiTugas as
select * from (
select
Nama_Tugas,
Tbl_Siswa.Nama as Nama_Siswa,
Nilai,
Waktu_Pengumpulan as Tanggal,
Batas_Pengumpulan,
Tbl_Guru.Nama as Nama_Guru,
Nama_Kelas,
Tbl_Tugas.Fk_Id_Guru,
Tbl_Nilai.Fk_Id_Siswa,
Tbl_Tugas.Fk_Id_Kelas,
row_number() over(partition by Nama_Tugas, Tbl_Siswa.Nama order by Waktu_Pengumpulan desc) as terbaru
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Nilai
on Tbl_Pengumpulan_Tugas.Pk_Id_Pengumpulan_Tugas = Tbl_Nilai.Fk_Id_Pengumpulan_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Nilai.Fk_Id_Siswa
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
) t
where t.terbaru = 1


-------------------------------- Add view Vw_HasilEvaluasi (Imam 31/08/2021) -----------------------------

create view Vw_HasilEvaluasi as
select e.Pk_Id_Evaluasi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY e.Pk_Id_Evaluasi )) AS int), 0) AS No,e.Nilai, g.Pk_Id_Guru as Id_Guru, g.Nama as Nama_Guru, s.Pk_Id_Siswa as Id_Siswa, s.Nama as Nama_Siswa, e.Evaluasi, se.Pk_Id_Sekolah as Id_Sekolah, se.Nama_Sekolah as Nama_Sekolah
from Tbl_Evaluasi e
inner join Tbl_Guru g
on e.Fk_Id_Guru = g.Pk_Id_Guru
inner join Tbl_Siswa s
on e.Fk_Id_Siswa = s.Pk_Id_Siswa
inner join Tbl_Sekolah se
on e.Fk_Id_Sekolah = se.Pk_Id_Sekolah

select * from Vw_HasilEvaluasi

alter table Tbl_Evaluasi
add Tanggal_Evaluasi datetime
---------------------------- Menambahkan View dashboard Guru (Agung, 31/08/2021) --------------------------
ALTER TABLE Tbl_Nilai
ADD Fk_Id_Kelas int;

use WiaClass

select * from Tbl_Kelas
select * from Tbl_Nilai
select * from Tbl_Siswa
create view Vw_DashboardGuru1 as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kelas )) AS int), 0) AS No, Pk_Id_Kelas, Nama_Kelas, (SELECT AVG(Nilai) FROM Tbl_Nilai where Tbl_Nilai.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas) AS 'Rata-rata', Tbl_Kelas.Fk_Id_Sekolah
from Tbl_Kelas
inner join Tbl_Nilai
on Tbl_Nilai.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
group by Pk_Id_Kelas, Nama_Kelas, Tbl_Kelas.Fk_Id_Sekolah

ALTER TABLE Tbl_Pengumpulan_Tugas
ADD Fk_Id_Kelas int;

ALTER TABLE Tbl_Sekolah
ADD Tanggal_Registrasi Datetime;

create view Vw_DashboardSA1 as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY MONTH(Tanggal_Registrasi) )) AS int), 0) AS No, (MONTH(Tanggal_Registrasi) * 10000) + YEAR(Tanggal_Registrasi) AS mmYYYY, COUNT(*) as Jumlah_Sekolah
FROM Tbl_Sekolah
GROUP BY MONTH(Tanggal_Registrasi), (MONTH(Tanggal_Registrasi) * 10000) + YEAR(Tanggal_Registrasi)

select * from Tbl_SoalSSIS


----------------------------------- Add timer (Imam 01/09/2021)---------------------------
use WiaClass

alter table tbl_Kategori
add WaktuPengerjaan int

drop view Vw_KategoriQuiz

create view Vw_KategoriQuiz as
select Pk_Id_Kategori, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kategori )) AS int), 0) AS No, KatName as 'Nama_Kategori', Fk_Id_Guru as 'IDGuru', Fk_Id_Kelas as 'IDKelas', Nama as 'Nama_Guru', Nama_Kelas as 'Kelas', StatusQuiz as 'Status' , MulaiQuiz as 'Mulai_Kuis', BatasQuiz as 'Batas_Kuis', WaktuPengerjaan as Waktu_Pengerjaan
from Tbl_Kategori
inner join Tbl_Guru
on Tbl_Kategori.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kategori.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas

------------------------- Fixing query view (Imam 01/09/2021)--------------------------------

drop view Vw_KirimTugas

create view Vw_KirimTugas as
select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Url_File )) AS int), 0) AS No, Id_Tugas, Nama_Tugas, Nama_Mata_Pelajaran, Batas_Pengumpulan, Waktu_Pengumpulan, Tbl_Siswa.Nama as Nama_Siswa, Nama_Kelas, Url_File
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Pengumpulan_Tugas.Fk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Guru.Fk_Id_Mata_Pelajaran

drop view Vw_Tugas

create view Vw_Tugas as 
select Id_Tugas, Nama_Tugas, Waktu_Pemberian, Batas_Pengumpulan, Nama_Kelas, Nama as Nama_Guru, Nama_Mata_Pelajaran, Pk_Id_Sekolah as Id_Sekolah
from Tbl_Tugas
inner join Tbl_Kelas
on Tbl_Tugas.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Guru.Fk_Id_Mata_Pelajaran
inner join Tbl_Sekolah
on Tbl_Guru.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah

---------------------------- Alter column untuk evaluasi harian (Wildan 03/09/2021) ----------------------------------

alter table Tbl_Nilai
add Eval_Per_Hari varchar(max)

alter table Tbl_QuizNilai
add Eval_Per_Hari varchar(max)

drop view Vw_EvaluasiKuis

create view Vw_EvaluasiKuis as
select * from (
select
KatName,
Tbl_Siswa.Nama as Nama_Siswa,
Nama_Kelas,
NilaiQuiz,
Tanggal,
BatasQuiz,
Tbl_Guru.Nama as Nama_Guru,
Tbl_QuizNilai.Eval_Per_Hari,
Fk_Id_Guru,
Fk_Id_Siswa,
Tbl_Siswa.Fk_Id_Kelas,
row_number() over(partition by KatName, Tbl_Siswa.Nama order by Tanggal desc) as terbaru
from Tbl_Kategori
inner join Tbl_QuizNilai
on Tbl_Kategori.Pk_Id_Kategori = Tbl_QuizNilai.Fk_Id_Kategori
inner join Tbl_Siswa
on Tbl_QuizNilai.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Guru
on Tbl_Guru.Pk_Id_Guru = Tbl_Kategori.Fk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Siswa.Fk_Id_Kelas
) t
where t.terbaru = 1

drop view Vw_EvaluasiTugas

create view Vw_EvaluasiTugas as
select * from (
select
Nama_Tugas,
Tbl_Siswa.Nama as Nama_Siswa,
Nilai,
Waktu_Pengumpulan as Tanggal,
Batas_Pengumpulan,
Tbl_Guru.Nama as Nama_Guru,
Nama_Kelas,
Tbl_Nilai.Eval_Per_Hari,
Tbl_Tugas.Fk_Id_Guru,
Tbl_Nilai.Fk_Id_Siswa,
Tbl_Tugas.Fk_Id_Kelas,
row_number() over(partition by Nama_Tugas, Tbl_Siswa.Nama order by Waktu_Pengumpulan desc) as terbaru
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Nilai
on Tbl_Pengumpulan_Tugas.Pk_Id_Pengumpulan_Tugas = Tbl_Nilai.Fk_Id_Pengumpulan_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Nilai.Fk_Id_Siswa
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
) t
where t.terbaru = 1

drop view Vw_EvaluasiTotal

create view Vw_EvaluasiTotal as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tanggal )) AS int), 0) AS No, Tugas_Kuis, Nama_Siswa, Nilai, Tanggal, Batas_Pengumpulan, Nama_Guru, Eval_Per_Hari, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru
FROM(
Select KatName as Tugas_Kuis, Nama_Siswa, NilaiQuiz as Nilai, Tanggal, BatasQuiz as Batas_Pengumpulan, Nama_Guru, Eval_Per_Hari, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru From Vw_EvaluasiKuis
Union
Select Nama_Tugas as Tugas_Kuis, Nama_Siswa, Nilai, Tanggal, Batas_Pengumpulan, Nama_Guru, Eval_Per_Hari, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru From Vw_EvaluasiTugas
) AS MyResult


create view Vw_EvaluasiPerTugas as
select * from (
select
ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Waktu_Pengumpulan )) AS int), 0) AS No,
Nama_Mata_Pelajaran,
Nama_Tugas,
Waktu_Pengumpulan,
Nilai,
Tbl_Guru.Nama as Nama_Guru,
Tbl_Siswa.Nama as Nama_Siswa,
Tbl_Nilai.Eval_Per_Hari,
Tbl_Nilai.Fk_Id_Sekolah,
Tbl_Nilai.Fk_Id_Guru,
Tbl_Nilai.Fk_Id_Siswa,
Tbl_Nilai.Fk_Id_Mata_Pelajaran,
Tbl_Nilai.Fk_Id_Tugas,
Tbl_Nilai.Fk_Id_Kelas,
row_number() over(partition by Nama_Tugas, Tbl_Siswa.Nama order by Waktu_Pengumpulan desc) as terbaru
from Tbl_Nilai
inner join Tbl_Pengumpulan_Tugas
on Tbl_Nilai.Fk_Id_Pengumpulan_Tugas = Tbl_Pengumpulan_Tugas.Pk_Id_Pengumpulan_Tugas
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Nilai.Fk_Id_Mata_Pelajaran
inner join Tbl_Tugas
on Tbl_Nilai.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Guru
on Tbl_Guru.Pk_Id_Guru = Tbl_Nilai.Fk_Id_Guru
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Nilai.Fk_Id_Siswa
) t
where t.terbaru = 1
