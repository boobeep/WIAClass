==============================================
		   Installasi
==============================================
- Pull semua file dari repository Tricone Travel di
  GitLab dan tempatkan pada satu file.
- Jika sudah, extract file 
  "SqlServerTypes.zip" didalam folder "file
  Pendukung". Maka akan muncul 
  folder dengan nama "SqlServerTypes".
- Copy folder "SqlServerTypes" tadi ke dalam 
  folder "Tricone Travel", lalu replace folder yang
  sudah ada dengan folder yang baru di copy.
- Restore database pada model project 
  (nama database: DB_TRICONE), khusus untuk 
  bagian view, ketentuan lebih lanjut dapat 
  dilihat pada file catatan.txt.
- Lalu input semua data pada file 
  (DB_TRICONE.sql) ke model yang tadi telah
  direstore dari visual studio project. 
- Ubah ConnectionString pada project
  (Web.config) dan pada service (App.config) 
  sesuai dengan datasource yang ada dengan id
  dan password milik datasource.
- Buka Aplikasi Report Service Configuration pilih 
  menu upload, kemudian upload semua file pada folder 
  Report Service.
- Selanjutnya, buka Ms. Visual Studio lalu 
  buka file ekstensi ".sln" yang berada 
  didalam folder "Tricone Travel" dan tunggu 
  beberapa saat untuk membuka project.
- Jika project sudah terbuka, pilih folder model 
  kemudian pilih file TriconeTravel.edmx, delete 
  semua tabel view, kemudian klik kanan pada diagram 
  triconeModel.edmx pilih update kemudian klik add
  semua view lalu klik oke dan save.
- Kilk button hijau di menu bar untuk memulai project 
  Tricone Travel.  

==============================================
		   Home Menu
==============================================
- Ditampilkan slider berupa tampilan menu utama 
  ada menu how to book ketika di klik akan 
  diarahkan ke halaman cara melakukan pemesanan 
  kendaraan di tricone travel, ada menu booknow 
  ketika di klik akan diarahkan ke halaman 
  pemesanan kendaraan, ada menu discount ketika 
  di klik akan diarahkan ke halaman promo yang 
  ada di tricone travel, ada menu account jika 
  user ingin melakukan pembuatan akun dapat 
  klik menu register lalu akan diarahkan ke halaman 
  register, jika user ingin masuk ke akun yang sudah
  pernah dibuat, user dapat klik menu login 
  kemudian akan diarahkan ke halaman login.
- Our Offer menampilkan daftar kendaraan yang 
  tersedia di tricone travel.
- Our Service menampilkan pelayanan yang ditawarkan
  oleh tricone travel, jika gambar di klik kemudian 
  akan diarahkan ke menu artikel detail servis.
- Customer Trstimony menampilkan daftar testimoni
  dari member tricone travel yang pernah menggunakan
  jasa tricone travel.
- Footer berisi tentang tricone travel disertai 
  dengan link pintas yang menampilkan link tentang
  tricone travel, detail kendaraan, Pertanyaan 
  yang paling sering diajukan, Syarat dan ketentuan, 
  dan link menu kontak tricone travel, selain 
  itu juga menampilkan informasi sosial media 
  yang jika ikon di klik akan diarahkan ke laman 
  sosial media dari tricone travel. 


==============================================
	      	 Account Login Page
==============================================
- Berisi form login untuk user Tricone Travel yang
  telah terdaftar.
- Form berisi kolom username dan password.
- Jika kolom username dan password benar, user
  bisa mengklik tombol login dan akan di
  arahkan ke halaman home dengan status sudah
  login.
- Dibawah form login terdapat pilihan forget
  password dan juga create new account.
- Forget password akan diarahkan ke halaman
  forget password dan create new account akan
  diarahkan ke hamalan register.

==============================================
	      Account Register Page
==============================================
- Berisi form registrasi untuk visitor yang
  ingin mendaftar/membuat akun baru di 
  Tricone Travel.
- Form berisi kolom name, address, telephone,
  email, username, password dan confirm password.
- Jika kolom username dan email tersedia dan
  dalam format yang benar, user bisa mengklik 
  tombol create new account dan akan di
  arahkan ke halaman konfirmasi email status
  akun yang harus diaktifkan terlebih dahulu
  untuk login.

==============================================
	      	Forgot Password
==============================================
- Berisi form untuk data yang dibutuhkan dalam
  proses password recovery.
- Form berisi kolom email yang email itu nanti
  akan digunakan untuk dikirimkan link
  recovery yang valid dari sistem Tricone Travel.
- Jika email valid dan terdaftar, bisa
  langsung tekan tombol send email.

==============================================
	      	Reset Password
==============================================
- Berisi Form untuk membuat password baru
  setelah berhasil proses forget password
- Form berisi kolom new password dan confirm
  password.
- Jika sudah sesuai dengan validasi, dapat langsung 
  menyimpan password barunya dengan menekan tombol 
  save password.

==============================================
	      	   Logout
==============================================
- Tombol yang digunakan untuk keluar dari 
  user authentication.

==============================================
		   How To Book
		     (User)
==============================================
- Jika di klik user akan diarahkan ke tampilan 
  cara melakukan pemesanan di Tricone Travel
- Terdapat button icon yang menampilkan langkah-
  langkah dari cara pemesanan kendaraan

==============================================
	            Book Now
                     (User)
==============================================
- Untuk melakukan pemesanan user dapat memilih 
  tanggal pergi, tanggal pulang dan lokasi kota
  tujuan
- setelah itu akan muncul kendaraan yang tersedia
  user dapat klik button add to cart.

==============================================
	           Discount
                    (User)
==============================================
- Menampilkan kumpulan diskon yang sedang berlaku 
  di Tricone Travel.
- Untuk menggunakannya user bisa klik button copy.

==============================================
	         Notification
                    (User)
==============================================
- Menampilkan notifikasi konfirmasi dari refund, 
  konfirmasi ubah jadwal dan konfirmasi pemesanan. 

==============================================
	           Keranjang
                    (User)
==============================================
- Menampilkan detail cart dari pemesanan yang sudah
  dilakukan.
- Terdapat tampilan tanggal, kota tujuan, tanggal dan 
  total harga dari pemesanan.

==============================================
	           Detail Cart
                     (User)
==============================================
- Menampilkan detail dari pemesanan
- Terdapat tampilan dari kendaraan yang dipilih, 
  ada button delete transaksi jika ingin menghapus 
  transaksi, juga menampilkan total dari harga 
  pemesanan, jika user ingin menggunakan kode diskon 
  untuk mendapat potongan harga dengan menyalin kode 
  dari halaman diskon.
- Setelah mengisi kolom catatan user harus mengklik 
  button update shopping cart.
- Terdapat tabel Price yang menampikan durasi,
  subTotal, diskon, biaya hari libur(jika saat 
  melakukan pemesanan di hari libur) dan total harga.
- Jika sudah memastikan data sesuai, dapat mengklik
  button checkout kemudian akan diarahkan ke menu
  selanjutnya

==============================================
	      	Halaman Pembayaran
		   (User)
==============================================
- Terdapat Nomor Rekening Pembayaran dari Tricone
  Travel dan juga terdapat barcode jika ingin 
  melakukan scan nomor rekening.
- Kemudian pilih nama bank, mengisikan nama 
  rekening, nomor akun dan jumlah total dari 
  pembelian. 
- Setelah mengisi data pembayaran dan jika ingin 
  melanjutkan pembayaran dapat mengklik "I accept 
  to the term and condition".
- Setelah itu akan diarahkan ke menu jadwal.

=======================================================
	      	Halaman Jadwal
		    (User)
=======================================================
- Jika ingin melakukan penggantian jadwal perjalanan
  dapat mengklik button re-schedule
- Jika ingin melakukan Refund atau pengembalian dana
  dapat mengklik button Refund
- Halaman ini berisi tabel tentang informasi jadwal
  pemesanan yang terdiri dari kolom start date, end 
  date, city, transfer pic, approva, status, action, 
  dan badge dengan pagination, sort dan search bar.
- Terdapat button confirm payment untuk mengupload 
  bukti pembayaran, kemudian button edit untuk mengedit data
  pembayaran, ada juga button detail untuk melihat
  detail dari pemesanan.

=======================================================
	      	Halaman Reschedule
		      (User)
=======================================================
- Halaman ini berisi tabel tentang informasi jadwal
  pemesanan yang terdiri dari kolom start date, end 
  date, city, transfer pic, approva, status, action, 
  dan badge dengan pagination, sort dan search bar.
- Terdapat button schedule berfungsi untuk kembali ke 
  halaman jadwal, button refund berfungsi jika 
  ingin mengajukan pengembalian dana, kemudian ada 
  add request reschedule jika ingin mengajukan 
  penggantian jadwal.
- Jika klik button add request reschedule kemudian 
  akan diarahkan ke halaman ganti jadwal tanggal. 
- Kemudian akan diarahkan ke halaman pengisian form
  penggantian jadwal, memilih tanggal jadwal transaksi, 
  pilih start date dan end date yang baru, dan menyantumkan 
  alasan kenapa ingin mengganti jadwal kemudian klik   
  create request.

==============================================
	          Master Data
		    (Admin)
==============================================
- Berisi table tentang daftar events yang terdiri 
  dari kolom dari tabel master data dan action 
  dengan pagination, sort, dan search bar.
- Berisi kumpulan master data (provinsi, user, 
  kota, supir, promo, jenis kendaraan, kendaraan, 
  bank, hari libur, kusisioner dan servis).

==============================================
	       Master Data Provinsi
		    (Admin)
==============================================
- Berisi kumpulan data provinsi yang berada di 
  indonesia.
- Terdiri kolom nama provinsi dan kolom action 
  untuk mengedit data.
- Terdapat tombol create event untuk membuat
  sebuah event baru.
- Untuk menambahkan provinsi baru, harus mengisi 
  seluruh kolom form create event yang tersedia.
- Jika sudah, klik submit form untuk menyimpan
  provinsi tersebut, kemudian harga yang baru 
  ditambahkan otomatis akan muncul di dalam tabel.
- Di bagian action table terdapat tombol edit
  yang akan mengarah ke halaman edit provinsi.

==============================================
	       Master Data User
		    (Admin)
==============================================
- Berisi kumpulan data User yang menggunakan 
  web Tricone Travel.
- Terdiri dari kolom nama member, address, number 
  phone, email, username, status, role dan kolom 
  action menu untuk mengedit data.
- Jika ingin menambahkan user baru dapat klik button
  create new kemudian mengisi form, setelah itu klik
  button submit, untuk melakukan aktivasi user
  dapat membuka email yang sudah Tricone Travel 
  kirimkan kemudian klik link aktivasi.

==============================================
	      	 List Price Menu
		   (Admin)
==============================================
- Berisi table tentang daftar events yang terdiri 
  dari kolom dari tabel list price dan action 
  dengan pagination, sort, dan search bar.
- Berisi kumpulan data harga sewa kendaraan.
- Menampilkan kolom kendaraan, kota, durasi 
  perjalanan dan harga.	
- Jika ingin menambahkan harga baru dapat klik button
  create new kemudian mengisi form, setelah itu klik
  button submit, kemudian harga yang baru ditambahkan
  otomatis akan muncul di dalam tabel.
- Di bagian action table terdapat tombol edit
  yang akan mengarah ke halaman edit harga.

==============================================
	      List Testimoni User Menu
		     (Admin)
==============================================
- Berisi table tentang daftar events yang terdiri 
  dari kolom dari tabel list price dan action 
  dengan pagination, sort, dan search bar.
- Berisi table testimoni yang dikirimkan oleh 
  pengguna dari tricone travel.
- Menampilkan kolom email pengirim, nama pengirim,
  subjek, dan testimoni dari user, kemudian ada
  action untuk mengaktifkan atau menonaktifkan
  testimoni.
- Jika testimoni ingin ditampilkan ke halaman utama
  website tricone travel maka dapat klik button 
  activation.

==============================================
	         Transaksi Menu
		   (Admin)
==============================================
- Tabel tentang daftar events yang terdiri 
  dari kolom dari tabel list price dan action 
  dengan pagination, sort, dan search bar.
- Berisi table laporan tentang aktivitas pemesanan,
  refund dan re-schedule.

==============================================
	        List Transaksi
		   (Admin)
==============================================
- Menampilkan kumpulan dari aktivitas transaksi
  yang dilakukan oleh user.
- Berisi tabel nama member, total transaksi, foto 
  foto pembayaran, start date, end date, approval,
  catatan user dan cataan admin.
- Kemudian terdapat button action detail, jika 
  di klik akan diarahkan ke tabel detail transaksi.
- Berisi tabel nama member, nama kendaraan,harga, 
  hari libur, start date, end date, nama kota,
  total kendaraan, nama supir.

==============================================
	         List Refund
		   (Admin)
==============================================
- Menampilkan list dari refund atau pengembalian dana 
- Menampilkan nama member, start date, kota tujuan,
  alasan pengajuan refund, foto bukti transfer dari 
  pengembalian dana yang dilakukan oleh admin, total
  total pembayaran, status persetujuan, dan status 
  refund.
- Kemudian terdapat button diterima atau ditolak 
  digunakan jika ingin menolak atau menerima pengajuan 
  refund, ada juga button foto untuk memasukkan 
  foto dari bukti transfer pengembalian dana. 
- Kemudian terdapat button action detail, jika 
  di klik akan diarahkan ke tabel detail refund.
- Berisi tabel nama member, nama kendaraan,harga, 
  hari libur, start date, end date, nama kota,
  total kendaraan, nama supir.

==============================================
	         List Reschedule
		     (Admin)
==============================================
- Menampilkan list data dari aktivitas penggantian 
  jadwal.
- Menampilkan member name, total transaction, balance,
  payment photo, start date, end date, admin 
  description, user description, approval, status.
- Kemudian terdapat button diterima atau ditolak 
  digunakan jika ingin menolak atau menerima pengajuan
  penggantian jadawal.

==============================================
	           Menu Report
		     (Admin)
==============================================
- Menampilkan List Report dari member, city, vehicle, 
  transaction, order detail, refund.
- Report bisa di download jika ngklik button save 
  yang terdapat pada tampilan report.

==============================================
	          Log Activity
		     (Admin)
==============================================
- Terdapat menu Audit Trail dan Eror Log.

==============================================
	     Log Activity Audit Trail
		     (Admin)
==============================================
- Menampilkan history aktivitas dari kegiatan yang 
  dilakukan oleh pengguna web Tricone Travel yang 
  terdiri dari kolom dari tabel list history dan 
  action dengan pagination, sort, dan search bar.
- Menampilkan id user, nama tabel, aksi (edit, add),
  value lama, value baru.

==============================================
	     Log Activity Eror Log
		     (Admin)
==============================================
- Menampilkan history eror dari kegiatan yang dilakukan 
  oleh pengguna web Tricone Travel yang terdiri 
  dari kolom dari tabel list history dan action 
  dengan pagination, sort, dan search bar.
- Menampilkan id user, error time, eror trace, eror 
  description.