USE [WiaClass]
GO
/****** Object:  Table [dbo].[Tbl_QuizNilai]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_QuizNilai](
	[Pk_Id_QuizNilai] [int] IDENTITY(1,1) NOT NULL,
	[Tanggal] [datetime] NULL,
	[NilaiQuiz] [real] NULL,
	[Fk_Id_Kategori] [int] NULL,
	[Fk_Id_Siswa] [int] NULL,
	[Eval_Per_Hari] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_QuizNilai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Kelas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Kelas](
	[Pk_Id_Kelas] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Kelas] [varchar](50) NULL,
	[Fk_Id_Sekolah] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Kelas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Guru]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Guru](
	[Pk_Id_Guru] [int] IDENTITY(1,1) NOT NULL,
	[NIP] [varchar](50) NULL,
	[Nama] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Password] [varchar](300) NULL,
	[No_Telp] [varchar](50) NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Fk_Id_Mata_Pelajaran] [int] NULL,
	[Fk_Id_Jenis_Kelamin] [int] NULL,
	[Role] [int] NULL,
	[Activated] [bit] NOT NULL,
	[Foto_Profil] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Guru] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Siswa]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Siswa](
	[Pk_Id_Siswa] [int] IDENTITY(1,1) NOT NULL,
	[Nomor_Induk] [varchar](50) NULL,
	[Nama] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Password] [varchar](300) NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Fk_Id_Kelas] [int] NULL,
	[Fk_Id_Jenis_Kelamin] [int] NULL,
	[Role] [int] NULL,
	[Activated] [bit] NOT NULL,
	[Foto_Profil] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Siswa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Kategori]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Kategori](
	[Pk_Id_Kategori] [int] IDENTITY(1,1) NOT NULL,
	[KatName] [varchar](50) NOT NULL,
	[Fk_Id_Guru] [int] NULL,
	[Fk_Id_Kelas] [int] NULL,
	[StatusQuiz] [bit] NULL,
	[MulaiQuiz] [datetime] NULL,
	[BatasQuiz] [datetime] NULL,
	[WaktuPengerjaan] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Kategori] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_EvaluasiKuis]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_EvaluasiKuis] as
select * from (
select
KatName,
Tbl_Siswa.Nama as Nama_Siswa,
Nama_Kelas,
NilaiQuiz,
Tanggal,
BatasQuiz,
Tbl_Guru.Nama as Nama_Guru,
Tbl_QuizNilai.Eval_Per_Hari,
Fk_Id_Guru,
Fk_Id_Siswa,
Tbl_Siswa.Fk_Id_Kelas,
row_number() over(partition by KatName, Tbl_Siswa.Nama order by Tanggal desc) as terbaru
from Tbl_Kategori
inner join Tbl_QuizNilai
on Tbl_Kategori.Pk_Id_Kategori = Tbl_QuizNilai.Fk_Id_Kategori
inner join Tbl_Siswa
on Tbl_QuizNilai.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Guru
on Tbl_Guru.Pk_Id_Guru = Tbl_Kategori.Fk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Siswa.Fk_Id_Kelas
) t
where t.terbaru = 1
GO
/****** Object:  Table [dbo].[Tbl_Tugas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Tugas](
	[Id_Tugas] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Tugas] [varchar](500) NULL,
	[Waktu_Pemberian] [datetime] NULL,
	[Batas_Pengumpulan] [datetime] NULL,
	[Fk_Id_Kelas] [int] NOT NULL,
	[Fk_Id_Guru] [int] NOT NULL,
	[Fk_Id_Sekolah] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Tugas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Nilai]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Nilai](
	[Pk_Id_Nilai] [int] IDENTITY(1,1) NOT NULL,
	[Nilai] [float] NULL,
	[Status] [varchar](20) NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Fk_Id_Siswa] [int] NULL,
	[Fk_Id_Mata_Pelajaran] [int] NULL,
	[Fk_Id_Guru] [int] NULL,
	[Fk_Id_Tugas] [int] NULL,
	[Fk_Id_Pengumpulan_Tugas] [int] NULL,
	[Fk_Id_Kelas] [int] NULL,
	[Eval_Per_Hari] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Nilai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Pengumpulan_Tugas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Pengumpulan_Tugas](
	[Pk_Id_Pengumpulan_Tugas] [int] IDENTITY(1,1) NOT NULL,
	[Waktu_Pengumpulan] [datetime] NULL,
	[Url_File] [varchar](100) NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Fk_Id_Tugas] [int] NULL,
	[Fk_Id_Siswa] [int] NULL,
	[Status_Nilai] [bit] NULL,
	[Fk_Id_Kelas] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Pengumpulan_Tugas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_EvaluasiTugas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_EvaluasiTugas] as
select * from (
select
Nama_Tugas,
Tbl_Siswa.Nama as Nama_Siswa,
Nilai,
Waktu_Pengumpulan as Tanggal,
Batas_Pengumpulan,
Tbl_Guru.Nama as Nama_Guru,
Nama_Kelas,
Tbl_Nilai.Eval_Per_Hari,
Tbl_Tugas.Fk_Id_Guru,
Tbl_Nilai.Fk_Id_Siswa,
Tbl_Tugas.Fk_Id_Kelas,
row_number() over(partition by Nama_Tugas, Tbl_Siswa.Nama order by Waktu_Pengumpulan desc) as terbaru
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Nilai
on Tbl_Pengumpulan_Tugas.Pk_Id_Pengumpulan_Tugas = Tbl_Nilai.Fk_Id_Pengumpulan_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Nilai.Fk_Id_Siswa
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
) t
where t.terbaru = 1
GO
/****** Object:  View [dbo].[Vw_EvaluasiTotal]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_EvaluasiTotal] as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tanggal )) AS int), 0) AS No, Tugas_Kuis, Nama_Siswa, Nilai, Tanggal, Batas_Pengumpulan, Nama_Guru, Eval_Per_Hari, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru
FROM(
Select KatName as Tugas_Kuis, Nama_Siswa, NilaiQuiz as Nilai, Tanggal, BatasQuiz as Batas_Pengumpulan, Nama_Guru, Eval_Per_Hari, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru From Vw_EvaluasiKuis
Union
Select Nama_Tugas as Tugas_Kuis, Nama_Siswa, Nilai, Tanggal, Batas_Pengumpulan, Nama_Guru, Eval_Per_Hari, Fk_Id_Guru, Fk_Id_Siswa, Fk_Id_Kelas, terbaru From Vw_EvaluasiTugas
) AS MyResult
GO
/****** Object:  Table [dbo].[Tbl_Mata_Pelajaran]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Mata_Pelajaran](
	[Pk_Id_Mata_Pelajaran] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Mata_Pelajaran] [varchar](50) NULL,
	[Nama_Mata_Pelajaran] [varchar](100) NULL,
	[Fk_Id_Sekolah] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Mata_Pelajaran] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_EvaluasiPerTugas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_EvaluasiPerTugas] as
select * from (
select
ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Waktu_Pengumpulan )) AS int), 0) AS No,
Nama_Mata_Pelajaran,
Nama_Tugas,
Waktu_Pengumpulan,
Nilai,
Tbl_Guru.Nama as Nama_Guru,
Tbl_Siswa.Nama as Nama_Siswa,
Tbl_Nilai.Eval_Per_Hari,
Tbl_Nilai.Fk_Id_Sekolah,
Tbl_Nilai.Fk_Id_Guru,
Tbl_Nilai.Fk_Id_Siswa,
Tbl_Nilai.Fk_Id_Mata_Pelajaran,
Tbl_Nilai.Fk_Id_Tugas,
Tbl_Nilai.Fk_Id_Kelas,
row_number() over(partition by Nama_Tugas, Tbl_Siswa.Nama order by Waktu_Pengumpulan desc) as terbaru
from Tbl_Nilai
inner join Tbl_Pengumpulan_Tugas
on Tbl_Nilai.Fk_Id_Pengumpulan_Tugas = Tbl_Pengumpulan_Tugas.Pk_Id_Pengumpulan_Tugas
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Nilai.Fk_Id_Mata_Pelajaran
inner join Tbl_Tugas
on Tbl_Nilai.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Guru
on Tbl_Guru.Pk_Id_Guru = Tbl_Nilai.Fk_Id_Guru
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Nilai.Fk_Id_Siswa
) t
where t.terbaru = 1
GO
/****** Object:  Table [dbo].[Tbl_Jenis_Kelamin]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Jenis_Kelamin](
	[Pk_Id_Jenis_Kelamin] [int] IDENTITY(1,1) NOT NULL,
	[Jenis_Kelamin] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Jenis_Kelamin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Sekolah]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Sekolah](
	[Pk_Id_Sekolah] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Sekolah] [varchar](100) NULL,
	[Logo] [varchar](100) NULL,
	[No_Telp] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Alamat] [varchar](200) NULL,
	[Password] [varchar](300) NULL,
	[Role] [int] NULL,
	[Activated] [bit] NOT NULL,
	[ActivationCode] [uniqueidentifier] NOT NULL,
	[ResetPasswordCode] [varchar](500) NULL,
	[Tanggal_Registrasi] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Sekolah] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_Data_Guru]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Data_Guru] AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tbl_Guru.Pk_Id_Guru )) AS int), 0) AS ID, Pk_Id_Guru, NIP, Nama, Tbl_Guru.Email, Tbl_Guru.No_Telp, Tbl_Guru.Password, Tbl_Guru.Fk_Id_Sekolah as ID_Sekolah, Tbl_Sekolah.Nama_Sekolah as Sekolah, Tbl_Mata_Pelajaran.Nama_Mata_Pelajaran as 'Mata_Pelajaran', Tbl_Jenis_Kelamin.Jenis_Kelamin as 'Jenis_Kelamin', Tbl_Guru.Activated as 'Status_Aktivasi', Tbl_Guru.Foto_Profil as 'Foto_Profil'
FROM Tbl_Guru
LEFT JOIN Tbl_Sekolah ON Fk_Id_Sekolah = Pk_Id_Sekolah
LEFT JOIN Tbl_Jenis_Kelamin ON Fk_Id_Jenis_Kelamin = Pk_Id_Jenis_Kelamin
LEFT JOIN Tbl_Mata_Pelajaran ON Fk_Id_Mata_Pelajaran = Pk_Id_Mata_Pelajaran
GO
/****** Object:  View [dbo].[Vw_Data_Siswa]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Data_Siswa] AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Tbl_Siswa.Pk_Id_Siswa )) AS int), 0) AS ID, Pk_Id_Siswa, Nomor_Induk, Nama, Tbl_Siswa.Email, Tbl_Siswa.Password, Tbl_Siswa.Fk_Id_Sekolah as ID_Sekolah, Tbl_Sekolah.Nama_Sekolah as Sekolah, Fk_Id_Kelas as ID_Kelas, Tbl_Kelas.Nama_Kelas as 'Kelas', Tbl_Jenis_Kelamin.Jenis_Kelamin as 'Jenis_Kelamin', Tbl_Siswa.Activated as 'Status_Aktivasi', Tbl_Siswa.Foto_Profil as 'Foto_Profil'
FROM Tbl_Siswa
LEFT JOIN Tbl_Sekolah ON Fk_Id_Sekolah = Pk_Id_Sekolah
LEFT JOIN Tbl_Jenis_Kelamin ON Fk_Id_Jenis_Kelamin = Pk_Id_Jenis_Kelamin
LEFT JOIN Tbl_Kelas ON Fk_Id_Kelas = Pk_Id_Kelas
GO
/****** Object:  View [dbo].[Vw_User]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[Vw_User] AS
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Email )) AS int), 0) AS ID, Nama, Email, Password, Role, Activated, FK_ID, Foto_Profil, Id_Sekolah
FROM(
Select Nama_Sekolah as Nama, Email, Password, Role, Activated, Pk_Id_Sekolah as FK_ID, Logo as Foto_Profil, Pk_Id_Sekolah as Id_Sekolah From Tbl_Sekolah
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Guru as FK_ID, Foto_Profil, Fk_Id_Sekolah as Id_Sekolah From Tbl_Guru
Union
Select Nama, Email, Password, Role, Activated, Pk_Id_Siswa as FK_ID, Foto_Profil, Fk_Id_Sekolah as Id_Sekolah From Tbl_Siswa) AS MyResult
GO
/****** Object:  View [dbo].[Vw_TugasKumpul]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_TugasKumpul] as
select Pk_Id_Pengumpulan_Tugas, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Url_File )) AS int), 0) AS No, Nama_Sekolah, Nama_Tugas, Tbl_Siswa.Nama as Nama_Siswa, Waktu_Pengumpulan, Url_File, Fk_Id_Tugas as Id_Tugas, Status_Nilai
from Tbl_Pengumpulan_Tugas
inner join Tbl_Tugas
on Tbl_Tugas.Id_Tugas = Tbl_Pengumpulan_Tugas.Fk_Id_Tugas
inner join Tbl_Sekolah
on Tbl_Sekolah.Pk_Id_Sekolah = Tbl_Pengumpulan_Tugas.Fk_Id_Sekolah
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Pengumpulan_Tugas.Fk_Id_Siswa
GO
/****** Object:  Table [dbo].[Tbl_Absensi]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Absensi](
	[Pk_Id_Absensi] [int] IDENTITY(1,1) NOT NULL,
	[Tanggal] [datetime] NULL,
	[Status] [varchar](20) NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Fk_Id_Siswa] [int] NULL,
	[Fk_Id_Mata_Pelajaran] [int] NULL,
	[Fk_Id_Guru] [int] NULL,
	[Status_Absensi] [bit] NULL,
	[Url_File] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Absensi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Role]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Role](
	[Pk_Id_Role] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Role] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_KehadiranGuru]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[Vw_KehadiranGuru] as
select Pk_Id_Absensi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama_Mata_Pelajaran, Nama as 'Nama_Guru', Status, Status_Absensi, Url_File, Nama_Role as Role
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Absensi.Fk_Id_Mata_Pelajaran
inner join Tbl_Guru
on Tbl_Absensi.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Role
on Tbl_Role.Pk_Id_Role = Tbl_Guru.Role
GO
/****** Object:  View [dbo].[Vw_KehadiranSiswa]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[Vw_KehadiranSiswa] as
select Pk_Id_Absensi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama as 'Nama_Siswa', Nama_Kelas as 'Kelas', Status, Status_Absensi, Url_File, Nama_Role as Role
from Tbl_Absensi
inner join Tbl_Sekolah
on Tbl_Absensi.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Siswa
on Tbl_Absensi.Fk_Id_Siswa = Tbl_Siswa.Pk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Siswa.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
inner join Tbl_Role
on Tbl_Role.Pk_Id_Role = Tbl_Siswa.Role
GO
/****** Object:  View [dbo].[Vw_Student]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_Student] as
select Nama_Kelas as "Nama Kelas", COUNT(*) as "Jumlah Murid", Nama_Sekolah as "Nama Sekolah"
from Tbl_Siswa
inner join Tbl_Sekolah
on Tbl_Siswa.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
inner join Tbl_Kelas
on Tbl_Siswa.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
group by Nama_Kelas, Nama_Sekolah
GO
/****** Object:  View [dbo].[Vw_RekapAbsensi]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_RekapAbsensi] as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Absensi )) AS int), 0) AS No, Tanggal, Nama_Sekolah, Nama, Role, Status, Status_Absensi
FROM(
Select Pk_Id_Absensi, Tanggal, Nama_Sekolah, Nama_Siswa as Nama, Role, Status, Status_Absensi From Vw_KehadiranSiswa
Union
Select Pk_Id_Absensi, Tanggal, Nama_Sekolah, Nama_Guru as Nama, Role, Status, Status_Absensi From Vw_KehadiranGuru
) AS MyResult
GO
/****** Object:  View [dbo].[Vw_NilaiKuis]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_NilaiKuis] as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_QuizNilai )) AS int), 0) AS No, q.Tanggal, q.NilaiQuiz, s.Nama as 'Nama_Siswa', kel.Nama_Kelas, k.KatName as 'Kategori', g.Nama as 'Nama_Guru', g.Pk_Id_Guru as'ID_Guru'
from Tbl_QuizNilai q
inner join Tbl_Siswa s
on q.Fk_Id_Siswa = s.Pk_Id_Siswa
inner join Tbl_Kategori k
on q.Fk_Id_Kategori = k.Pk_Id_Kategori
inner join Tbl_Kelas kel
on k.Fk_Id_Kelas = kel.Pk_Id_Kelas
inner join Tbl_Guru g
on k.Fk_Id_Guru = g.Pk_Id_Guru
GO
/****** Object:  View [dbo].[Vw_DashboardAdmin]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_DashboardAdmin] as
SELECT Pk_Id_Sekolah, Nama_Sekolah, (SELECT COUNT(*) FROM Tbl_Guru g WHERE g.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah) AS 'Jumlah_Guru',
(SELECT COUNT(*) FROM Tbl_Siswa s WHERE s.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah) AS 'Jumlah_Siswa',
(SELECT COUNT(*) FROM Tbl_Kelas s WHERE s.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah) AS 'Jumlah_Kelas'
from Tbl_Sekolah
GO
/****** Object:  View [dbo].[Vw_DashboardAdmin2]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_DashboardAdmin2] as
select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Sekolah )) AS int), 0) AS No, Pk_Id_Sekolah, Nama_Sekolah, Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran, Tbl_Mata_Pelajaran.Nama_Mata_Pelajaran
from Tbl_Guru
left join Tbl_Mata_Pelajaran
on Tbl_Guru.Fk_Id_Mata_Pelajaran = Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran
left join Tbl_Sekolah
on Tbl_Guru.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
GO
/****** Object:  View [dbo].[Vw_DashboardAdmin3]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_DashboardAdmin3] as
select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Vw_DashboardAdmin2.Pk_Id_Sekolah )) AS int), 0) AS No, Tbl_Sekolah.Pk_Id_Sekolah, Vw_DashboardAdmin2.Nama_Mata_Pelajaran, COUNT(*) as "Jumlah_Guru"
from Vw_DashboardAdmin2
join Tbl_Sekolah on Vw_DashboardAdmin2.Pk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
group by Vw_DashboardAdmin2.Pk_Id_Sekolah, Tbl_Sekolah.Pk_Id_Sekolah, Vw_DashboardAdmin2.Nama_Mata_Pelajaran
GO
/****** Object:  View [dbo].[Vw_DashboardAdmin4]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_DashboardAdmin4] as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kelas )) AS int), 0) AS No, Fk_Id_Sekolah, Pk_Id_Kelas, Nama_Kelas,
(SELECT COUNT(*) FROM Tbl_Siswa s WHERE s.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas) AS 'Jumlah_Siswa'
from Tbl_Kelas
GO
/****** Object:  Table [dbo].[Tbl_Evaluasi]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Evaluasi](
	[Pk_Id_Evaluasi] [int] IDENTITY(1,1) NOT NULL,
	[Nilai] [int] NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Fk_Id_Siswa] [int] NULL,
	[Fk_Id_Guru] [int] NULL,
	[Evaluasi] [varchar](max) NULL,
	[Tanggal_Evaluasi] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Evaluasi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_HasilEvaluasi]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_HasilEvaluasi] as
select e.Pk_Id_Evaluasi, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY e.Pk_Id_Evaluasi )) AS int), 0) AS No,e.Nilai, g.Pk_Id_Guru as Id_Guru, g.Nama as Nama_Guru, s.Pk_Id_Siswa as Id_Siswa, s.Nama as Nama_Siswa, e.Evaluasi, se.Pk_Id_Sekolah as Id_Sekolah, se.Nama_Sekolah as Nama_Sekolah
from Tbl_Evaluasi e
inner join Tbl_Guru g
on e.Fk_Id_Guru = g.Pk_Id_Guru
inner join Tbl_Siswa s
on e.Fk_Id_Siswa = s.Pk_Id_Siswa
inner join Tbl_Sekolah se
on e.Fk_Id_Sekolah = se.Pk_Id_Sekolah
GO
/****** Object:  View [dbo].[Vw_DashboardGuru1]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_DashboardGuru1] as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kelas )) AS int), 0) AS No, Pk_Id_Kelas, Nama_Kelas, (SELECT AVG(Nilai) FROM Tbl_Nilai where Tbl_Nilai.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas) AS 'Rata-rata', Tbl_Kelas.Fk_Id_Sekolah
from Tbl_Kelas
inner join Tbl_Nilai
on Tbl_Nilai.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
group by Pk_Id_Kelas, Nama_Kelas, Tbl_Kelas.Fk_Id_Sekolah
GO
/****** Object:  View [dbo].[Vw_DashboardSA1]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_DashboardSA1] as
SELECT ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY MONTH(Tanggal_Registrasi) )) AS int), 0) AS No, (MONTH(Tanggal_Registrasi) * 10000) + YEAR(Tanggal_Registrasi) AS mmYYYY, COUNT(*) as Jumlah_Sekolah
FROM Tbl_Sekolah
GROUP BY MONTH(Tanggal_Registrasi), (MONTH(Tanggal_Registrasi) * 10000) + YEAR(Tanggal_Registrasi)
GO
/****** Object:  View [dbo].[Vw_KategoriQuiz]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_KategoriQuiz] as
select Pk_Id_Kategori, ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Pk_Id_Kategori )) AS int), 0) AS No, KatName as 'Nama_Kategori', Fk_Id_Guru as 'IDGuru', Fk_Id_Kelas as 'IDKelas', Nama as 'Nama_Guru', Nama_Kelas as 'Kelas', StatusQuiz as 'Status' , MulaiQuiz as 'Mulai_Kuis', BatasQuiz as 'Batas_Kuis', WaktuPengerjaan as Waktu_Pengerjaan
from Tbl_Kategori
inner join Tbl_Guru
on Tbl_Kategori.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Kelas
on Tbl_Kategori.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
GO
/****** Object:  View [dbo].[Vw_KirimTugas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_KirimTugas] as
select ISNULL(CAST((ROW_NUMBER() OVER( ORDER BY Url_File )) AS int), 0) AS No, Id_Tugas, Nama_Tugas, Nama_Mata_Pelajaran, Batas_Pengumpulan, Waktu_Pengumpulan, Tbl_Siswa.Nama as Nama_Siswa, Nama_Kelas, Url_File
from Tbl_Tugas
inner join Tbl_Pengumpulan_Tugas
on Tbl_Pengumpulan_Tugas.Fk_Id_Tugas = Tbl_Tugas.Id_Tugas
inner join Tbl_Siswa
on Tbl_Siswa.Pk_Id_Siswa = Tbl_Pengumpulan_Tugas.Fk_Id_Siswa
inner join Tbl_Kelas
on Tbl_Kelas.Pk_Id_Kelas = Tbl_Tugas.Fk_Id_Kelas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Guru.Fk_Id_Mata_Pelajaran
GO
/****** Object:  View [dbo].[Vw_Tugas]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Vw_Tugas] as 
select Id_Tugas, Nama_Tugas, Waktu_Pemberian, Batas_Pengumpulan, Nama_Kelas, Nama as Nama_Guru, Nama_Mata_Pelajaran, Pk_Id_Sekolah as Id_Sekolah
from Tbl_Tugas
inner join Tbl_Kelas
on Tbl_Tugas.Fk_Id_Kelas = Tbl_Kelas.Pk_Id_Kelas
inner join Tbl_Guru
on Tbl_Tugas.Fk_Id_Guru = Tbl_Guru.Pk_Id_Guru
inner join Tbl_Mata_Pelajaran
on Tbl_Mata_Pelajaran.Pk_Id_Mata_Pelajaran = Tbl_Guru.Fk_Id_Mata_Pelajaran
inner join Tbl_Sekolah
on Tbl_Guru.Fk_Id_Sekolah = Tbl_Sekolah.Pk_Id_Sekolah
GO
/****** Object:  Table [dbo].[Tbl_AuditTrail]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_AuditTrail](
	[Pk_Id_AuditTrail] [int] IDENTITY(1,1) NOT NULL,
	[Error_msg] [varchar](500) NULL,
	[User_login] [varchar](50) NULL,
	[Error_date] [datetime] NULL,
	[Error_section] [varchar](200) NULL,
	[Error_action] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_AuditTrail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_AuditTrail_History]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_AuditTrail_History](
	[Pk_Id_AuditTrail_History] [int] IDENTITY(1,1) NOT NULL,
	[Tanggal] [datetime] NULL,
	[History_Data] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_AuditTrail_History] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_FAQ]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_FAQ](
	[Pk_Id_FAQ] [int] IDENTITY(1,1) NOT NULL,
	[Pertanyaan] [varchar](500) NULL,
	[Jawaban] [varchar](500) NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_FAQ] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Kontak_Kami]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Kontak_Kami](
	[Pk_Id_Kontak_Kami] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Subjek] [varchar](100) NULL,
	[Pesan] [varchar](800) NULL,
	[Waktu] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Kontak_Kami] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Pengumuman]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Pengumuman](
	[Pk_Id_Pengumuman] [int] IDENTITY(1,1) NOT NULL,
	[Judul_Pengumuman] [varchar](100) NULL,
	[Pengumuman] [varchar](max) NULL,
	[Fk_Id_Sekolah] [int] NULL,
	[Waktu] [datetime] NULL,
	[Background] [varchar](20) NULL,
	[status] [bit] NULL,
	[file_Tambahan] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Pengumuman] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Pertanyaan]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Pertanyaan](
	[Pk_Id_Pertanyaan] [int] IDENTITY(1,1) NOT NULL,
	[Pertanyaan] [varchar](max) NULL,
	[OptA] [varchar](max) NULL,
	[OptB] [varchar](max) NULL,
	[OptC] [varchar](max) NULL,
	[OptD] [varchar](max) NULL,
	[CorrectOpt] [varchar](max) NULL,
	[Fk_Id_Kategori] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Pertanyaan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_SoalSSIS]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_SoalSSIS](
	[Pk_Id_Pertanyaan] [int] IDENTITY(1,1) NOT NULL,
	[Pertanyaan] [nvarchar](255) NULL,
	[OptA] [nvarchar](255) NULL,
	[OptB] [nvarchar](255) NULL,
	[OptC] [nvarchar](255) NULL,
	[OptD] [nvarchar](255) NULL,
	[CorrectOpt] [nvarchar](255) NULL,
	[Fk_Id_Kategori] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Pertanyaan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Testimonial]    Script Date: 04/09/2021 15.25.07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Testimonial](
	[Pk_Id_Testimonial] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](100) NULL,
	[Foto] [varchar](50) NULL,
	[Posisi_Role] [varchar](50) NULL,
	[Testimoni] [varchar](800) NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Pk_Id_Testimonial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Tbl_Absensi] ON 

INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (1, CAST(N'2021-08-26T01:08:30.463' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (2, CAST(N'2021-08-26T01:09:12.253' AS DateTime), N'Pulang', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (3, CAST(N'2021-08-26T09:45:18.120' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (4, CAST(N'2021-08-26T09:53:53.437' AS DateTime), N'Pulang', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (5, CAST(N'2021-08-26T11:18:30.010' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (6, CAST(N'2021-08-26T11:18:42.340' AS DateTime), N'Pulang', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (7, CAST(N'2021-08-26T11:25:31.337' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (8, CAST(N'2021-08-26T11:26:29.097' AS DateTime), N'Pulang', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (9, CAST(N'2021-08-26T11:29:09.563' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (10, CAST(N'2021-08-26T23:29:52.157' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (11, CAST(N'2021-08-26T23:30:16.747' AS DateTime), N'Pulang', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (12, CAST(N'2021-08-26T23:31:01.570' AS DateTime), N'Hadir', 41, 3, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (13, CAST(N'2021-08-26T23:31:12.207' AS DateTime), N'Pulang', 41, 3, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (14, CAST(N'2021-08-27T09:16:34.793' AS DateTime), N'Hadir', 41, NULL, 6, 8, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (15, CAST(N'2021-08-27T09:21:45.463' AS DateTime), N'Hadir', 41, 3, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (16, CAST(N'2021-08-29T21:04:21.407' AS DateTime), N'Hadir', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (17, CAST(N'2021-08-29T21:04:40.603' AS DateTime), N'Pulang', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (18, CAST(N'2021-08-26T00:00:00.000' AS DateTime), N'Sakit', 41, 3, NULL, NULL, 1, N'LampiranAbsensi/Siswa/13/3_contoh-surat-izin-sekolah-karena-sakit-sd.png')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (19, CAST(N'2021-08-29T21:07:57.573' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (20, CAST(N'2021-08-29T21:08:16.127' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (21, CAST(N'2021-08-29T21:08:29.103' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (22, CAST(N'2021-08-29T00:00:00.000' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, N'LampiranAbsensi/Guru/B3/8_contoh-surat-izin-sekolah-karena-sakit-sd.png')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (23, CAST(N'2021-08-30T09:47:21.540' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (24, CAST(N'2021-08-27T00:00:00.000' AS DateTime), N'Sakit', 41, 3, NULL, NULL, 1, N'LampiranAbsensi/Siswa/13/3_contoh-surat-izin-sekolah-karena-sakit-sd.png')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (25, CAST(N'2021-08-31T00:56:52.450' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (26, CAST(N'2021-08-31T00:57:42.660' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (27, CAST(N'2021-08-31T00:57:54.340' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (28, CAST(N'2021-08-31T00:58:55.527' AS DateTime), N'Hadir', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (29, CAST(N'2021-08-31T00:59:20.743' AS DateTime), N'Hadir', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (30, CAST(N'2021-08-31T00:59:29.987' AS DateTime), N'Pulang', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (31, CAST(N'2021-08-31T09:51:08.033' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (32, CAST(N'2021-08-30T00:00:00.000' AS DateTime), N'Sakit', 41, 3, NULL, NULL, 1, N'LampiranAbsensi/Siswa/13/3_contoh-surat-izin-sekolah-karena-sakit-sd.png')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (33, CAST(N'2021-08-31T00:00:00.000' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, N'LampiranAbsensi/Guru/B3/8_box-liquid-food-container-outline_318-53891.jpg')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (34, CAST(N'2021-09-01T00:42:18.610' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (35, CAST(N'2021-09-01T00:43:41.060' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (36, CAST(N'2021-09-01T00:48:05.090' AS DateTime), N'Hadir', 41, 9, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (37, CAST(N'2021-09-01T00:49:00.363' AS DateTime), N'Pulang', 41, 9, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (38, CAST(N'2021-09-01T09:47:44.073' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (39, CAST(N'2021-08-31T00:00:00.000' AS DateTime), N'Sakit', 41, 3, NULL, NULL, 1, N'LampiranAbsensi/Siswa/13/3_contoh-surat-izin-sekolah-karena-sakit-sd.png')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (40, CAST(N'2021-09-01T09:59:43.127' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (41, CAST(N'2021-09-01T12:12:24.213' AS DateTime), N'Hadir', 41, 9, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (42, CAST(N'2021-09-01T18:27:52.227' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (43, CAST(N'2021-09-01T18:28:29.630' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (44, CAST(N'2021-09-01T18:31:49.147' AS DateTime), N'Hadir', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (45, CAST(N'2021-09-01T18:31:56.943' AS DateTime), N'Pulang', 41, 3, NULL, NULL, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (46, CAST(N'2021-08-31T00:00:00.000' AS DateTime), N'Sakit', 41, NULL, 6, 8, 1, N'LampiranAbsensi/Guru/B3/8_contoh-surat-izin-sekolah-karena-sakit-sd.png')
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (47, CAST(N'2021-09-02T00:02:12.093' AS DateTime), N'Hadir', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (48, CAST(N'2021-09-02T00:02:43.553' AS DateTime), N'Pulang', 41, NULL, 6, 8, 1, NULL)
INSERT [dbo].[Tbl_Absensi] ([Pk_Id_Absensi], [Tanggal], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Status_Absensi], [Url_File]) VALUES (49, CAST(N'2021-09-01T00:00:00.000' AS DateTime), N'Pulang', 41, 9, NULL, NULL, 1, N'LampiranAbsensi/Siswa/13/9_contoh-surat-izin-sekolah-karena-sakit-sd.png')
SET IDENTITY_INSERT [dbo].[Tbl_Absensi] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_AuditTrail] ON 

INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (1, N'System.ArgumentNullException: String reference not set to an instance of a String.
Parameter name: s
   at System.Text.Encoding.GetBytes(String s)
   at WIAClass.Controllers.AdminController.Crypto.Hash(String value) in D:\nawadata\Level2\WIAClass\WIAClass\WIAClass\Controllers\AdminController.cs:line 37
   at WIAClass.Controllers.AdminController.TambahDataGuru(CrudGuruModel modelGuru) in D:\nawadata\Level2\WIAClass\WIAClass\WIAClass\Controllers\AdminController.cs:line 251', N'Hardcode user', CAST(N'2021-08-20T10:31:22.830' AS DateTime), N'Tambah data Guru', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (2, N'System.InvalidCastException: Unable to cast object of type ''System.Int32'' to type ''System.String''.
   at WIAClass.Controllers.SiswaController.EndQuiz() in D:\nawadata\Level2\WIAClass\WIAClass\WIAClass\Controllers\SiswaController.cs:line 363', N'Hardcode user', CAST(N'2021-08-21T18:41:29.137' AS DateTime), N'Tambah data Tugas', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (3, N'System.InvalidCastException: Unable to cast object of type ''System.Int32'' to type ''System.String''.
   at WIAClass.Controllers.SiswaController.EndQuiz() in D:\nawadata\Level2\WIAClass\WIAClass\WIAClass\Controllers\SiswaController.cs:line 364', N'Hardcode user', CAST(N'2021-08-21T18:47:56.507' AS DateTime), N'Tambah data nilai kuis', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (4, N'System.NullReferenceException: Object reference not set to an instance of an object.
   at WIAClass.Controllers.AdminController.BuatPengumuman(CrudPengumuman Pengumuman) in D:\nawadata\Level2\WIAClass\WIAClass\WIAClass\Controllers\AdminController.cs:line 832', N'Hardcode user', CAST(N'2021-08-27T11:29:28.490' AS DateTime), N'Buat Pengumuman', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (5, N'Validation failed for one or more entities. See ''EntityValidationErrors'' property for more details.', N'SDN Imam', CAST(N'2021-08-29T20:30:07.570' AS DateTime), N'Buat Pengumuman', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (6, N'Specified cast is not valid.', N'daniel hendra', CAST(N'2021-08-30T20:21:52.383' AS DateTime), N'Tambah data nilai kuis', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (7, N'Access to the path ''C:\inetpub\wwwroot\wiaclass\assets\img\profile\41_sekolah.jpeg'' is denied.', N'SDN Imam', CAST(N'2021-09-01T22:19:35.477' AS DateTime), N'Edit Foto Profil', N'Belum ada action')
INSERT [dbo].[Tbl_AuditTrail] ([Pk_Id_AuditTrail], [Error_msg], [User_login], [Error_date], [Error_section], [Error_action]) VALUES (8, N'Access to the path ''C:\inetpub\wwwroot\wiaclass\assets\img\profile\41_sekolah2.jpg'' is denied.', N'SDN Imam', CAST(N'2021-09-01T22:20:00.283' AS DateTime), N'Edit Foto Profil', N'Belum ada action')
SET IDENTITY_INSERT [dbo].[Tbl_AuditTrail] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Evaluasi] ON 

INSERT [dbo].[Tbl_Evaluasi] ([Pk_Id_Evaluasi], [Nilai], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Guru], [Evaluasi], [Tanggal_Evaluasi]) VALUES (1, 80, 41, 9, 8, N'Bagus, tetap semangat', NULL)
INSERT [dbo].[Tbl_Evaluasi] ([Pk_Id_Evaluasi], [Nilai], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Guru], [Evaluasi], [Tanggal_Evaluasi]) VALUES (2, 80, 41, 4, 8, N'Semangat belajarnya sudah bagus, tetapi harus dicoba untuk teepat waktu mengumpulkannya', NULL)
INSERT [dbo].[Tbl_Evaluasi] ([Pk_Id_Evaluasi], [Nilai], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Guru], [Evaluasi], [Tanggal_Evaluasi]) VALUES (3, 83, 41, 3, 8, N'Bagus nilainya namun masih terlambat dalam mengumpulkan, coba untuk lebih tepat waktu', CAST(N'2021-09-01T09:58:21.163' AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_Evaluasi] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_FAQ] ON 

INSERT [dbo].[Tbl_FAQ] ([Pk_Id_FAQ], [Pertanyaan], [Jawaban], [Status]) VALUES (1, N'Bagaimana cara menggunakan aplikasi WiaClass ?', N'Untuk cara penggunaan aplikasi Wiaclass dapat dilihat pada foto cara penggunaan pada halaman ini, namun apabila ada pertanyaan lebih lanjut dapat menghubungi kami pada halaman Contact', 1)
INSERT [dbo].[Tbl_FAQ] ([Pk_Id_FAQ], [Pertanyaan], [Jawaban], [Status]) VALUES (2, N'Bagaimana jika saya seorang siswa ingin mendaftar WiaClass ?', N'Untuk memulai menggunakan aplikasi WiaClass, sekolah anda perlu untuk terdaftar terlebih dahulu pada aplikasi kami, anda dapat menghubungi guru maupun admin pada sekolah anda', 1)
INSERT [dbo].[Tbl_FAQ] ([Pk_Id_FAQ], [Pertanyaan], [Jawaban], [Status]) VALUES (3, N'Apakah murid dan guru perlu untuk mendaftar ?', N'Untuk guru serta murid tidak perlu mendaftar karena data akan dimasukkan terlebih dahulu oleh admin sekolah anda', 1)
INSERT [dbo].[Tbl_FAQ] ([Pk_Id_FAQ], [Pertanyaan], [Jawaban], [Status]) VALUES (4, N'Jika Saya mengalami error, kemana saya dapat menghubungi ?', N'Jika ada hal yang perlu diberitahukan seputar aplikasi WiaClass anda dapat menghubungi developer melalui Contact yang tertera pada halaman ini', 1)
INSERT [dbo].[Tbl_FAQ] ([Pk_Id_FAQ], [Pertanyaan], [Jawaban], [Status]) VALUES (5, N'Apakah saya perlu membayar untuk menggunakan aplikasi ini ?', N'Aplikasi WiaClass tidak memungut biaya apapun dari seluruh kegiatan kelas yang anda lakukan, Terima Kasih sudah menggunakan Wia Class', 1)
SET IDENTITY_INSERT [dbo].[Tbl_FAQ] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Guru] ON 

INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (6, N'782374623', N'Dani', N'Dani@dani.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'812636123', 41, 3, 1, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (7, N'748365', N'Eliza', N'eliza@eliza.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'82631623', 41, 6, 2, 3, 0, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (8, N'196312241989032006', N'Dadang', N'dadang@dadang.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'08364563456', 41, 6, 1, 3, 1, N'8_guru.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (9, N'1926458346623', N'Haikal', N'haikal@haikal.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'08564836432', 41, 3, 1, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (11, N'623478912387', N'Keira', N'keira@gmail.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'0861236152', 41, 6, 2, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (1013, N'453478593847', N'Jimmy', N'jimmy@jimmy.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'0823462634', 45, 11, 1, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (1014, N'23847823748', N'Nina', N'nina@nina.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'0823462347', 45, 12, 2, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (1015, N'2384723747234', N'Jidan', N'jidan@jidan.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'9238472347', 45, 11, 1, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (1016, N'196312241989032006', N'WiaTeacher', N'wiateacher@wiateacher.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'08564836432', 44, 17, 1, 3, 1, N'default.jpg')
INSERT [dbo].[Tbl_Guru] ([Pk_Id_Guru], [NIP], [Nama], [Email], [Password], [No_Telp], [Fk_Id_Sekolah], [Fk_Id_Mata_Pelajaran], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (1017, N'623478912387', N'testing', N'testing@gmail.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', N'08126312536', 45, 12, 1, 3, 1, N'default.jpg')
SET IDENTITY_INSERT [dbo].[Tbl_Guru] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Jenis_Kelamin] ON 

INSERT [dbo].[Tbl_Jenis_Kelamin] ([Pk_Id_Jenis_Kelamin], [Jenis_Kelamin]) VALUES (1, N'Laki-Laki')
INSERT [dbo].[Tbl_Jenis_Kelamin] ([Pk_Id_Jenis_Kelamin], [Jenis_Kelamin]) VALUES (2, N'Perempuan')
SET IDENTITY_INSERT [dbo].[Tbl_Jenis_Kelamin] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Kategori] ON 

INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (19, N'IPA 5', 8, 13, 1, CAST(N'2021-08-20T00:00:00.000' AS DateTime), CAST(N'2021-08-20T00:00:00.000' AS DateTime), 35)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (20, N'Kuis IPA 5 ke 3', 8, 13, 0, CAST(N'2021-08-25T00:00:00.000' AS DateTime), CAST(N'2021-08-25T00:00:00.000' AS DateTime), 40)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1018, N'test IPA 5 sebelum uts', 8, 13, 0, CAST(N'2021-08-23T00:00:00.000' AS DateTime), CAST(N'2021-08-23T00:00:00.000' AS DateTime), 60)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1019, N'Test IPA 5 ke 4', 8, 13, 0, CAST(N'2021-08-24T00:00:00.000' AS DateTime), CAST(N'2021-08-24T00:00:00.000' AS DateTime), 60)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1020, N'Latihan pretest UTS ', 8, 5, 0, CAST(N'2021-08-30T00:00:00.000' AS DateTime), CAST(N'2021-08-30T00:00:00.000' AS DateTime), 60)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1022, N'testing setelah edit kelas', 8, 7, 0, CAST(N'2021-08-25T00:00:00.000' AS DateTime), CAST(N'2021-08-25T00:00:00.000' AS DateTime), 60)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1023, N'Pretest UAS IPA kelas 5-A', 8, 13, 1, CAST(N'2021-08-26T00:00:00.000' AS DateTime), CAST(N'2021-08-26T00:00:00.000' AS DateTime), 60)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1024, N'Test sebelum uts IPA', 8, 13, 0, CAST(N'2021-08-27T00:00:00.000' AS DateTime), CAST(N'2021-08-27T00:00:00.000' AS DateTime), 60)
INSERT [dbo].[Tbl_Kategori] ([Pk_Id_Kategori], [KatName], [Fk_Id_Guru], [Fk_Id_Kelas], [StatusQuiz], [MulaiQuiz], [BatasQuiz], [WaktuPengerjaan]) VALUES (1027, N'testtest', 8, 10, 0, CAST(N'2021-09-01T00:00:00.000' AS DateTime), CAST(N'2021-09-01T00:00:00.000' AS DateTime), 40)
SET IDENTITY_INSERT [dbo].[Tbl_Kategori] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Kelas] ON 

INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (1, N'Kelas 1-A', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (2, N'Kelas 1-B', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (3, N'Kelas 1-C', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (4, N'Kelas 2-A', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (5, N'Kelas 2-B', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (6, N'Kelas 2-C', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (7, N'Kelas 3-A', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (8, N'Kelas 3-B', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (9, N'Kelas 3-C', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (10, N'Kelas 4-A', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (11, N'Kelas 4-B', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (12, N'Kelas 4-C', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (13, N'Kelas 5-A', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (14, N'Kelas 5-B', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (15, N'Kelas 5-C', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (16, N'Kelas 6-A', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (17, N'Kelas 6-B', 41)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (18, N'Kelas 6-C', 31)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (20, N'4-A', 45)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (22, N'Kelas 1-A', 51)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (23, N'Kelas 3-B', 52)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (24, N'Kelas 3-A', 52)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (25, N'Kelas 2-A', 53)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (26, N'Kelas 2-B', 53)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (27, N'Kelas 5-A', 44)
INSERT [dbo].[Tbl_Kelas] ([Pk_Id_Kelas], [Nama_Kelas], [Fk_Id_Sekolah]) VALUES (30, N'Kelas 4-B', 45)
SET IDENTITY_INSERT [dbo].[Tbl_Kelas] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Kontak_Kami] ON 

INSERT [dbo].[Tbl_Kontak_Kami] ([Pk_Id_Kontak_Kami], [Nama], [Email], [Subjek], [Pesan], [Waktu]) VALUES (2, N'Kinan', N'kinan@gmail.com', N'Pertanyaan', N'Bagaimana mendaftarkan anak saya?', CAST(N'2021-08-20T09:13:42.150' AS DateTime))
INSERT [dbo].[Tbl_Kontak_Kami] ([Pk_Id_Kontak_Kami], [Nama], [Email], [Subjek], [Pesan], [Waktu]) VALUES (1002, N'Mahatir', N'mahatir@gmail.com', N'Pertanyaan', N'Bagaimana untuk membuat akun sekolah?', CAST(N'2021-08-25T09:34:39.677' AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_Kontak_Kami] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Mata_Pelajaran] ON 

INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (1, N'A1', N'Bahasa Indonesia', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (2, N'A2', N'Bahasa Inggris', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (3, N'A3', N'Matematika', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (4, N'B1', N'Olahraga', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (5, N'B2', N'Pendidikan Kewarganegaraan', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (6, N'B3', N'Ilmu Pengetahuan Alam', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (7, N'C1', N'Ilmu Pengetahuan Sosial', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (8, N'C2', N'Kesenian', 41)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (11, N'BING', N'Bahasa Inggris', 45)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (12, N'BINDO', N'Bahasa Indonesia', 45)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (13, N'BING24', N'Bahasa Inggris', 51)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (14, N'K1', N'Agama Kristen', 52)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (15, N'A1', N'Agama Islam', 52)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (16, N'MAT', N'Matematika', 53)
INSERT [dbo].[Tbl_Mata_Pelajaran] ([Pk_Id_Mata_Pelajaran], [Kode_Mata_Pelajaran], [Nama_Mata_Pelajaran], [Fk_Id_Sekolah]) VALUES (17, N'MAT1', N'Matematika', 44)
SET IDENTITY_INSERT [dbo].[Tbl_Mata_Pelajaran] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Nilai] ON 

INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (23, 85, N'Tepat Waktu', 41, 3, 6, 8, 1005, 1010, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (24, 87, N'Tepat Waktu', 41, 3, 6, 8, 1003, 1003, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (25, 85, N'Tepat Waktu', 41, 3, 6, 8, 3, 1002, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (26, 87, N'Terlambat', 41, 3, 6, 8, 1003, 1013, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (27, 90, N'Tepat Waktu', 41, 3, 6, 8, 1005, 1014, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (28, 85, N'Tepat Waktu', 41, 3, 6, 8, 1005, 1006, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (29, 80, N'Tepat Waktu', 41, 3, 6, 8, 1005, 1009, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (30, 70, N'Tepat Waktu', 41, 4, 6, 8, 1005, 1008, 13, NULL)
INSERT [dbo].[Tbl_Nilai] ([Pk_Id_Nilai], [Nilai], [Status], [Fk_Id_Sekolah], [Fk_Id_Siswa], [Fk_Id_Mata_Pelajaran], [Fk_Id_Guru], [Fk_Id_Tugas], [Fk_Id_Pengumpulan_Tugas], [Fk_Id_Kelas], [Eval_Per_Hari]) VALUES (31, 78, N'Tepat Waktu', 41, 9, 6, 8, 1005, 1005, 13, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_Nilai] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Pengumpulan_Tugas] ON 

INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1, CAST(N'2021-08-19T20:34:35.730' AS DateTime), N'UploadTugas/5/3_bianglala-di-dufan.jpg', 20, 2, 3, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (2, CAST(N'2021-08-19T20:35:54.140' AS DateTime), N'UploadTugas/5/3_chicken-hand-drawn-outline_318-51868.jpg', 20, 1, 3, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (3, CAST(N'2021-08-20T09:26:38.560' AS DateTime), N'UploadTugas/5/3_box-liquid-food-container-outline_318-53891.jpg', 20, 3, 3, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (4, CAST(N'2021-08-20T10:06:01.620' AS DateTime), N'UploadTugas/5/3_ajinomoto.png', 20, 3, 3, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1002, CAST(N'2021-08-23T21:07:10.923' AS DateTime), N'UploadTugas/5/3_arrow-keys-vectors_637061.jpg', 41, 3, 3, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1003, CAST(N'2021-08-24T09:51:05.123' AS DateTime), N'UploadTugas/5/3_apple.png', 41, 1003, 3, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1004, CAST(N'2021-08-25T09:53:43.323' AS DateTime), N'UploadTugas/5/3_Contoh CV FIX.docx', 41, 1004, 3, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1005, CAST(N'2021-08-26T21:08:39.193' AS DateTime), N'UploadTugas/13/9_images.jpg', 41, 1005, 9, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1006, CAST(N'2021-08-26T21:13:55.767' AS DateTime), N'UploadTugas/13/3_Heart-Health.jpg', 41, 1005, 3, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1008, CAST(N'2021-08-29T22:43:51.373' AS DateTime), N'UploadTugas/13/4_PPL_C_ETS_05111640000016.zip', 41, 1005, 4, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1009, CAST(N'2021-08-30T09:54:24.613' AS DateTime), N'UploadTugas/13/3_PPL_C_ETS_05111640000016.zip', 41, 1005, 3, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1010, CAST(N'2021-08-31T10:00:18.103' AS DateTime), N'UploadTugas/13/3_ajinomoto.png', 41, 1005, 3, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1011, CAST(N'2021-08-31T10:34:44.973' AS DateTime), N'UploadTugas/13/3_images.jpg', 41, 1005, 3, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1012, CAST(N'2021-09-01T00:48:36.670' AS DateTime), N'UploadTugas/13/9_arrow-keys-vectors_637061.jpg', 41, 1004, 9, 0, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1013, CAST(N'2021-09-01T09:51:08.080' AS DateTime), N'UploadTugas/13/3_box-liquid-food-container-outline_318-53891.jpg', 41, 1003, 3, 1, 13)
INSERT [dbo].[Tbl_Pengumpulan_Tugas] ([Pk_Id_Pengumpulan_Tugas], [Waktu_Pengumpulan], [Url_File], [Fk_Id_Sekolah], [Fk_Id_Tugas], [Fk_Id_Siswa], [Status_Nilai], [Fk_Id_Kelas]) VALUES (1014, CAST(N'2021-09-01T16:42:46.067' AS DateTime), N'UploadTugas/13/3_1200px-WxWidgets.svg.png', 41, 1005, 3, 1, 13)
SET IDENTITY_INSERT [dbo].[Tbl_Pengumpulan_Tugas] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Pengumuman] ON 

INSERT [dbo].[Tbl_Pengumuman] ([Pk_Id_Pengumuman], [Judul_Pengumuman], [Pengumuman], [Fk_Id_Sekolah], [Waktu], [Background], [status], [file_Tambahan]) VALUES (2, N'Hati-hati Covid!', N'<h1 style="text-align: center;">HATI-HATI COVID</h1><p style="text-align: left;">Yang perlu diperhatikan adalah :</p><ol><li style="text-align: left;">Jangan lupa cuci tangan</li><li style="text-align: left;">Jaga jarak</li><li style="text-align: left;">Pakai masker</li><li style="text-align: left;">Jaga kesehatan</li></ol>                    ', 41, CAST(N'2021-08-30T00:35:14.050' AS DateTime), N'Beruang.png', 1, NULL)
INSERT [dbo].[Tbl_Pengumuman] ([Pk_Id_Pengumuman], [Judul_Pengumuman], [Pengumuman], [Fk_Id_Sekolah], [Waktu], [Background], [status], [file_Tambahan]) VALUES (3, N'Semangat Belajar', N'<h1 style="text-align: center; ">Semangat Belajar!</h1>', 41, CAST(N'2021-08-30T00:36:13.677' AS DateTime), N'Beruang.png', 1, NULL)
INSERT [dbo].[Tbl_Pengumuman] ([Pk_Id_Pengumuman], [Judul_Pengumuman], [Pengumuman], [Fk_Id_Sekolah], [Waktu], [Background], [status], [file_Tambahan]) VALUES (4, N'Pengumuman Libur Ujian Kelas 6', N'<h1 style="text-align: center;">PENGUMUMAN</h1><p style="text-align: center;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAwICQoJBwwKCQoNDAwOER0TERAQESMZGxUdKiUsKyklKCguNEI4LjE/MigoOk46P0RHSktKLTdRV1FIVkJJSkf/2wBDAQwNDREPESITEyJHMCgwR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0f/wgARCAPjAtADAREAAhEBAxEB/8QAGwABAAIDAQEAAAAAAAAAAAAAAAQFAQIDBgf/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIQAxAAAAD6qAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAAAAAAAAAAAAABkAAAAAAAAAAAAAAAAAGDzwAAB2LwAAAAAA8YU5OIBqTDYgFycTBKJBVl+UZBPRHmC/IhGNTYviAQTQ5F2VxbnqgAAAAAAAAAAAAAAAAAanykikgnFCWhPK4+sAAAAAAHlzJyK01JpLK4lE08+XJIK02IhglkUuSsLMrzuampFO5DO5DL49MAAAAAAAAAAAAAAAAADU+aGCYVBMMmCee+AAABQFIWBUHrDzZuST1R5Q4ncsSIVZPL48sdjuYIBKJhoRTUsyERC7IZAJhXliQDudS3KE0PcAAAAAAAAAAAAAAAGp5ssCOedMkk9GVp6UAAAHjClJZqe+PDnIFqVBwLIriyKItC4BUmh3KcuiKWJXmxFJpUHqiYeJLgpy8Kw6m5KK44n1AyAAAAAAAAAAAAAADU8gSixPkRdHviMcz1ABQEE7kQ0LQqi0Lk8iXhANixK4nkQ7HMrCcSjkcS2K0jG5OIpxOpxORLMkw4nYhlUWpsdCrLchHUiko3OZ6UAAAAAAAAAAAAGp8wPTnlz50Qj6yTTQ+ggHjTQrilJ5YFeWhZnmyWVhNPQlacjBUFmQiUCQUh7koCATyWVpVE8mlkUZkmHE6mhSl2QDsciUVhYlgcSpLs9uAAAAAAAAAAAADU+TFkeBLM4Ec9OWh9IAAKg1IhanMgFkWBWnQwcQbHUinYyQy8IBqVZZmhBOx0Jhg1NTgdzJk2IZPOBsczJ2NSrPQkclgAAAAAAAAAAAAA1Pkh6o84RC0L88iXJ9DAAPDnnz0J5YviwPPH0c+OHqDuSiOeVPVAogTj6afMyGSiwNynOx0LQ8ue5PmR6Ylnjz1BBLM2PKF2SyvLcpyEfWDxB9DAAAAAAAAAAAAABg+egnnEjmp549oexAAPPEc4FycCoLQjFkYOhGNCSVp0JxXFkWxVkMrSzOBFJxLNwVpPNAbkU6kM1LMyczoYOBsWhCLYAAAAAAAAAAAAAA8yefOx6UtD5abn0QsgAAeHO5qczYkl6aHkySbFkeXJ5NOBalCSjmZOJ68Hhj0xXkc7F0eKLgtyIedPSlOdjY7kYsTzp2I5LPbkkAAAAAAAAAAAAAAHlSEefLw0PUFsAAYMnnCIZJhWFqWJUEU2Ni2NCnIhPLkpzqdCsJheHYqSEaA2LEgGxZgrScVZ0OhEJBYlWdSGbHpyQAAAAAAAAAAAAAADBRHnSSesJZ8pLA7HsSGeVOhaHIgGpNOhTHcySzmcjsAdCYU5yJpzOhgsCOVhcFcamC9JB5siEklG5FOhgyZLYqyASzBHJpUnrSmB6MuQAAAAAAAAADkeQAI53MggGCvLojlIcCQdDQ2NTQ1OpyOpqcDYkEckA5mpoYNzYwdAXRgyDsQSoOxyNjqQSUag7GDJwOhoam5samDsWpYAAAvy1AAAAAAByPIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvy1AAAAAABg8uehJAByOpyKc0LUqyQSSqBakAwbm5cAAAAAAAAAAAAAAFUZJIMg1NzUGCtJJFLUwCAWJzNSuMEgsCSAAAAAADB406nrTIIJOIh54kG5uVxZkM5FqQTY7EotAAAAAAAAAAAAAACgIhzI5xJRyBLOpYkU7lQbnM5HU3NyyI5FLIuDIAAAAAAAAAAAByOpg0OgOR1AAAAAAAAAAAAAAAAAAOZqdjidgAAAAAAAAAAAAAADBUnYwbFgRyQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYPPEEEc9uRCYRTodgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADABg2AAAAAAABgGQDBkwZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMHly8IhcAjkgHhC/MlYWpANyGW51NSIQSwNzgYIxcFKbnrDIAAAAAAAAAAAAAAAAAAAAAAAAAAAABg8iSiaXAIhKIR5cimTmaFsQSKSyOWpzIZsdiaVRgwcSQe/MgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAriSdivJJbmQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCUJwLIryQekOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVpVnQuCgJxGJpBLEjHItypMFiZOJEJJcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0POGhennCeQC7IBHOpgvDzp1Mko1Op1LUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFWVBYlOXpyMghk4GpsYNTqDoRy5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIZSFkV50KMuScRiOTCCcDcySDmXJyLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIx509UZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKwqj05kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjlCTjYyDJzJBHLM6gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHA8+RyaciuJxqSjmemAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgyYMgAAGDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABzKMlHUgEk2BsQDmWhyOpWmxalQRy6LIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFcVRFJRocjYsiEQzsSDidSaQS2I5EJJdgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArykLIgFsczQgFscTgSSASQciYDQEIklcTTsTSjOJPNTsbnAsCpJZ1NTJg1JJEJBLKg7mDscDsdSEdDmdjU0B1NDgSSGaloRyyJAAAAAAAAAAAIJSkg1NTQwdAYJBCNzoYNSyKs7GpgGpg0LghnEsypIZOOhZlURyUaFaTTqbmhbHE5EUkkM0OpMIZsSCvO4LArSWRzkZOZ3NC3LAAAAAAAAAAAGAZAABgyAAAAAAYBkAAAAwZAAAAMGTBkAAAAAAAAGDJgyAAAAAAAAAAAUxXFoaFgV5FJpDOZdHA5HMkHM5nYlEc4kg4mpuYANDYlEQ2NTucDqV5aGCrJxuVxYlIXRKI5qaG5IIh1OpXE4rSyOJsamhyNyOTi3AAAAAAAAAABWlYZMHojy5KNTiZLUjEc5Ek1OZPOZWGxJLAhkMnEElg0LchnIklaRjuYO5aFOQSwI5aEU1LEgnQhHAuDJXmpZkYjloV5k1OhIO5WlkWQAAAAAAAAAAAAAAAAAAABgyAYMgAAGAZBgyAAADBkAAGDIBgyAAAAAAAAAAAAAAAACvKQsjJgkFEWpIIh2OpwOhzOxscSvLI4EkhG5uV5aEYtCUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQSqIJktCQVhwLgpiKWxwOhguzgVBKNChL0kEMkGhoZLolgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHArS2POkUtDqQQcSUdSKbkg5mTU7HM2NjQhHY7EQszBGMm5ghlwURclAWZ3OBCLQ5kwowTi0OxUmoOpuRiUU5IIxNPQAAAAAAAAAAAAAAHIqS5POmQWBxIZsC2KsGDsamCzKs1OBakM0BzOpYFYYIRgmFqaHcrDqcTc5HQ7ncjmhMJ5qV5EOB3NToSyESCMdy6AAAAAAAAAAAAAAABgyDBkAGDJgyAAAAAAAAYMgwZABgyAADAAABkAAAGAZAAAAAAAAAAAAAIxVl2eILw2IRcFadzUiFgU56s80XJzOZBORclYXRBOZ0NDBgtCpLA4lcTwSyIQiSdjBzJxWkEsiMTSrJJELA7Ghk5G5qdScVRkkGhxLYlAAAAAAAAAAAikAtigOZ2OBZHAwcjJocT0p5kviEYIhg7GC2KgyYOpgyXhTk4qzmbGTqcTuVRKJBxLIjnMknU4kY7kUsDQqzUgk8sjJPKEHYjEwtCSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYMgwZAAAMGQAADAMgAAAAAAAAAAAAAAAA0PJEshliegAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANCjJRHJpYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAZBgGQAAAYAMgGDJgGQYMgAGDIAMGQYMgwZAMGQYBkwZAAAMAyDAMgAAAAAAAAAAFaUZMOhWFqcixMEE5kYlmSIWhWE41JQIpKIRIIwPQlMcTYwczgWhwLU88SSeV5II4PQFAZK0tjmXx5MkEg0JJDJxFMmS8PKnYmHIil6SwAAAAAAAAAARCtOhzNDJwLUhGTocDJJOZzBg4lidCCTjQgGTYvzz5g6FeSDsDYtCpNSxNCvNjcvSiNTmSDQvjz52NDmDB3NSITC6KI1NDsYLMmgAAAAAAAAAAGDIAABgAyYBkwZMGTAMgAwZAMAyAAAAAAAAAAYMgAGDIBgGQAAAAAAAAAAACuKQmHcqC4KMtyceVL4qz0R5IsiYQiyKU6Eo4HY6HAvjzBYHY5A6EcsCCWpEOZocTgXxTG52OxwNzkbmpwJ5oSSIRySbnEyRzscyeblIXBaAAAAAAAAAAAhlWQCwOJOOBkmnA5kYuSuOJLIhZkMySytIpYGpalOcCYcSESzB2NS6IhBMmxzLEgEkpixAOxEOZINTYGxqdDqVZKNzuQDoTiGTSxAAAAAAAAAAAAAAAAAAAAABgyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQCGcCSVhelITQaGCyKg7nEkmDUsCIdCGSTsRARDieoKEknMuCqOZCLQiHY2LMoyQTioJRzOwORsYOp3IpGJB2OJBLU5HU4Ec7ko6lgAAAAAAAAAACAcSCbGpZkAkHAqCwLQhnUpScdziWRCMHE1JANTYFoVxg5lsefJ5sRzoRiSWJXHMsSEdiOcjqcjuczJJMnIjkkrCYbGxPI5xNDYmFkAAAAAAAAAAAAYMgAGDIAMGQAYMgGAZAAAAAAAAAAAAAAAAMGQAAAAAAAAAAAAAAClNyIRi6LEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAryvOJ3O5cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFOUhanEmFWSjJuamgIBPMEgpS8NSmJB6wyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADkVJgyanE3OgNjQyaGp0JxqczqYIpdGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYMgwZMAyYBkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1PHEgsyrLQqjoDkW5WHqgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAalIRjsbA6mxGNgbF4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACoPHlUTATzoUx6QricU57A8IWxHPpJkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqyAdjkZNCSYOBuaHc0NiAdz0hkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwDJgAAAAAAAAGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVR58inqCKVJKIhYHYhkw1KYmkYsjscyGRiYeqOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFOJHJ5HOhwOpoYOh0OJ0ORgwdyKdDJ1JoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/8QAPxAAAgMAAQQBAwICBwcCBQUAAgMBBAUGABETFBIWIVQVMQdgEBcjJEBBUSAiMDI0UHAlczZTVoCQNVVxg6D/2gAIAQEAARIA/wDxrz19Z4/+tnr60x/9X9fWmN/q/r60xv8AV/X1pj/6v6+s8f8A1f19aY3+r+qHJs6/bCtWl8mzvET/AMXlurp1uRZedQ01ZwWgZJtdyvZDjuz3uoN9C0pIXMzXumF9w8tq6sopsbCcvl+w/hNt5nBasOBaen8p1f0rjhzqqo++DpsWLu7pVOF39Ktvo1Gg4BB2bvasb9OmO5T3EWBOW9N5dvK45g3EdnvtPfDgPav6/Iwo418UVrOZFpJ8Yscl0dm8q1tAaM614WDr8pv1eaClPachDlVbXV3lYZPObNLVugihFQDDrhO67cPYM3w5CbkjWKzzC5X2NbLCQffm0CKCbtyzgcSfb0bUWrSFTMnm8i1/pLcjSmA2MwJLrG5TqHpZQRuVNebpxDq2Hq3LfJ9+k9sSmmYQmI5NtvxsCY0lVXX7D1OfxbduOu61PUu1rgUBA4ucO5Lqa2lZraPZUW0zaodQ/ln1Z+gfUKu/q+x5tffvq5TfoHyWtlJrwv4davKNPIuYfa0OnUbXJ1o8bYfd5hqU4cB0UoQxHWjc3rfNX5OXpqpJTVB3a3yvWDhuq1sqTqZtuKzTHdvjQv2UcuqaZ16ZthI8w2S4B7MGH6x7Pi6s79xHAKelHY79xSgT0/lGmHAH3TMA1KdqK1jrH17vItkrNBsJxqsyHf8A7IX7T1kVahhyG7ZzUX3VKtc1Bv5GcO9xewGaFOdGQmxT52rHp079WlSzAaHaO2bxnH1eNYyoqVwtQCLbj5ti1LfK8mlj1VVguICe3P8AjFNPG5t52Z6c0HQB9YPGsC1g4Fq6ivDzVMiHA5Oea2ofTVSMXyJI/wCLvca/WeSZlx4obSrAwXK5JxuLfFjysZNWr3YBwIUN9te1Wt1MVAPrMV86HCbteznGdhHhRWgHhHEdhFPA9b0HPygdBjqYW5scYuZlpWVVNhhKurXG5+o83RzlVkKQpoPijxC/XqcdA3I75lprndY/EzyeaWdVLQ9FqSgU4GNay7+zZaaji9al6o/q9fZx7kXtJ0X7Rk4xycK+nks6180HJUF1zji+LZybWw15K7XbhPXDuFjcs7p3JDvfaDaztLA2dvFzs3Uso7A6CuHZ4TdTav8AoXTeq/SOu6U8W23/AKWi7GVWRQaDfK/I3s3f0tHCim8NGBk4DgxxTwKjpRZTQa07UbeB34zay8BNWkViIGeg4UeZp5d3LvubNM+xhGHZ+vP1z5q9X0vX7WsHbVyjS06Ccl6rsL+0YejZ3srTuxTCKtdqXq4rxV3Ht3Rsw4DpvEQQN/H3lcufs5HoHDawpkHcPuzxK/Smyk9K/Ziy5j8fkVzMuUH1sJAWKxq+auEXxfBeyjwxneKQdw69bp4NC1bhVTNV3bN3g1/walOldhtW/wCE+4YFjN5IN/GlKadgYG7V/wCyF/yz/wDxPWPlabTdcxrNyvJgoHdWeEa9u+q++zfbaV2kG3OO8lv1Tq3L99yGx2MLWZbyKoHOnciFq9QARxfVO2i8i7bN1ZcJS0OF7IJtJGzfkLnfzj9F7PwphNm/2oT3q9YtC1R5go77Xttv+5H/AMCdxkc2jC8EeKafseTV5vaoU9JwUlHNLRinAu5ZB3scaShbW0ktb83c03Q4urfDLpzTMesR+rYqyevVr1z7x8IPlm6VnYmllVHVstpgc2eX333c6vj0qx+5Si33uci1c3Jq2L9Sp5n3grdimBiZmYiI+8zxbmH69tXKR1oSAd2VTHmhlSdAVIdozdZVq1b/ACLVTroxadSkeiVaHuKjsunDsX9ekdA6vz8wcO5Ozfp2is1fVs1yjurH57OlxzUuzWELdAJPw73KnZnGqF5FSH3LsBIIx769TJrX1f8AI9cH1f5HyanuVsycuhJ25PwdX+R7aNqvk1qVGbR04sN6fv6VBOX+o1agHdueA5Dk1m0jTv06YTl00nKnxzyT4X+soqhNkHCpyNbmB5l3aUdSDDPhEK6ytbcO+NbVy1CloSS7XIt/SobtDKy6SLDrgGfeOXWQy9mblCEaOUEGaKvNO/LUY9tAqB6FmLcDfnSrajrShSFC0xPXEeXzuquTaq+qaOzQChyzbv1laVbGS/NYf7cu224GCV9CRecMEYDN5DqfUCMbbzk122FE1J5nJ+SaqCfTzsyEw0gjqpy6C5rZwLKRV27Qlv1tonSqlVoIOxZvnTEavJrsW7lDWoBUvIqlaCA5hr25yU0M6ob79UrExPM3hh67n0ITo5UjDq58yXVv6CrqewVUoIIoX7Y4RX9pAVDEJaasTmjdLA1brqcJtUVeaEhzuX8N/WE1gmyDgS5H+PL9p64J/wDpb/8A3R619hND+wX2bdZ2hKDybllQ/q2pLFfaWo5DaqZ2kh/HDzouSBKlWHlOt46HUstFVjghk3NpWio6tO1sX3faWW2YudUzKAqomw0nMs+d3/49of8Asz/wdtF/O5sneqZzr6JpzWILGFs2sl9maBg+7tBcivZ4zfqc4Q+mEnldntjo8PT/AKoFZEVJm8P7prxI11iX2KBiJihwwb+tuN2Atgl1yZWPKcGfqei0MAtPORR8MJ0s627i9NWZxx1GaumDvUv6u7fwNEEYFqpZlUCmKvGd/D1MS5FgL6qk+sYU+JX6yrmvTTKNpV9jUdbsWL51X6vEzu1TR+0Yu+fE25IV3pC9e7CGXi7uVy0LNhoXkXK0oc2OI7H0GHhrknUTLglU4W/e1suUdqA5dABFvBKF/HoWcq+H2rPnwO3KFx/NsC6lMnXrQ7zHyzIdZ5iF12CevTilCu1/BPVycemGA6hUTf7vrBl62XT18BKDtZrqzCoN2OJax8ay5oVpizKVIvV72ZqRtcktIy03AshWhSsHLsxympaycW5i0gEouBydGmnl+Tq0ct19VVLROH4+zo0uSaVqlKLWggUV6iOL2r2rfC4k0Kbn1hS+jkcjDhenQOp2v6N+YMqGDvY3JaFrzKvKmtNMpu5N+z9qfGHZu35f+s5/QuafESp1Ey98mHWPxXNyrvuph7bUh8IZk4h0Yn3uFPu2oebIefG7l/b5EZpmtLyQ6k+pj71XHyjPKa+zW1TtNUFLX19e5sXc46QBnMq10UKG3j2sC5GM+1NSgaWrtYezfxuS37NKVXNSFCmpZ4g7W3777InXKKyIpWdT6i1+GRmuoSm/ZcNewVzjnIaNxxd404u5rKZzrcT1p43lnSQUWJSlN6v/AI8v2nrjFuKPHrT5AmnDQgQhd5D0V1WUhcvvJtpn6JUKflblt0v9drYufX4GizXdFWZBEfwe0n930Duq8P7gisYfUuoTZiIBSAiat4MFNqld+yKiTek3tB/Ns1ofcDr9x/2OT754cUwTSm4247wgEcxMEacXcp1S5QRDyRkb+xfNBt48deo4YPz4vO0adDWtTUNP6cMlI0udDay6FyaBB7t/0ur/ACKKO43MmtJyFA7nzq86OatW5fxX1KFooELXLeTK4zTS8kFZJx/CAqWFW6ibKCgluATCR5zalFu5GA86FVpg1+pv1KHHJ2g7uTICaoPl6Y4V9RhWk/8AVE8trFVxbKEy0NV8Jjrf26+HSiw8DaZnClJocpcesnN2cl2W+z38E4GzGxF7smVepbOv1y3lKeMhWM0G83n+1rkKlb+XmAmWRorIxa7kkK1tejNb75tWLEknm9n1at29guRQtEAhZ5Vyytxy3TS5BtmxPW/vqx4rgCDuWrUzCK+LvvuXzztLMdnXBH5wE8urRzWOPSn/APv3dGMnFt6MhLIrrk5COSw+/ToUak2HuUDrHVjk+jO3ezc3CO7NOQgzu8ps1jq0U47n6z1eU6nHd4dkHwVVtOxWP4OTxjllbfu3ayUmqa89wnF5kjT2tTOKsSZoQcyfEuWK5FSuWZrTUCqfWFy/9eoX3UKBzZrf8lfL5dp6Oq2gGAYnWOBszybl1bj2jTqOSbZsT3Mt7ldfD2aFK0qfFc/d9Lai1ya/jeGYmooGSyzyWvU271G3EKVTqjZN+VsMt4p6l2odFMRLAHi3Kk8izrVkEHXOvPY1Bz63+lRrPwHhnf5v5TyxHHadR8pO17JfYeT8pr4GRW0vDNlL2CPR8krfq+ZTUEGm+g3i/wCtLDVHep4Ft+UH729vkNfL49+rQEvA4Hwha5elPCw5GFeTE4HumeV1zjFOqqWq1jkA6PmylY9qydI4uV7UVJqcp5OPHMyvZfWlz3T2hO7ySMzJoXqlabkXWgpQ5XJX2duMjTyW51s1S0P8WX7T0FqCyozOxwJkNg2YSKa9oLNaoqrDctbCj+KfIo9RVTK2A/zCwjjgwe/TD/VsdJcxDhckyWwJghPijaG9cqHrXU6Nx1LsYcuzqWFTpvoU47wzs/rjT3P18n2Uyk1KJcB/Tz7OtaR4gVYsfa7Em3T4pFDjm21L7mlft1ZCT4lWp07lKf0vfVbhcCRUcHUUqkEUHxF9hIt9Tk6KcFD4oPP0t4rRJmbG/v39WpRthVDIZVCe2jp8Ko8XVi31P7rFztjN2tbl0zSSkK9Gr4hPgAXqmDObppIG0XEkCzLGjQwNnIjD0nWbdl/im/j7MVOOYVNIHNIYsOdby9tGByPIfSlxuau1XnSwNGpyfHmkgpzG3FWmjzWlbcGbfpImyedbF0odadyrkePNOhdRUoOmw5+BpuwnbCbWNrO819rgPWzNvc5XddTQgKyqvqDNPDu61/jtLazbUqpg+vZOrgTk6/JkUKTxqNzYBPQcYs1MDAuhGi94PTL6eni7u/v7D0JrpQSvRDpMa6E4G8/OstbQWypcRi29Q+VOvK/WCyjQ0/hOHymcT9U8CPL7v6lCuVw2/wAKvghLTa+t/uKxMu9xK7UahNm3QvqWNwNPOVPMdl+nmbbVNlcoMDPE36+2GdfbnWKA1urmvtXeL6tlGO5Mt7JpBm429hbuI9yEOQAemfTsfYQelZq0Hy5+jZr9NoalHK5FSoUn97twK6Jx8vbxOVUG20IOs6tFMp4zTso5hyR70GCntXKj0sTd39fbspTXShw+mEKoXNq/gfq9B/aKTkW+uE5utR5VqxqwZwCVqCxv8Wbv81uycvSEUglDta5ua/D/ANLPOcrTe4adicrN2snlUzbroNF6nKiKOLXEcAq2wHSO8BgRUNbN29rllptNCAq16vrBLaGu/iOPQt0nm/O1QUcTxu+jmaqUAU5HhsRWYNjVRxhWBFLaqa1PvC50qu/qTxyqpPc6qAtvbcy9tHF9vGbSJp+2uwibeDfo80y/SQRZR2va7X8EW/xPpX4qHKoRLGHtUNvX5gZ0kIBFOtKhKcvVfxLMxrdS186OuKjPHxnY/Pp7e5aqNpf9T/ii/ael1zebjAGuldOZhXLczOv4uaKdqAsBWDsGbxmbbjArgR8LIoieO4aUaNJ8mRnMVXB07iiSvklLiWMOrp6yeK2r9oTQc+FVULbj5shNEKdOnfsuMK0GkuH9p1M3tc9ye7ZJn+zsciyMP4Rp3hRJ/cQTyTHdkt1FX1FTV2g2p5vxt7gSnWUZnMCMV9SlY0LNBNgSs1oiXBQ2c7Sh80rQtGuUg0lc142676YayJb1a1KVS7WpWbArsW5mEB1X2s2xqOzUWwO4n7sVGnTnVLMh8TcFcNlSNSk/SfnKeJ2kDBMXr72Xh/D9TthX8v8AydZW/k7XyjMvqsSP3KLu1nZ1yvUuWhU+zMQkNTUpZNWbOhZFCv2743JMfbIxzLwOIf3DU2s3I8P6jbCv5p7L61tihjIF2laCus5+IzQ5bg6doatLSS1x/sHTdjPVrLy22hG40fkCmbWcq+6kdoIspVLmL+vOLf8A7ynrR28zLpDbv3FpSfaQnG5Dk7cF+mXQfI/uLeb8aS41O1lAYTIlFnlmFVq1rL9IATZiZSeXybF1jJWdfU8wGSkMzTp61SLWc8Xp7yMEjUpP0n5ynidpEQTF5uvQ0yeNCyL5QfwZKNrNsarsxFsDuJ7/ADTqcnxce162lfBDvjBfDJ3cvZg5zLgWYD7HDtrNRqpzHWwC26IkE39jPzrNdFy0KWWZ+Khfq0q2iig+wIWrETKl63KMTFbCdG+CW/v8KGhU0qg2aLxsKL9jo7ObdvvoVbYNs1u8NXGxnFrzlRaD3YD5ymd3MF9xPtjLaIfOwM72WONGrNwIo/5OsaVOtmzfe+F1YCClga9BuTOoFoJpQEnLh51xiZ7Rrp6GYIYkZiRmPtM3ao3xoy8ItGEsFX+ML9p6mvAPrvlczDohLJwcvC1sND9KnWc+on13Ty/Crcaphu8b8JIk4hyczfy7U1bnizqjgNcsQkEWKBAGdVNshEQ/HpZL0KrsBla8S+zU8lNP61GK2qR04cgKyuMRM7Oa06wVSfBt8X+zt5d2eZzq4rqFq3FeFtpadtB4HKqp5EZmnAKKyOAnkfah58nGCn8A7t3Z155fykMmJkOyptlyOxTD+ExngdgqFAh1t5ePH8OXCKk+uFP5qMGude4Cb/u2YPrTurzsyzdd/wAiFkwusTXoUNHI2ouQei+2z3x1P1f+tOz+hTVh/oB364d7/wDWFtfq8om56qu/XLxE+acWAw7jL29a6VU/4p4c0AADclkWY5Zo5uts7lmxcgH0oBOd1qwfJ8fA1Kdumm9E+ZNelcNHOaYb+ImppPVIotc2vZerynRq6VyEhQpSFbre1T1+BcbtQEG8boKIbXvWea4CdvLrZPZhGkuuXZjtT+JQBTbKracuH1ioawbPL9O52kDnEMXBw1XIp4zR9PKx3Vf8jtgq1/FxSL8QaU0O9UBspD+I1ZVjA9O22Gim1xUNsj2Jys7Nsq/UW9y5jF0N/ivanXO5EH3q0ItP/iIJa9JOXbTQOVJ/hL/8EB/77OuRzs/WXIwxf80LmyWHfxqXC4vZXYaCEyfWJr0KN/K3Pbg9F9xs3g35uf1qhOdRTdd6H2Twnu3me5ZuoCjeAFgdXW2KF7R1d2bfbRTdVNIObKRyW/xYIOQVdhnVHUff53x6tf8A+voQ1FrrhEC2eR3XVYt6o2z7hgbFXNpcieGROY2n2N6eNaWbl6+HfVdhty2bA0Y3aNuz/EjUt53f3aFVVlI8Fv19nmu/dT90vQrpGM6OYRw8jicpL50YD+Jt+pE5WPbdCq9l8Mslg6NWeIcqx6boaiqLTrEZ6n9WyAfi1QziAIm9mJWjKqpUcmsEhAHk5qcr+LRIQbTgs6T7/wCNdUaFvUywIxd8xtV+sF8IM5Pw/pl9Y1zdmoxSyawXcCTfCRh3Ts3hEfviycEcB3+muB/juU3/AE743HZTfoa+mdZLoF66ubqaWs25Su9vdOVIZx0ItchbYGe6Ki4SB/7O7xSnsXRuxYtUboD8fOjhGYrLuUzbacd+I9iwjgGchoGGjrf7kxMRSwqdPbvawS2bN2BhvVDi2bRm+CoOat/vLao/w5yu4hNzRbSGe8Ut7iefuHUJ5WETUiYV0PBqEULdKb2k1NuAhnWlx3O0cactyeyJGAjqnx6pV2I04Nx2IrDW71cSrW3bWuEn7FoBBkci4xS5DNY7bbKjrTMhONxHMxnts15sHaMJGX4vHs7HzfSQuWBJSRT9DZP6KGXB2oBTZck8jh9XO0R0XXbujcAZAG4+BRyYs+EScdl0uaf0PleKVQdmAm7F2B08GrqaNG++TB1E5JU9TiVZ5GO33Z7MI9fqOJZg7FzTDzA+4olN6D+HOWAfBV/WAf8ATZ4rQ16tYLEuB1WIhFjL4ZnUL6L8vt2rqJmfOf8AD3L87nBd01S45MoVxKgJ5bJdaaeXJSmX4VV++GzJNiwCZTEYGHVwMyKFKWyqDk+9XEq1N65rhLZs3AEWdHwrLmvfrKbaQi+UE5N/jubfxZymogK8gIR0jjdNO0jVg3FZTWitE/TlT9T0b4MeDtBUKd1Q49nUMWMpSYKvAEMzV4lQrxlfBtmf0uTmv1Y41nP5KjemDi4mO3Wtwqhf0jvpt3KFlv2cUcDx4oWaQTZBVqQl0bHHs7Xy5z3q+CvtMTUwqtXcbrCbSstRCTnJ41QyNW5fpwYnb+5hGDUjks7vdntSnw9foFIuRTtHBnZ8PhGLvFc69ffdPyqOxVmq2D4zQbxeMApb6YjA9VURWrAgTI4UMCM/odT6l/XP9/2vB4P8dynOcUr1KHeLVboprGhVmgIjWITU9eTpPx0BGcCX0Hsk1VdxoOpUGBMGB3a5DNh6KiDsWDFSwjuR39O7p6RsbL20yRMPnNpIpBKMmq6L9uPDAYuavKoDWHtJf8zC/wBrnOxppuqpYr4S6sg71jot9579V1eTdTbkFcivS5vLw0YdQAG0qc2+0c1057BHHS+ZVIuLi/zualKvdDNg6zawvk627oP50NAFD6B0YsdHasBqpqhSMq5rkzs7diUY180sgHrrGYT9Z2szAznWqkWvnSB7X2+RW62+9qQbYrRlrshVVziCxNK7NKJbQMAMMTfsX9a1mX86aT0qFvbb2mhybYRb3b9JVf4TXBHKdKhxjMsXairT3IljXBzRPaXOqypM5vvqnV5q/KSp1rJERlINZ1y+66pw+9dpnKmgmDA7V+/xa7mWbuw+9QuQcODM5wFyTA6YAU1StJirzXStTVBWAUncTLq0WecyrJq305ndLkS8pv8ALvT3Kz5M5rW8sW16c3Ar0QfpmmnPaPn1U1M+6UjRvVrBxHeR2OU3c7letQB3466xqvnm6uXiPM7jbSWFNpPMXWYrppZRPtva4ITxrk1qatKLom1t/Qejq7zcagP70fuGgVIZzdJmlhndJHrHIl9uJ8i0uQxQzZvmglwTrL7/ADV9O3fiMc3VaDwQ52Ftu0rl6jeoTRt0pD5hfv6yOa5lIzQFC15e0Tye6nZGXaUxYnV9Qszm1m2ipQClaOoVm+pBNsb9riOy+lp3T00HUh6JnnsDSszNEDtIcoJUfNriIszZwiAaTgVbKzzGam6FB+dAKO0NeCyeT2f1+7kfe5bnRbAhzK3pIiirMi7EtbPkN++4k5gHv2qlNoOk7rd/av4ONZR78E5TSsnc5FaNNP1Ni6aJoHYhuxo7IYydSdgEd6ipqppE46SDshAuJYywf8drYb6z50cX7NH7knA9a3abNK4efYd9ir6951C76+Fbm4hNgDaOTQo7dKdXY2G2pT9zHsm/eKtx6l8Ij7Mt4+LWyk/2UfN0xEG3/bdxDKubFvR00xdN/wAYEVcGqBWlE3XyHqMpj1V4YIRah+i1xWaHpFIcXQL1Niw3uvNnP6PgKITKQ03gk6gVW9J47CNyrpptlEpqDUYB1HFqKtxbdCwCQmtvcWjWvzdr320nNrzVdNrgtd0kKLzkIOmNMw1OE1tL97jgOKy0D19HKKnpIffc09Elk00Y6k779aGlLG1wRIIw0pvatqGnM6UBBx9EgAU4RouCatU6nfY4pLZ49lIS5qKc/wBta1+Gq07t93vuSF8ABwauSvTwXZTWEANVC5MOGpacHraVvT+CiUoM7jB00sQ7Ta9M15rrilxevSdmmD2lNCqVYOg4AgAAEaTwGKc1D6dwuhZAAtGbYDOihHVWnCs9NWyft+MIiTVWQmZJSVhMx2mZ41TPS1Lb5Jw6SwBqZ4k2AzZTr2QtZ3yFT08MiqmpNLUsJtVTYQ2I4WCsyrWq6TwfVtFaVYDiJpqOUjYsQ51o7LDwsBOLksoKaZw0yMyDhlRWJRoBZcLaBy2vatcQrWg0hK02IvvXYPqnlKqbV/TEyk7sLgguZKre1Q0iMgOlDIET4vLtYbVvUsPqhZiyFbfxQ26qVTZdWNDhcBnwmtYC0V+9ZuW7ED2tN4nNqnKL+k15S9bvnc4nWtq1QOy6P1Ny2n19EKi5LY0XQn3YvQqeIVItzcU9qrU35uw/YzLF/wAJ09OxQcme8FHEjRSQjO17VUlycsMuMmmlTq5OtaoRUXKo6jhCq0BGVpPozFf1jKeEAFutZpatyoVWvFdPVcCUhYEwmEIxBH/j9rjyNKfMqZr2o/ZlvSvUPhS0qUm4bKnearnXuROh9mIp0PnJCFOlXo1or1VQpcf5dZD7CrlW55XD5NYkk7J3dJWaqpUfXEoVbtG63zLUmq23UCohSVVpNN1z1vqCk64CbfiyOZGauH6jQKQMax9N2t6NanmncqqMbNbuVPc2VZNX+91jfa1mVPnnb999NrnnTmUQ9RxqbFvDUFyWQ2qkxqu6rcv15QdqyFcFUJWGjA8v22ocfgrAdBcvuBi8qu39aK7YQpMeWzJXeS3qvI5VPgHO8gLA3b+qdmnqG+t29C5ZSjX29ZuBrJuuRBjQRbWbuZ6c0jcn1VPCHmdU+Uan/qNjvRRUqoQcRW5Lt2jVTCao2TvlUk55drtpjKZoJeqk+04tPlt+r6xo8BxCaxPXQ5Fv2YrfeiU3QtQgR5dduVJs1fApRmiurrG5Fsa9nMSEVA8ymsf1xi29V3dDU0ZfFe0Iw3O29Gpesx5kuqv2m1JTmcj0q2BWGu6t/dM2LbSbyzSO93RFVNWLqKpJfy/Wre/Lxqh2Q5tUJ5RrJkqrSoNe2K8qeG3rW9yhUm0gPFfbWfOtv3I0jRl2s4FJqxZJ0covzfk+1P1P1H0IrJ5ZyNtUHd82PLSdcjrmlxreB+yEyBulEz0ixdynVKWYpFR15xwc1eYa01odbmgEPozZV1Z3r1vLibfaHI1hrSWDYtfQFCwD0xY9UJhvJb9+hnKLNAGOY6Anqxyi/fynBVs0wIaDXvbgbdtHIvTlvYLIUu755nqBFw4mqYRSZaQWpyfYzTATOm41wqXgnkmvGnPliqdOdN1GARy/UGPPafnQp2cVsOkcm23mFQZqBZnRipJzy/TrU0OtBWLzjYQroOV6gX/AZ1HBI2Bg9W9tN4AGu28FZpQtvbklyA0cZCmjJlfADGrv7lg09zpgm220hfWVu6tTCrJTZqzKs332na1Z0ph1qYUo5KATS2r4NqUqTkRFqIKGxzDdOkL1eh9sybzep5NrBL6rjp+150gqb2/b0sltw+ynHkyffa17CbVChmvqg6yZgbr3KtRQPck87tRqA93Rcl3jvkNb0gTOlFAA4vffq4ardz4Q+TMC/wAM1oqSbD+wgMkXX9aPF/yH9f1o8X/If1/Wjxf8h/Vr+IfDrYdrXzd1/Wjxb/57+v60eL/kP6/rR4v+Q/pXN+CIsw9NaAd/kb+acDsgIOrQYBPcY1+R8M1b8PfdswHYO4H/ABH4i6QNxtOQnuHWrz/jGjSOp79tMH+5t3uMv+7uRaZ/cOrOnxO1HZ2/pGMul8wG9xlUKgORacQkey+j5Nx5qSS3k+qapnvITyPjkg4J5LpyLvu2I5Lgd29uS6nd0RDOo3+Nf5cj0/uqFTEbXFvcG39QaUWA7QLA2OJgbTDdvwTe8HJ8i44cEJ8k1JgwgD6fscTtOl1nevub/mc7/GpS1R8j05BwQLIRvcYrQMV9/SVAF3jq7q8WupFT9/RNQd+wHr8VKVGW/omaggAkOS8eCB8XJtQJV3lfQavFopTSnf0zQTJaQI5HxytIyjkummQ7xEzyLjpw2D5JqdmlBM6Dc4sN33Q39KLP+bW7HE3QmHb184R9k9WdXjFu4FyzyPUNwyEwZ6fEjiz33tKJtDIN6Tr8URTbUVv6UJd28oDt8WFKUhv6UKQfzUB7HE2wmD3tE4R909fr3F/f976i0/b/APnRv8a/aORaXaAkI6Tz3iU5gUrr3XAD/NHNeB1p+aK0KP8A1nnPBzT4pT3DtIdlc54OoIBSewQUF2+v+GzT9T9637eHe5pxTbpxWO/bQAz0evxJyUqPdvmCPsqPqDjX/wBR6fQa/Ew+fbbvx5YmD6ft8Wsvh9nf0nOCIGDjknHv8uSan2bLek3+Jg5rv1/R7tAgPpG5xer2itv6SoA4KOnbfG2+GD5Lqdku84DGzxWLJ2Y39GHMmZM55NgFWiuXJ9WU/wCSp5Hxwn+c+S6ku7xPzHkfHQ7duS6kfCSKOnbHFHgmHb+icI+yuvqHj025tfVGx5ijq3u8ct1Rru5LpmATBdquxxarVisnf04VASrs/b4vZCQs7+m0J7T1G7xiAkI39GAkJDs/b4vaSarPIdNoGfkOHbXFXmg3b+kco+yevqPjv/1Lqfdnl743P+M5lP1pv23/AHkoP+tHi/5D+v60eL/kP6/rR4v+Q/r+tHi/5D+v60eL/kP6/rR4v+Q/rB3qG/SK1mmZKEpCZ/4zghiTCZ7QYyPf6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35COvoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CevoCt+Qnr6ArfkJ6+gK35CesHHDEpnWWcHEnJd/8AjzyvtGlE0uzqToCAmygHQk3rhpfsH9JtBfbyGI/MoEf6DYCwIzKBEYmZnO3W6VW42pQOTr2ZQIfUJDk37h0myVE2AxaLQMzV3Tj4ASoaXVjYvVsoLpZ8SMV5efRbHzepFVEtcaxYQ37ZVgTChg2uaKwFXInsaVX0Ii+FnwzWnkT4RJ+gPf8AUfQ7Xrc13VUgImdhvxiD3DRpJq3ac1weLTA87d9toA2oavNV9pPVDbZfzr1lFI/NUexEIydK5cuWatygFUkQH3/xunpnRv0K0IgxtsIJZR0vLVZZtguoj5zCj96nB/CbSfn9uwldqAZgdlIEAyRiNyqQmQ2FFAREnKrtRwmarSTAP+cv1Gj2Gfcr9igiGfbrfMQ9hXzIfmI+9U7LKLSezO3jld2q04WuykznvEDcbYUIerX9gzPt1U233clF2rQ7w2T+cK1PNx39VWgvvXl8Jpbxumh7dPwBohBIYNyqQGYWVEAdvmU36fhh020Qo+8CZ3qgHIHaSBxMRI7Ov6VJdmnKXx7S0N6G1XJRNF65WMzBH+o0oGJm4iImJmJuWLKvH6dWLMnM95RyGbVOgyrU+di8JkCT5IqcWppJpvaqzID1tad7O+ZozhsIWqWsbSaTqSXNXKTMBIg/493jl+3Tf2+CrPvm0OrmDfcdxIQHaxfC0Nr+m+pjPX8ddLvi8Sn+i7UVdrkh0lAFMTPVTM16AbTq0d7NmyR14mldPit6gFBSGtUxSwMXzkUMp4QtzoBLI5HTt360001FNSS5kDv4F1s6XYE2jvVVph1ICbrCBHLYzK4qkyoa0aidoUhFyWylqZyNOash4A7/AKx7vamcXd63Y7911IisHSqenZv3JvU0iFkDTFjGy7te1UbdgImlS9UOuNU7tKL8XUCHnussBOJUfVqMO529qw4nN/x3IsktW7miaAdVUw5fE09uaiaTlV2gryKl/GsK1UsJm/UR2Vnor97+Jt23g8q1SShNxEhp5T6eZbbZrKBXoV0xGTDSpX1ZtXtc9+G2YqYGknTrtbUQaUvuH0FByaWNQFqF66FeBobONfsCdSlWqDUAa8I6pU7mZT07UZiWXDttfWDbDQbnQqgqDaZDDYbWuOx002Y9WUlBAysFK2vic0DOLNuKkqk34OlfxKOc/tVinX7QU4BHQmGUp9mVpUXSsDR7oO6IWO9VtdoaPHLbY0VVqajB1WuhE3cG+89SQrIGLFytYWFjBvviw+AUkmX1WorBgSO5VseiHr9rBPjfTfZRXWzUiYkcQ+CpX/Ll3U1UrbUBizquxLqOI08quAPctoGwtqnZ00Va4iAIN4lbj/FSAycF2iSGJiC/oEBGSkRGJKe8/wBIAC4n4DAwRSU/9mMAaBAYwQlHaRQlaF/BKgUMfsPXiX5vL8B8nb4/P/sGJoWL83vOpQRXtGgep2M2IeU3kdkDBtn9ay+5R79fuEiJQWvnC00ldRDAYKiHp1hNeVw9oB5DhYfyrTztmnNiEHUCLF47Bk7j2vYC3Ln1TbYzZp9W+OX3hodpqxNqtXSPR03a+tyKgg0RBvr+WergOKUeGEl8WxJ/0XKibteU2IKVl27wlQJSCgiYABgR/lWIj+V3chuhk6dz1q8nStygBdfqoaCLFpK3MmBEON336WaT7Xwhovaqf6XPUiQhzBXLDgA/pDmGlNyDhNJtI9OaIdTyrDFLmzpp7I7QwlcqwXNFSdSuRmyFjGTyeH8r06Fu3XFIOFFQJ5NjATwK+n5oCTbFvl+a4BTkaVJtsziIjM5jk36T7kuishTpUJ6vLUI2sqK9+sFB6je9j+SY9Ynw6+A+uIE3pu9lIthUbfSDz7dl53JMfUfCKF9T2zElAaWxcVyqhkUgQUOUTnlyLlDaVXVOk+p2oJEeszkmcylUizpIOy0hQfU8twBADPTTAtkhAtvk6aNPPOjKm/qLIBDa3ISoVjnk5VaDoaULhvJ5Ryy5XsWUhlVkriSVvZLr40VX0zZL9lYPLFTQs3dq9WVXO0wafVnlAo5rFB1yumgFYSKf++2eN23Zeon4U/Zt3Zco9bJ1b1mSFtaEhZQ9Qcbz35ucxNqQkysNbE/0XAcXg8IoKIaJH/Rq+5Oa8c2FzaIJhcxwqA4bNFAVk6ppgGWbPD7p+rCqVNQQa5eFTi+37SG2gpT2vsvv6TwvXbmKp2rNZEolzoa7hV/9FGnWrUkv8cIlupxy7Y2gdSCpWrVKZqq9O41ySMapm0n0lKRTlXX0XszSsV+1MYeCK3Vvimu+/c/6Oalq6pp9W+M8rt3AtPfTNq5aQdca41coaqH3BRCalCKqIPJ5KG/paVeKQna+ClGfFdowAnRSNp6U3rIFxPaVfDRS2m23LHWGxx3il+np5x34rFVpVDCI38jW0fZpICgWa1EAsU8IsqtOF0J0kEhSAPO4jsU9YNHvVh5sd5uo4ntxmVK8HTA6AM8PT+H8iPMDNh9KKYLUqADims+3B3AqdnaQ3LMx/wDdZsWWUse5bSIkxCTYMZ2w64UN+daKSUQVp/1BlfaTuQPcxXHS9eg2pNkLEEsTlc9afJKQYrLNK6EmVY3omdurVyq1nReKSYkDLqP5U16rL2RbppIQN6TXEt4q40vqpepNa5UhVod2lNEMxvhRFib9eCNOPoJhhBdEDsXZs2IpcUu1EPAbqTl9I6hTo4GleyZoRdAEzSFHZUFChE+3ygYif/F20+5Uy3Wc9IOcqPn4y5OsQZf+zs6JUpJnvBU3LSL71qqRXU1U3TcFNxVYA3CBSEI5PJ3sVPhD4X0/NzK+2pOX7uq1SQKTIJtbZI17CZfXitNdDUnyO/azsdlmglbniQRAN5EZWccKiQld6e7iTuZr4TKrUHD4MldHyPNbCIpX0HLmLiJv7gRrUKlN4HLbUoeN9x16FhyoiTUsjGMvYs34qtCa0V4RDLjNXfX+ivtZVkDcmVl2jWoRd9KbY+eYmYihp09GSim+GyuBkus2++zradVwhAVGAASetcizr1SBPmqeMq/VS/ZPft0HArxoQtkT/Ixd/jPw7d+326TgWx4irJJyYepwtg9rj13UnT7PQEXKi0D1Ui2Mt9slTEn/AGcTxE4y79ULnwc61DqzNjHuXKvp1LQ16k1DRK38a0DD4Q+r96lav1uUXX8310GAFLVnMzx2VXa7qbo+A3itsGhx27SOj2soIKjbJdRgWxwMuhDUeWk9TTNGBcRNNQvTKat87XV9J2M+whUxBtWQRMcXdFOaiWpSmzSmtcixxxzck6wRWC0alql04eo3SrW7N1LpQ15QGBkvyvNBtHwnAeNAYIvu6ZaQqei24GrC3jeffp6IMgASErcFeg5XIrl8zCUvStcD/wCIN667OyW2qyZcYdu4s2r36Q67VuUrSCtKCu6nesxuNzLkgc+GHpbZ5Fo1z2FnCO6Dn1GO2qCLM1mv/wB8CEGGnkmW4yEXHEQmXyw+R5q0Me5jFCv4SfVbXRa1oqqcUT4iKU6W06lvOpfNM/OqBVFlpBnIUOq/u/sENYO9nzbZV+Zw1TJUyJ5HmDJQRtCQaCZ6HeoFV84tKYmxNaADkOeWdF4ZdKJgpiS5JlDJ93n2CFlJs26SytwUt70zAGi/bo13+Fxtg5EyjrN1KmoPzpEZhICcHxy9Yv0GutyEmNlqo/kjRTZdW+FOz67oISg28ZM1XJCwC23LSrBdVM81artK26GvJcJHq5xj36V5DbfwKxb9kDjjyQ07dyVVnjZaLTDLynXlOoW6cAm1TNUudxyTyyrLmmlpSvu0swz5GrV80QK6xIlejgRo37bnOjxPrCgYuYGhfQQXNb59wV0nKsq/ViC7AMvl8wZPF7PnN0X+8k2u0+tHIsUKBik4c5+pNsDp5Nq/jpT3nMQsXKmvHFbXoPqzeD+2rVkdwqDf5RNyBcoK4+OwD+O3LFwbJ6UmYG6R6x6ZZ2PVpEyGFXUKoPCzmZlNiGuh0m9je/8A4hu3EUap2bbIBIfuVffb7+uDn+NKLKFJn+WNvOnUzSrA3xH8xYJ2eOW3u0WzdT3uuru/8a2ZIKrTDtBCEkM4O3Z161JwNrdoSLL0s5FlKhsnZkIT8JPp29nIqlYc4wUEzBk3ezk2W122OzEmAMj9czovDT88y8mEoYLczxJoE6YNSieURyPL8Rsiz3AABk9TqVRe5MmXkTIicfUWX4UNizMjZgpV0loOSDVz3Exghn+T7KyZWasJgTMZGJpcct0EU/SvApqqnqvI+K3T8xzfAzeuuDJ38O5sA9UX5UhyPHCrfHrlk9GYtpGbk1+koeHKZcFCGr984WMcbvkZtfpQ9x021jM8r0I73yl6DoKz5BOPejIrJjQMLPlFts8zjtmk7PmbYGNNjy/+7c2AsCMygRGJKZyd7zqvnpiFSKpQXRbuYASZ2e0DJxMP1aKPH5rEDDZCALJ2f7jZPScPzVbeqJPfylBBlbH4lX9kZndy4U45thIIAWM6sXGWodWyXq9tLFw3pulUl801WYG0QHIRjb6TwKFzTeAOsqCT61Nl6jD03qLtppqtENeiZAEO+7GykOo38shScWYmH/OFdUtjPvNBdS0DTavyhF/avVD1w8SWOrCuai0b/wA5VaKVBRihFt51L1e9BlWZ8xCYiZu7Nym/XV4UsZXBZ012d663OO1SlIQOXNw5zNOvdEVi0SfCgYY/yNs1bF7MZWqmsCb2Eps8cKNF7aEVAQ+p4GJbxe6eYNaLavmBmST08LSvdxnSglR4SXCsLUCJAbwADbT3tEOL3xpgj2q/2yzodHxm21NhTbSRhmaunE5Gbcqal27aak4tAqPhGJpzpouuvi80y/tEcV1IxlZxaSpUlKgWN3jNy4dnu+tIvvqt9owr4JqpC2kho3IbW6ocdv020C9iufptsH1kYFuhazGOegwp1jQUXcb2uQ09SHSMIAhNaOMnUwNSgmwJHck4A1puBFERamFqH42Ru43s79HSh3xiuJCxdTjXq4urRGz3O7LIA8vJsovxcuuUZhVGsEf+INt7quLds1pgXJQbAmhvnFls3LSnU1U1ua0+RZ6u0GbYObAV5De5CIYto843qtpXB9O1qtZ6EOlgm5gpEq+5SsyyUS4wCJn5zyLPgJIpbHxsxUmA3ZHfs/M3zT9NRgl+tVrPQt3lCXmKhmhv0L7ULrkfex5PH1r320eQ5Yeb4VHA+XC3cpoqrsPGwtZ/D70tckX9ZV98ktN0EV+i5NmxPaDac/BhzGntVxzims85c2oVhU0+Q1VZVQ77u75qLsPle1TbfOkEOJoMhZ9XNmpUtsqu80tWj2Ch+/m1+0m/vEoixJK36DbMJgziZsTWidPb73aCqDj7fqMVnyjbrEkYmybGuc1SxydexbxqfmulFp9d5xOBs+1j0psmbrBUwe8y5HQW2VHLgOLAVpCheRool9YpkYMgKMnXvXtGEucis5Rsh9NG1ppxKurYap6isQl4cm0r2d6J0uxwbphy87diZvNtvE1RdFFWG8lzFV5dLTKBBhmLOQ5yhgzdMRCxcc2eR0vVtEljwlMsE3X3sVjWbKDiGBXJgFg61nVTWsjbV4U14K507kmeiWC2XDK2qWcO3KdetFh8WFr+cCclv0ButrETINTwrl0G/QZfVSURk1xmASzfoKixLZcPgR7JRPIs6E2myTe1UAayP1RHmamBdJL+Ez1HJc+YV8PYmWw3sOzcs1sWxbpQqTWkmxNFsvo13H2+bFCU9XdRlLk8KYZnV9EnSpvI8tUfKbHcIAGkbeR5qZsw02jFYyB5hsViQboCxMAUjIlybMEBKDacHVm4M19qlZk4UZ9wULuqGjX0ZsQiS712ylgK3awdoa0zJ1s6y4Rs1bj6PrWDgbPk7DrXm0t7LCXyFV0O8wu3aVeoFmxFhSj+HeW8iz0valpOglPBBTO1VdNbwWCD53ZrFG/bfSflmkyEW3QS0Q36BiMiRyZGa4Unkmc9vwrE18/BZxKd/wC2TAQdyL5HHms61cydTU1yrBJYaiyeRoPFU28Zy8KEW3Tv7kDkXpznkq1XSt/cNWuctAAefiOA7xyTOkKxhLTi0o2pilaTdpqtVz+aXBBgX+G1KhX8yzTBsKl6iX83cUm3BTcu/MvXWgJsYTbAVu1isk02gsTOpx0rhaXhuykdAAhkWeOWbNwbDdKS8dpdgB+mbETbarVmu6yHwkw4w4JP4Xxj53gudaXG50dV16bcAcpAETbwLNu0Fh2p3ILCnhGZxptA86ZvgcUjeXW1ghsXajrBhKK8Mgk2eM3btWU3No3dgWMG7jBOfbedwSJtwLYQ/jzW2YcNpCu1VqJAOONBSIC/2IKHosmrxeEOBvnS0oqrrNjOzTpX79qXwcWyAoDRqW7fLDCs0q6zzZTLg4ylNsHVjCYiquqQRxx/fvF8PtoRe7RxtodhVf7KC/N4ArcafXtV7IX486HPPvV4y+sFMIvgUVVPXHVDi7qFPwUtKa3epCGHPE3Tch8aIf8AVIslGPnHnJsCb4dLrJv6Th2fbp2L1+LU0yMlFX402KdWlauw6qh/nkL9A7dyi8XQEVWSyRHiKVJMEv8A2vzdTHI82z/dk161bt42w06mGx5hotTWQ6zXUL6v0+6cXUoTdH+/sYfzfTY7IZS80QZolUsVxx9eEzT0pQ2KcVHMLib5MzjTDuZ1zLrcwHa52O+galMAIgH8ce59x3vhE2bSbPVKtfHkkmNFUIN7ojqOKvkH/PUkzfSmow5zJzHWrtojvBYrqqGmrh2gw0VYvtU+GAxzM3jTaFyu6L4GCDeUBrVG38x9NLoTLwJcmmnoIHPTFtUqREw+b+Q+zp+7UuRXP1SrzFbiy6bpmsaiWSVJIVY71q1Ii4Pe8yWRNbjLkIhCtEgT5jYSEcVclSlxoj2VnHQ6njlv7/DUlRekuoJ4mROU+8fnhkW2w34Bxt8WVNm+EyrQO92ocddTdQOboHFNjz7bOGOvcpscYSiv5IJNzi96/TOvc2id3WARLuOufZsum6ES+2m11HHXw6D98PtpTf7bGczR9TxP8B1bAvEg4v2cFv2QZahrWkVDHbT07NwXq7PQCoVV462tUylxd7szjMoNXGrQ2V2T1Zc0FuXJp4uYhAHf+YRlzn9O4rYfVspPTDvYqKrTIYFhRyNXS8FOXw71anGHVYoQN8Jikt4D1jUZy8irQlsOmuuFwf8AOHKLVmngWrdRsqaqImC2NhyQuTUvdjqW64Gud2hBwqTMTmyNaR+oc/8AsuxNmWuNAQh4OqhYHvAGEFHQcizDr+eGnCewSDLez87+dNO1IJm2de0H67S+CZ/tvm+CJa6uvDtx5Da8tCaS7CutXciVKOg8wlOiqtYGhdRfSba5TMAZLOHa1NV+aUkZPiQiRLkOaBmMvKexGHeeU5UKNktbAAgbMy7boIeSmO+4yEGX69TIbHrfN5IBk9J5HUmhWsvg1E6v7HiDeoHa9dRsaXZZd71xFCtL7B/APkI9VtmpaSxqYeULYSpEeSZbEpYppsh6vMEN1acX1UDYYOfJiueL233cCvZtn83H8oIj0ZLVthLYVVzwg3mG/QMBODYMGYAuJ5Hn9ldjYcuhkhCORZdj7hZ7D63tfOeQ5sRPzdIH3WMAneF+tXpKrOkHoN0NjkWaXeQdMx4zaE52lX0R+dbySHxE4PVvXKmnnKTCYRZsQo5i4+tvjSsF802gNleeU2n0sJ1qo3xNWa+p2aYwySI4gHQiJHkWcToSBtNhCcwLtyinNHQkyKsS/LB7e7A+IM6yXzG6lLZLVqjoqoyRw90lC+t2y6nh3LdWYhyEGwZytl7r7lueFmsuqtps+oc+BCZJsfKzNTtG3TKsbxlpCsyAx/XYjfg4tSeazMi0A3rje9FNXuDbTYmerevorp6jvZEPT0lpHqd6hB+KTMTiyNaQo3kX1GdeSmAYSji5tUabmqe3sShg2yG5QKy2uJNkktlbOmchz1JsMaTQisQC6I3aEwfYzkxsevKszQRqUxtVJKVH37S/aCtsvpuQzxoqxYlqdqk5sgBlMxWG11T0qtz2PGUjNY5Bo0N1MIM7xkJSkrnZWvTaXZZlMeuNmDv7MtuUQpNIO18a1lWPbG9SJwO80eUx73eRZ1F9hNhhidaAJslvUBhnzMxlTwrnB8gz4/cz+1uafU7lSLZSyzKUjUmwQZWk65yS8ju2a4IUYBv6js11T9kVWyUOtzd1ov5NM7db520NJjcK++8FlVsQGzUsElk/4bdzj1cl9EHQmXDAydjjbrB6RzegCutrt6dxxryOyd2PbK4u186/HLCHoP3xLw3W2+iiSAoie0zExExxCfE2DuB85Ypwxb47F31RdNSFqfLnLTgXEvq2B0+9mso0CaOKuqAQUtMk/wB0GuJjxhwm3tfGPLdXcmMfOPPG3BvhvsWTf1cwzt7SL/tQrwmJD0jjXrJsoS2vINNhrJ3FntqmmdEOx5kUOk4HiutfJ12xYhcuGtxw1X7Ns7nf2FEsgTxs1TUP2UvbWq+rPVLKKprvuQ4PG1C1CnlS2MwXAlXkaRB8JyKF+zV7T2oiq2TClXEWjm06UXw7VPsDp49ZPWr3z0pMq9k3AODmnlZSqRuh8rkuxzVeja0JH7K0Vh8GhxKBgZi2EEuzDwA8Rs2qrwehXrg2JFXEZ9ZVZ935pDOKhPTMFraU1jKj9/jBzQwDpvon75OmsgkHNTjU1Ms6IOryMJNKm5lYqmdXrGyGylYr+erm2r1uk5NpaIqN8sCSG3OToszBBXorMYnezS18htIHQnyyPc541MhKQtRCFWRtVk2aN1XKM6KcqTPqPg3P4qR5QZydEwrDTmvIxxd0IakNGOzLi7k9RgPnYrX26UuKu9jY61qZaGVapA2FS9RL+Z8dtyTXzp/CxKF1xPUx7OcFWKzZdLdgLXf6dsdj/wDUi7ucxtmI4h/00tuwc1qUVVnmpsHpusW/uaVigDbx1za2imbsBNy2NoTdxxrzZZO9HtHcVZ+eNmszlWBbYh8usG/vfwDtv0DTc8K9JQqsBVxH1kai0X5VNye6jPi7pVcH34ibco7zr5FurD3JMXNt3wfBcZBwYSlOphVlckIhcx5s6jbfsyAOqTVYtXGrqvuGvINigFMTbkWade6hLZbOo8BKb+Kq7aoug5UNWZiQqYB08i3TqX2ra85lbw4w+LkPi+HaLi7chiZh5NM0MfDpN7GwWnxx19+kcXQVF1SldrXHXWjvGV0BK05DhmOOWIOSi+H30Yv9bHHf1W7YadrxKfSmr8aOZZr67r77YulyAUQXqt2w1TK1sEhAkJqRxxtScyaVwA9BTFxGTnxmoYEmTmuaTnM/nDZsOq41yzXkIalJmM5u9cfcoqtylM+F3ujHIczsMeeYI4CVCjfz3pW1RnMNMlrj6jypJYLcTZamXh1mW4uocYPhnZxr74Ozf2EVgCzW9kWEVvqzyLNqy3zNMJSmXl1GtVlBuiHSAlI9TyfJgPJ5ykfANnvY386tZiubTJktFPT9eoi2FVhHDWFIB1V5Fm2gEkNM/mgrAQrapNhZAZzDa82RKeQ50T8CMxL2PWkbF5pY53s4BKZXJjGXcKzh1btnsEsri0+lb+c2oduHFCAAD+dzd77maqo4/Cb2ospjZpFQi6BmaZgpjqeSZX+5IPJvllcDJ61MLyqZmcOdJQqHaLG8PtaNO13Ma7GA6nvU5pAdx/waFZbmSfIs4HAgmHDSZKoDa2jjNvtz7cg1NQHgE7dMHQgzPvLhR8nciri+mCEtdFi0dYun2lIYlTO8S8/gH/ftWoV/Ls0wZCpeol/NvGVPv+2133KkVR4auO2liqBakOsKNQKflZDrdWo6a6ah0XOhUV8Ukbib4OSAKqTXlORnszk2QN4tl9g3xNDjT6AUvDeCLFUmd3WOJW7EFJ63dx0zqMO9nWbUVBi7KhV3hoJ4g8KB0/1IO00BoxNxNxPJe4UFWQB6JgHYFx18LU6cFKbUvUFPEPECnaOzNoM6mxMhn47/ANItDVtuqHZmfXMONPC7FgbodvbC1IX0NsUnIQwFmwJD5ZtF1LFVRJ4malQoWq4l2oWqx2wjzEswF2E0rNS2p1Wu+s0mdRx1w0lVh0igO7SsRNC5mX0VlU1XRqhWHosC0evWvt0ZbNewxwxUwzRxZuMyzByamL8ruLHaTMXbsGQVl11EeEw7tG1DqqTrOlpxrYDtF2gcXBUFysNfqeNNiJCvcBKhtLtKUHG3jKzi9Emq+dyCet5sRKX+MBPuyP8Awu/zSkoryEM/YZ4tcsX8CtbuEBPP5fMijvExEzEzH74+lZPhhaV2wbWiDSk1bRVNy4b22X1BzkWYh3IqSvP38swg0iU19pFm+6mpFmTS6Usnkey7OemK0hAo7Pt9WNOaPJ7QtcxiZqLMEBsTS3tWLhvKqHryMFv1qrrUvOycDdCrA6t+7+iTboVW/KGDBhm7ihRDXaEXws2JCpB8tzfC1vawQrr+wczyGgIWZOWr9fx94Lk1Aft2dLfNKPDO94tYq7kEqqNKLZOPlOeFRjyh/ZaQf20dqauFdvjWbB1+8ACDt1Lva5phZ7V5I65cjpBLodDVkmwFeQsb9apQi4+vaBPaCLrLe39d2lOeRpSaiCA2vf5FllSOxFSwh89qnJK8Vafz9qwdoHGuVcooNrm4BfIxUm4PW9uC3HvhSa9FhNQLQHZ10U21UvB39uwEgzNui+nbcEubKXtD40uQ+XjqNS1TeEmmXGFvlFCpDJMbEgusNqSs8ipou+rAPYfnGvJV+RU7N9NRQvKXyyFnv7cFjaYVCs1bdenNkSbNhtAJVZiucwMkwNu6GGzu0DuNN3qMxLZX8GpYJwm5iAkzoa+krByte5dhwWDBdgJ5VlCk3EZiEJmxBTv1I8cEp8McRwtRcrzYD5qiw0PU9zvyXe+eBpFQa9FmopToI+Q0VE4G+VRqNYSEclpTKgALBmxxogHk99EGoeVKC7GZUtPUtJzqpvlTbpvKLJ7WofE6mmBoBssBb+uTFfrVWXaumVRSlT/uVJaqgmLrYJwLGGmDQZE+Ngn2/frKuXm7WnUtmqRRCpX0NuyPLLNY3EVVdIHwp3JqSWktqrImD1IIR36ZIB3Z0QVv05hG0iwlzUJsH4HmghTyanYJYoTbbLEBYGMm2NyLki1h+G0aZh+np5t2583xdTUom93Wbd0p0jovct5MpBaWYbOl9JvvGYFYVbNRswr77Wlo1yse1XryuAb/AI1olKigD+BzH2LMxnZ2bXpK0WyCG/LvMTIz2ntPb7SnjDVZg0I1XeGFNDsXGIKTkL7RllIKTernF1WJsQq41APhHcc3L9CzedL5bNx3m6ZiA+NL2HQybsfGJnivz+7r7SbFUELbZ40dqb0non3u+H5y/jMve1s3ziTuqt9X6zbaIBFplUxYJwf0ymHe17JTb9ibEsvca9w7UndIIs05pzDuMg8rJOuGUuBPV3Cm9Tms2zEAclJw7jiHzEG9pKmlNFoO49LsUs4rvaJAQhl+grQzW0rXc1uD4H0fHjcbmnpv851PUhwcagPcgLf2tGBMBvEFOoenOjZlMVoREU8rwXb9hlgmxdgIMKHHZptzpG+44oKNShrcYmv6fa+czTF4h1X4uCgUorrTAKBUZg+J+RLwPSd3dTCpM2eN+1biwy+6SGwp4RlZcUFWgl5Oiw83zMcWj0EU50nyhCDRAP4pD6zklfb2bQGjPT6mkHJJcmoLYFqIiMypohukXqwKmtfBlPEYJNgCvumX0vTMr+cdzH9AbhJmRGCbGUZ5j6lu3NljBIRdmUIzspNJR9/EuAg6nGVopU6Vi0dmrU+4qrYRVs7002yEBDxrOvxdNYkml8wxTGEPVzjvtPc33DDy0ppTFriwWk3FOuukbVZdeencbCybX2LZnaM1mLG40utUXza7TTcTe2xnTqUJqhZOv3MZkixXF6zXaLTuVjOVvtYAHhV8pFo66UkEyV3O971fO4piu4XFBgLAkTGCGf3halriYWAh/rFHLZV17d+bkt9qAgltzLNrd0XkZ1VNpBUWyeIjL/LF847tQ6Y+ne9R6ZvH3Zd9wDVxr4Sfa+6RO2y0Y4vHf0mypsXDbCqsVoHJzP06bf8Aby32bBvnqrgNUD0WdN1qq8WQ1WdjxQM3ewbnygUCxPG4HHbQbdNkFYmyDFYxrvndi+32Gmsmz/O9sHnWYFRoqdI9gPAtXbD9GHn56yXwFZ8x3iYie0z1T5DqTxm7LmgeiISyuwdoKebUZd87ZaCoNx8ipDNvsDzGpDJae7uC7GuRQc9FhVMbYHRvhcNgKgyhPaJbf0CocmCXPP1ZpGcpfyKjVIhf5QJYC1wt5NRTYJDBfEjaGqU3tnzuzH1DeAe/NV6g36h0DuAqyQKlgsB21KXa1mbrgqQuo1JsD5KIO5D3iY74HIQHArN0nG0wrS576Omi+5ylQYMT8ZNd3ZrUrgVCBrHmHkgLG9RGwdWWnEi0a5t49tjOLW91ptaKTY9+3vg7EfNJr6tiEDZVNrWrVbaa7wbEvbCRZx7X+/pXWva47j0qZo3q+ZTK3aKRUMjE9HyGosAg12IYYmUJdynNSBt/tjUKAsSc8izx9qGkazqsBRAfJqAx27OJ0ma/AjfGb+kFlJIq0lAyXM5RQSlzDh0eGFkY1LMWgk4U5XY5Hs5ukjdqpC0LxeZkxO7fLLxLd5afKddUnAXNpNLM9yyiwIQvyM6pa/ra2ki49zlTdUpJRv0iMA7N7ncmn1S5DTuPSgAeBPJghOroJy6B3bEHKl9u/R8hoKC2T5YmakhBi3Wmzv5yqrmriHsTZRc364hcT/eUOTUmz3dtVqaqxPhxC4lh5am9XufPwotTAGYFP1JS7d/g/wC9v0pHNvp0qsvr/OIgyWQ8ibpVFS+jb7FMgCK13ZvhauW0kMVad1VYlDbuxyuKTTV601CcITGwnfrIPXgwc1jJrm6y/aNFVsAFZEyfRauhQDXY90XVUa0HB5tvUjTs51l6nsiqqwpk7WkPDwvmavP7Mqc7BvWLVzQSb/ZRXYArdGof1QeV6x+Iaov8z+R0kRblwPCaqIsHH1FRjy9/NBLlQ/FfKKLY7gu185caBXPIqY4saviszVlUu75llzeUaSpeR14roaod8rdekdlFyKq1LKZjOKwzOrncCAsEsZaOLfsmerNtpWIRfJCgjlFCfF2Va7thswMbtMorSHkOLVcrCZZyKkvKVpTD5rNWLflc5FXlN6FBbGassBjaTYdSQ35ycGsSgv8ADX6526Lq63nXNgSMNpZNuskFTqMMFmExExMxPae09K4vXDLitLpKwNZtcLL+LTYVAN0nTEAgRhvGgsXbFp9o5l6WpKD4nJ1mpPSbPlpBT75uV+m2Hyl8+J3xnwauArUvxZc6e0VySIJwJG4Vw7Xke1QLeT+NeZ7WzdKJZdVb6jjn+l84OL5XRL6XHxSHvv7GTzf0XEJOlYqlpHIvUhUyUFKpiC7FMdvknh6V0Bp+4fjlEpf1RqTVifkwGTPaJLYxP1ZwSdolgEjIwnDCvftWUO7RZb5iCpxNNamNb2jICSaLMWeKzaqymxfaTIqxVW2zx2bF4LTb7ZJdsLIRW43KLFZ3vmUotttdX6026poFsKkpjvNbiyqs1jTYn5oEw6v8b92bne6a4tVhrzDuMC+xbc22ck80tHrRwJ0qXqssgC5gu8FxxLJugdlppuoFLRs4RWMsqRW4Dv4/7Qe8D957z0nItJ13XY1DlbmQZp1KQaWZZpMmRF6yXJaXHC0q/gsX3SM1ZrlE8ZMpdM6JyTbSrEyfG4m5DgunERfm9AU+OTVsU2+6R+s5zu3MVOfxi2msDWuOBgBbxwLqbZ2bLCdalRfNmIbbdOxNv4FVMj7W+OtqUbTk2X3bLM86fZ/GitiMuutjt65CAYRhk2aUX2iT7BPlocYgCmQunHfQi/1kZ05qXBL5d5Xm7voZNqzojbraZVpBcrgHceQ2+1suOFOeuw1J5bZ3w04t/YUynwroCrYsaJMIzaoFCOUh00Hsb80WbZk2eqeAa8x+devnbquVKpGhkTSa6zNk32mJBMNDjf8A6MFBt5pyqzFhTaGZ6d21bN0ufakPmTsrybRaIWTUR1fWIT4nBqeB32zNil6jD1sWwmrcelpvbYFCux1dA81CJpAkQulMODBfrYylXTmj3qnXKrnZE0dF1ubRtlqFKkNbLfouqtRfOrNciKIrAaq4A5suMRiCZTxLTEXPM46c2NGbcBW4rKHrMb5dlw/4QHHIWjOUNw4migkQZ8S8uaNItJ/iGoNaIDCKKWpXO6c/qUlJFRRNWimtJyfiWIfL+ciiZGYgu0zHaJsX9LMjTaq6d1VSnHyLNdfDYv5U25dI10vU2NbRjhta6Vnu4rMLc3j1t77umgnm5Fdwgr+VijuMxEzEzHbvU44pNN1J163aqtWSyVSyQpG9oOab3AIE5XHERlLoFZeYJdDknSzV0rNmzBm51mRlh/8Ai3ea1OBfchkqaquZib9VuLOdau3TbTtpmXQfKqq0tMq1juolQYByRBPhJVbITFv0yl+vEWs2x7j0JjReix0Gr5r2eJpuVpsg0gXY5MivL4dTtiSRUUjm6IaB2RBRpOs2VMDU015YIM0ub5nCgY+pqnfwmlwWvYKv4J5XV7LJVS22DrMsx1r743MLQip7VZoUIuJZ9SVaoGFwHrNS1F2nk9X+yH1rEmdr1JG/vurV9B6Ktn2K4ImUaOwOdWh9mnZhcQEsnK15rXLNa17LYbqMrJZU5Ci02qEIeHstaken8jTXdbU2nZE61T3O2brp0LLUApqjUAN7bNsadQGmVgRJ6wko5JX9zwHVshHuzS+eVbG02/ES+ZTZlRRv3IpaeQw7UoTLjhvT9okZU3251qAhZNkG8qqBLpiraMEkmCI99LuwQNtBhfGocdUd9N8nxWq2TFPziT+pkdj71bMEF2KZBX5Ghz0pmrZVLbJ1e9LkiLfqz6tlQWWGkDv76gVoJam5WbWqxYmU601d++qTdZlg15RWta41riK7Klj+3ZKgPL5FW031lAh6ZsoJ65t2VVKbrVgvipISZzS1Hv5O0/FbBBZwuFDeToTDpbUswaLS6phmaIaIvkFMUSHElg792KGxjtO1KUEbYb1Z25qZE6Ls+2ICuXGDeU1Vm2Iq2jFLUrIj3kPlQ/C4iR0IpnHSeTpaalxRtwbXtrR0nktZ6Ks1qtlzrKzbCI5ElOnZ0Ymy+hNBViBv79am6yHhe6KixbZL6mre14ZQ+I9sanz6wtqUndrWDdYIL9nqeW0fUdZhFiRXVm2PUavl1KaSTcR5vn44tcnq1GuVarWUkoFs7N15Xl+9NG0XYDOQPllQVNbFS2alrS2Tra4WJuDFd8OpthZq+o0yC59Sz8zuTSkanJEWir9qzwh/lju/kIVssL7qFsFTAEXUdY+vKrujUabbRxoMEBDltIkm4UvIYrnZXNfcXYdKYqWgZKBsAC+S1jAoitY9iLc0/Bg6VgMey1ybdkwtWIgc24vRzkXUd4U9YsCP8No0xv51ioZkEPWS5JvHKllULum6yIolC4uYY3KEVH23yMGsomeOplpnNp/c7o3eg4xWg1SdlzYXbZa+IcbUMVf77a7VQatXU8Rq9pj27Md0qUfVDNCjYuPBrGTcbDSjlVW1aqUgqA0yC6ppTHHKoOh8OdNqLBPl1rCC1b9hlp0HFU63U8YrEklHZsSJUIo9Hxms6Tl73tbIqEWWciLJUzbab3q2IeHVzARcbfNr3droABRpccRpS07NqzMsUCp6jjiu4lNx8zF33ek8bQmwpoWbEeJ7XiIcSpjBxFmz3bTmmR0MkKV07UPawzQCZjVzg06o1zcxYi0Gd542iWyc23/e9F7rPzgoOtmLTZNt0uKNHLVpWKjnGXasZFC28WQ2qFdty2awSxERPFkSpwTcszDZR36LjSJc1k2n92XRuf0N4xXadhp3LUuerxE2OL1ok5Gy8IK4FzsPHVC4G+0+ZXcK51gYLZoLnRiwg0vewExxGpAOCLVju+nFMydxpDnPcdp/lbCviwuNpO7FubdiXRZ9gSzuOooPotCy45pIJIRdqqvUn1LEd1PAlnBcaUfzk7tv5nTipJHxaqXm7WXB5rCrExm5wUDtSLTObLyeXWjkp0rVR7jLtVkphbeLIfUis+5bYEIOvETxZHZ0Tcsz5SSU9TxtEtk/af3m9F7+hPG0JcoxtvmVWmWuqvGUVARKLVgGoBixafEqU1n1ge8FOqjV7WsCu8rJk94e2oVWYPjFaTmYsujvbC3EdK4vVAzLzOkjsMecnx9JYjcqbTvXNPh6u5z362b2+fhrKZBvRxZCDg027InCAQXX0tT9SvVhz4SgGhAxxNEVHV/csyDkpTPTcBLfembVmJutBrJDjNcCGQsviBuzdiG4P2VlADzr+ebc2tXjlXVNxve8IaoVfEIkRiJnvMRETMcbrw5rxsOhh25sydvGKpxi7SpS1/8AdGJSlHHRs0Am460Ng6qkTKeMVknLVPcB+5Nseh4zX+0e3ZKBc13bJoBmZiKKjMwQEABfzhvNajAvurnK2qrmYli6to9Exlz7SApLaYTyVAjEnVtQU3IpSCtwG1ntXTtSxDTU1M74xv1rQudNOzly8K96y9p0aqoZXa9kG2LN+7FuxM2rEwnZUkQ+oa3aPmh4H7sUiHOvhoKeQAxUpcaDh+1qVKDlQ8nW6d8/OVS46XXbXd9lBPhVdU7ns31XUHaCsdCwZJzXRZza7xIyhqhMSq7moORopbZ+dyZM6TKXIorY6juC97QzgusKN9Z6LqaaVtzEwojk94HYkBVi7YsOpsfE4Lnv4rQeZeWwymByWVo3nkwLV9yLiqhzZp0r95FXBtttS8L8gt4cmuX6lrPKgf7mcuTQ31piw2y87PnvkmoDuW0gqTYhFlvZRuYDeT00mYmDolQAb+rnJUzmXHxVvqBEsBr39/SZIGQl8JmC4zqWtdVLvdb3rIFtvq3yitT88Pp3AlEKkot7npUWW7VC4AK7yyH8mqIt2kEiz/dCVDjnkaP1IKQVrDJN51oNu8lVlqTqWwMKx2I6nldXwtbFS1/uVl2u0afd1hQVLBymQHuHKKzgqGmncYVqWiAMmw+lBVp8DTGJjri9t97jlO1aP5uYHc5byKup1hRV3969pdY+h5JWm3CDrWg73JpfOjyM5zSt3qRK/vs1Y609wXHWchraoV9WKljrOue9Wh8IaoZmfh1R5LM1ZO/VlZncOskFckK1qZyaVQjRbF3zm9yNNBrAfUsx8ENfE0NlV25FaEPSZoCwHWjyWtm2baXVrRzUUDzIuR0lJvssi1EUZCGxHJ682hq+rZl5O8MjQ5LVuuqJBFlU2wZKps8kRWG5L6dsJqIGyYX+TEqncOpSbNioaYMNLaDMQL7NSzCu4Qwj5ChbmrKu/uu4FQutbTDLSprUudDHAmIjkKJeCDRYBs25qEIckOzbzAqUjldtrls6rbwWWvBNG5MV2sSw071SYtza70YqMhTerPJatdtqPWe0KqhcZu5JVSbolL58NlVcpDkhKqatu7SaCqNiVdz2f/UQUda8kvg0hXHLK01TdFO59qcXOwasG5qwqvPxiE98nSVrVJsJAwgWGogsbR293Gml7IVW2WqI8W3Fys44N8yFhi5j/DaNQb+dYpmZBD1kuSZxpToOW3LEsJAVxPUwW1hRGdD2+TVVbb1PG0f52HyUtY5nQ8SpwCQJ7zFNKaY9Z1R4X2ttERypQIUw+OKM3nNt/d1wbnR8ermgwl7/ADFbG5LsvNHOCwIuY6HvN8zOFUnSv3Z7yd9Iob1GAgcujnpc4FUyEo6RxRKIgRu2O0JcmIz6kUc5FQDk4QsVwQcdqBTFJkRtAXCt58WQdUU+1YjtQ9GZo44Ub9m6FhpselaS6rcYQhKgC0/uFYqslQzwp46c4GHIpTCYMMKO4G6497VVSrLOphJqxS8jjfFEIBEWqA2r1S0TDEqsnMCritBQRCZMCC4dxZchzbz3IisuGCpLJ+VPJOH++8/DcsKX7Qnx1R413MO06V3GGwzJMlUJPzKJIJH5r4ylKaIKtvAqiJrwZ8OqkBh7liPmtQF1r4CdcmTZsPiDR4JF/GFPO7M23xN2USzoKWpHJDtrQEAdqe8J4rXV+1uzJesyvJnjxQ7WFA+/PqBQlKOPgOPTpFZd3rmLDmlxpFI6cqtO7U2taEOAyUUAcrKY7QWNnRlZq6QONoK+wzb42izbsP8AZeubD1PKPpxMug5tv+1/3+i46maD6cWXwDLPsj19MpmS72nSM3veIcuiObWmuDmMVBlIDHG6/wC4vdEhaO0vpuSLLlO2T2eWpBwM6vF4jNtNrHZs2vWsrAMPKlA1rlk3FaioCO2lx1Og+807LQm5XBBQ7jiLEX4e9pxdhfz6bjy51RrLZ96rZaMI4uhU1O1yx3qw6A6+kq3ger239n1Iqn07jaH+/LLLp92FQfWnxpGmTZs2rHdgLjp/HUvtOf7TwltlVmY5XWs2qFYKoGZjcSfcuOoLsc2G+eLkW5dV44mrFX4Wn96r2uCQwQilo1fcswN5xuM8zGTnNtmBScWjEiB2Q6/yLVh3nRTfWUrvb4zWsPeYvamHNS2Rs8eS+po1vacC77IacXMgbd1Fo7LhJKDTEfo0ZUJsoizflNQaUIq8eBeJXoG9sEDBaRY+UrIrNroYZgbjd0njiUHTkLT4Ck83pDKzRzVPAHG2HPN0z/OGvpKyc5t5y2GtMRJxG+EWorHSuLMxaQQXK6goh0VLZjNL3uqm4i2bhWiz8lpB0Rp7cvisdX2kGjWXVerM0F6Vc2qBi/gw1GHI9qYQ9FL2IbVsIFrbXJKNa4ysUMLxMBTTockrXnIVCLKpfDPhNDk9bQsipFW12OvNkDdyeuhxqdStg0HqQQ5eivTSxigYqVNJJhb1JzeVvEza1RUVmuta5NRqONLwdEplcWJDlNe13BFa52KsdgGntKqYib8pt2UzXh5HS2mvu6YHUOE05GQKeSIF0oOnbFo2RqkGVoK06fspAwj5mEg/dWg7ipqWDOrK4mD1wVpJpHWsiTpMVmjldJwgRJsoA67bAll7gaVttYKthRKADmb+mFCzVSVd7TtmQL6XyWq2EimvZbYOGd0Byak91VVUXWJsrFwdBvtg6gIU+1577qx9a2y8IMKU2u86qqbOqoeGqtckwpEYjvtbDqu0gFOAaqDCLYJ2XTs6VQ6TJVTACEj5NUATlybAOC1FWVU3+zVW/wAbFeQYL4ByOtLogq1kB9uakmrlNB/3SDiEwYaSv7rbJ41mmq4lTri47DvKblTfVUtGI/P5BW5Cdu5chVVvqhTXZU095NPGReMLb0EpZm4OR1ytwia1kP73NOTv2059J1uyXxSoZI5ztJ57+r5lWgUqsoxQXJUA6UHTuQ0LIVpDL0ValSbCQMIFhLIH6s0OT3QaTXxKESmq3klFVs65Q3utgJadjkQSF8FUr/8AdJNbWTtLQFGDTaMbRLUD83dr6ZumsizK0/Puw+UVlONJ07YtCyquQ1eSV7D1J9WyomvOv3ocmq3zrgqraCbMN8U1N5VqKsqqWpiwtjOqGoF61Zq+ByXV/jJjvao11WaSgeVv1TdHWdsSKVzYOy4wy12jj6lpQEsaDwVFMbkEewSqrXszL0eLvMiXJa4/8lW26PTi73RugO1bODbZS6vXbURb1wq20IbVs9nthIMy+RVtJ1da0WVTZWTFTv6TsugD0IlxS9a+zeSUl3JQYN7g4EMK1yVZZ1l8VtBAJIwY6zyetVtWUNq25mq1SmzW2F2EvJVaz867/AacvRXqVTcoGr+DSUQf4bXzx1ct1E2GoXRESd3IG5aqvJ7FurAYRMcVVFaE+6+QigVDo+Mgflj3nx8666/ceMJCZkbbQ73RuzGVmxmg8IcbfM83zNvjibM3e1pyhuOBzBVjAnRsXEtkCsEJsGnx0Kh0jG245pk0gjFydPuabIeELSGi5k8SRD5YNtod2obMZeaOdFns4m+xYJ89aHHkX77rjHsAjrQkJTjAi622Dylr/hLusbM04sMB6QUmyDodL+LJfSVVbcfK1U5q9TgBKbyxu2A91YCfSuLqC15gttD+8hYkMjNHLrGgXE35tNvf0pu8ii62s1EVO6xkOOqC+q7Ft8tW83jIcbGkis1LW2TpIeClcUpX6JkuyoPDKQ7t30WX6mMdYWdk2SI2K42qua21rb1WAhkE6eL1ZTUQDWwip8JUCuNqVIyNt3zC6dyDbxsGuNnuOiSuhd6poKsjxm9j57zPzdx9L6V+u55GV05KXOwIfNuZv2Qm2pa2yHG1CFsBsFAWmw1q82mGdnIpqIiBAQAzl4zbLrh34epQ6h2lqp4CaNBlGu44RIGAQWEBVctE2m9swwNcxxdPgFB23mES6Zinx1dSDhNp3+/TCmXT+I17FOKp3HyEVlojqOOxBwc3nd/d9zrToq1M51J8lAOHtMswIYdwjv2YbcrigzDiqQs+YLTB72FWJDIzQzENVDibDXm6ZvceTeu2bR2GgbVgATXxgrXXWltKDecMbFrJOrmbXrSdlt/5lABxcJiux1p3mSaXdRxtcOsvK4/z2USgmxxKvFjzDaYHdyHTAcdEHKbFtsyu4dzqpx4c2KjV2HvmjLzWuhkQ2pfaXs0z0e/2ysVWZbe9TjKXgAmF3GVcvnch7UsZWKqzqtx5SD7nZa2PSil2jiaJrwltyw0YpjT6u4fv+Kbd15+MDCY/RLEa40xbZCoGTFSbE8Uq9zlL3J7KSpMu40tt0bh3Hy4bQ2InO48FA6JBaafpLYAxrZ4adKaxmQdjE4OvjKrXnWUumPOcNYM8eAsW5mTbdIWmm0js8aCw64ZXGxNtqWn0fHQKLMe4/tYtRaPrHy1ZVdyUtMxY42/zjy1763G7b6zSS1Y9xNOuD5bNarZeCz+At+qqUqrGpNtpWVy0Fa2q2cy82s+0DKt9SpjN0laPsQAMUVZspaH8q7OcOtlPpG0lC4e0n9NhDikLjQRLodNcuJJPKRnHcdNZPTuOC1N1c3GxFu2FouqmOVW26wq66IdamyYf+L+TzYVg2H02mpqYh3c92x+t3Ch3ei+sYU+rOmeJVqqtg2x91qN6eUJem48Kj5VUE5PqnsTbzptqqyQyyRGaGtN/dr3UxYhLs5h+vPJgh7ETSb5V2lVyityQH2q6WUnKl1plTurfBjkrmqyJbcZTnqxaPjmP3f7F4FRLGOpWmhv7Mj5HAIVyBdTdC/fqL8T1SbHpmOObpBlGmIO86vL2O6dyhSaznTTbMBQG/wBO5QKfY+dCxEVoTJzvubTs5luLBLrjahdgKulo2ysUWvNT7NtTas52wd+49AUHQCHkhjd97ab820DyUgLYhYGro6Nw7VJtkwbatKbUnM1jov0gsA9qP1WUQf8AIrFi1ZrOO4nEiUI41mpqZ1YQOQzWeVHV/jlK/ba9xu+bZXPQYCAc1/tW5exXh8w8ZpQHwgmfe3Nw+g4pQUqVDYtfCa51+wcWpC7yg54TLFNmB45VB6WQ9/dVs7kdRx2rFsbAtfEjbm3A6uBT1XG20Te5olEw7jVV52Da6xMvlMn1U43UqW12UufEraxwgjjNJIJgW2J8ct6ni1OUGmbFqROnFLp/G6rwsidmz2sgoD61M1GrluoXO8qaMCXUZNSNZOlAz50omuHVbHUhN5S7FgfdYTTLRy6+llNzrXyJLQgDkMipGojQgOzkIlAdfT1P2GnJvkW2otmr/wAoTHWBfsaXgptu2Qak2Oaf1pQKsb1KNoeI3L6TtQ6y+v6j4clIugLOwVzSoQrzVyRpeu4EbIt2IzTrNSw1E4J1rd5Owablt9Cu1qopWrvJqdkLNRMl3mWoBmTyRMYIWHSTk1qa5fZ5JsGeNfRENp3a0Jb03WGNZOa2s0PZ+YgfFNqTpZ1G1Djc9BtF38nTHeJiJ6RxyulFQAfYgqjjatl/DNHFbmdmy5oygloRHHa9mm+LTrPltJWthhxmoq1D1veExZiz8M7jNLOupsoN0mkDX3t4aLhPh9iyaXtBrK4YqlJsoTZepFgzIlRxaj4iTJvkDqjWYNnjVW2m0Nh9g2WgADanjVRN0LgPseYGm6Do8crUXUmre8ppKNS4/wDzocn+/GdKYMwIKxkJYEme/jgdqyYOwRccW26IYHGH0LdmbR+dx9RqO2+U3LEPshRLKayoNvVsjwzGq1br1WjqzeM+Xd7fFj5LWu2wlqEQoKGWdDn505v3X1m0DsAEFpnxjjB0Lr4uG9/34BejUnXuib5A7fYA5VFpGxcdft3E5xQsEW+eFIW8Gfna+B2/g0D0tIcDTw7FZrj9Z9qOuF/fh2T3/FDqpr6Wbb2aoPe6L7nIodZYatz+F1b0LZzdafeSq6ddT6wh78+XVBR1Y/krfq2b2JaqUiAGvCVfP6TpFUoKl1lTqVaK4P8ApnOhuaaoaqMyCiuDuI5jbUvVL68+tNWAyeK0cmWzVsXJ+aYRHU8OziwBxZbbmoJ9+1/jtS+aHm62p6VyqHnxuh3zYVLkhm/euLeJ0pOJQbUB70Xmru8Uzb1+xadNn+8yMvTPEM6TQfltjKLJWg61+M0Ne1NmxLwYSZQc5HG0ZV8LKbLyFNWKql0+MUKqnhEuOWtN0H9BY3gQr53O1f7JmeG5ndUyduWhZi1LY/8AEPJSmOM6ZgZhIVGlBYzXRtZtC1ZaxqKzfvVI5ocdk22mw648GDx1NtGZIXTOT8pyEVPYDmF5J2mtGaS2CNNt+ppYY2fai443Rc6xNRtiKNu+D/nqScJKwH6Seq7Os2znOoTBnlg0NyzQVccSSpJf34157XGux23yfmdEtpFpWuA5x1jc55wuXFgOOdjWrD5oqolXiGl7Y7+ymLRtIUKlUQeqhudWrTaDSfSf54oS6dpNAJuJh2cRWenXLFBG/wCBrq8oBULVQSc7OvkxZs+tCUGE8aM5fpgctV8LERFbOrHHI5mpYsHVrgYPLkT3Nu64xbNHoZsWE9aV+41Wrdl5pbRXVNA7+g8dOl97KK6LqgmAKDXB9piJiJ7fyHdqJvUnVLIySXBIGIZVILKbEJ/tkJlKzVx3NUNQRUcRTOWIgaqouFbgZ8xhATIZVVeoehHl9kxgCJdFC7ZWxCSsFHxllbFoVXralM91SUqFOZTQFoBTHwtnJvinlVKQtisJj5YgSOlkVKVI6laXAo5kpihmV8+hFKp5QQMdhipUr01eOuuAGZkp6r5VZGk6+vzew6Ig5RRQm0yyITL2fYmRl0oVZXKpmLQ/F0rxqCk2FeGWjZGBdI4lCKtiv8DILPbyzSoV6MMhAT3aXzMqGBQzrHmqi4T+/aL+PQ0G+W0n5HIeMpfkUbVr2XI7n/u94u51e/4ZsiU+BsNX/wD7Ev/EAEQQAQEAAgEDAgQDBAcFBgUFAAECABEDEiExBEETFDJRECJxQGBhgSAjUHChotIwQlNjgjNScoORsiRzgJDCVJKgsfL/2gAIAQEAEz8A/wDtsfBrPgufCz4WfCz4TnwseJJ2Cv8AtuWJrvOcUnRZWcXpSM/hfjL4Rnc5HAQRnBAPAHilnCPrmKx4S9V14elkebP+ZyYz5vP+X+GiS+R7Tn8GdxkcHRfDn23OVxDITWcE6NVn24+pM+SnOX0xfWs5xcQfFg80Z/48vgOTJncd7yPSEZ/Lr/8AZntXLefxLzzXqr/sbmjr7/D/AAj0VnLH6X4z35OPeryM9+bjz/8AU25xRqY1F/7bl92s8R2rPSdZW0Qz2vlksmv5F5ztMW3npWyNFDW8k6GyjU5/Crz3iqcnI5X4DyZHvZkZH18TOcNO+WTPWW3fX7VnpC/i2znqKR4mcvxydeB0gPnPVXs+EmknO/VvPW9e46Jzh3p6tgTnmokz1LeVsjZeembKz2+Y+E8fXnBaW8meqVsuazxKni4A0P8AY3HfEFMwa7ULlc/Fjy8OcXPwtHGosZHNwV0RjzcSXhzcR8LOVjxMWH0h/s/eWJx5nY9bOcHI2Jjzs2k56nlYzh5W4JrHP+NxlstZF9+Tpfrc5eVniy/CT7y+4595Tc595z9Z3X4PO+Jzl5mYjOPlWJjS7HK3v1Fz9j/ufh728mekt5OPOa2PGN9UUPhHP+ZUlBn8Iz78TnDzN+oidptMv+LnBzdcoZy+qSs9qrRSZdpPbI5euOWDL5WScb3OqyO981ck7JDJpeiQ3pXPvDO5/sL/AMuM77pXQqDoziiePjf/AMtZxRt5v5To0A4+pRqk+uSM9HEcTHEfoNO3Ltura77Vz+V/7Hg+uXee8cee3DdwlZ/52fZ1k87Ec0ZxUQQjjYqGXcO2s4uEljirG+3NxL9GcOvmODkzmst9L6bOLiOM42foz35eG85YOQrlqdXntyQ/hFk6vrx5fHHnvxND/VZ9yPDnP3jnJ+vOe/y8v6TtyLDvWbKrU59uaJz7Tf1XnFwnGcUSbhc9JfR6ftXe/wAOfmrkZM+NJn25YnIqMqyrqqyLkZpc2WxMZ9uWTPMEe/JnDBxkaPyZ9yO8v9gnm6YgAyIL6YmdaGvt2k2Z6m2j/wDb2nPTn9VAiM5eV9nrcn34Zds/rOfwZtH+gWTl2JcY+phCfxK9pU1nxpucjPvKbMjmlTWT5trwZ7y71nvxLnH3vlt8SZVll592cj2k704Pgkzf17MOaa85/wAODOLW7ykuLn7lGe3XrfT+BWp9NFGxpw55jzkcs64T71ecuV45Y2nUY1soh/Bs3yjj6mdcWHjijYbz2jPaur8OR3TxncpMvJ5pyPaA2v4FdiZN5FTtDzRGe9tfh46Ews3XI5GFhtsUx5Zubk/bJiq7RManUonVXl3kH3pzh/CHTKeETw5ftc32GewoXjbvl4J1dy/dejPLqZv+hweeI8decz13nPN/AKZxhOiDnLKwj87xZz8TFXyZz8HRxQF562K6eRsSkx8XHsznwNcWrzmF4eqPEueminjWrGyciO3Bynasnzc56nieP9Izh9I3LLnrYrVl96qc1UxqfoSs70XbOc2+Tjj/AMvPWFbZ8tRhH59eCzPVmjr9pic1fzPVhO6zvV8PMH156CK1ogKxhrm4GXt1zmk5rK7NUZ6OHZHtd59+LlNdeafoTTeeiitQR3iqys9ZNFsHvGXGe3KTniHlM09Gvfkz0cV0RXGaneWrxur9+LPWxRN9Zumc0pXFPi8DtwHLCM56KQ4+b9a8dOeqh6HmDxWemhYSqFJyfHBaarPb4s9oz1kV028g9aZHvwm/zznqt8rFl/SV+2TxxcxolastND7a7uRza+Pw/cnJOub3FWUOzsk5/HkXc5/4+JusO1TL3SfvWc/M2X12Tn69f9LvVZ/3Fwms95HNISn4d93r8fcnet57g45OVvdK6ys7zX/pQOXlf0PeYDbX4PmvwZrErVB+E7DZnvI5G9FfhW8n8K93He6DO65Ge86dZ3HWArB+CIA57En4HcTPdkdNftz4qpmKmdad13pDppfBl9+gnK1y8eAcaM07RrtpnPTclNzWtFdUtbTOPm5ozi3d2cXXufuL15x+JKL0H9L1WRfXFnXkbeQjJ7WcRJsjPsNar8f0M+03nqd61nB9H4Rn8Yrd56r6eWvFmel5Vi8+/Pn3uc9OlvKn4f8AMm6z3jkAHOb68rI5+3JEj5gz1eHbjbzg0nL+E9rOMDZH4faLzmvUZxd4Jz78cfjf+Wc+Ocg3n266z2vWtz+H3j2jPtxTn34nCxsG/PTj5oA05zW2/tw6pvjrqArTpqEB1kQSHMim1pb7KVTh6BoU7LsnTkel5MHmKnLu+Ti1WX3tbB5agHsSdmsAJdBMoHbuFoHgv+l6W+imc5b3y3j6rF3PbKR4svn3xZ6eyMvn34yNFAZd7GTF7aM4LJzn5W71nNq6pcL1ycVOer5evozm1dNOF9p5Ml/Dt09O1wvtqsn1Wcd65Yzn5mqoRnOL1PRl3tWnbvBOlly3btxdzqciwnI0IGVRpjC8vSpWN/fB0VnpOXo+LhzeWM4tSymXXZkzZ0526dZSMTOcVaKhzZ1ZSKgaN6DO3T0/t3loHYh7p/iNGcmr4fR3aV8SZ09nC6vljhFKsrxpRrPuNiOUgAYa+FwXXggUKuYfuu8u1v0/H5Y+xT5t7E4Giqf6f3ifE5Hu5xeqnllM+bjvxZy+qjjf0me7X4FBMUeJTyrnuJLpy/UxxNr7TOdo01flpyPUFzfX4ZvPinIXNZ6Xh35jbt05fqY4Z/8A9M413sPM4+riK7+0S97/AA55NxqNiZHqo5dk+ZrX0ufNxqpM5vVTx+HTM+9VnvfNd5fKEzT7dToc4uWbzl/7H0zRqrrLTe4z4xITx1pppxQeEnL5iI3M7aqk0Z1lnb3KOyOOi/U6t1MYc55r7RnxDk7WbETJ23XTA989jhXRWR9qy9F9fWT0Zw+qnkmy/DNmHqYSOrwmPqp+J38X8PyTlJJwcE56PhOSyScvhDleaa0cVGkEz0HBNu5z03BN818hSf1hpJkz0urfU89Z9qQ2ft8z2d93R2EfczU3HZVOPqO07VZ0I4cD0RyF430RwUZe2zfl3W2d7/Wv9jzeOEDxOe8Rd7yeKYCM+8YxFVROMicsmGui1fqffZkQUXxOETSxOf8AgrZhEzpjPYJVHPaOmWcYimuNz/le8YRD9PjVIpk5za1BRpQnDiiOiU1tQ2ufwrGIpuFzR4HZZnLItJ4UyJDK8BOVE1TFeZrGJpTk71lSU7rL4os5GpJSpc0T3r7BnbrlaVz+M57T0Tow8V1gOUHazwN+WT2nOLSlTnLrr4iXYTkcU8YdHgAzt+RnOid/EPvWAdRvsx/GXOPVTf3Kl7OMTZzNu6Uew4aubPuy9t4SW8sLvAi+iMrQ0nlddtv9gT7p42GcQFcpFir4Kzi/Xe5+6+94e77q+VfdfwPWtNjaEPDnq92UTzIQZzyt08v21RnKpVTpdR93f4cUOrjkmnVC+3TnPLqZGsje+W4oJqDb2cdq10U9X69aDhD2qrqXWafpbOgP1jPt6Xt8O8IeTr2bRqa/JW+wM5xeJZnscmek3CF3l8NXycM8ftb1zlxVU3yznLwoaOLrFkvOUWOR47YJjHhrzyfe9hOHFQlcX/Vl8NXTzVK8gAm2c/hHKQshWcuokGM8807z1W6efOaF5vz50Vq+h9rKSs4xOLi+LSJeQPT6gnjLz1TuOUaTQiAGeebx9ePDfjipE+vItnzc5z+sv1oEQ1hF6mzlOMPK1kFcXbpa2gueqt6Mv7IqzO56qy5udos6kUTObdRP/wAOUz+tYcNSbmyfetuRw3r+svQNNGsiKOTUYRb0PUEjl8KGnib7yW5EPf1PHbM+XxWRw2BfFDXlrPTRoqLZA2rhaVIxfkHDirrh4y0V3nquunl3T+XbWeotmOInjm6amUapa7C56lrk4+CiG64ytjWMX7crGp1WRw3W5vjb0Ts3R+pnFVAUc5Ocv554mJ6kQTO9nO1vfw3OXito3BRal5xCTubZ/Zv4Btz4GfAz4GPp8+BnwM+Bk+jx9Hh6Q8TjwZPpsfRx5nx7Y+kjTb5pNYei4/8ARlekj/Rj6Pj1f+TD0fH/AKM+S4+8HiPow9HH+jD0XHuiv+jH0fH/AKMr0XH/AKM+TjVyf9GHouP/AEY+i4/fL9FC6P1nD0fH/ox9JCNuR6Pj/wBGPo+P/Rh6LjL/APZj6Lj1H+TH0cFbnx3Jw9HA3h6KNX/kw9HGocfRceo/yZ8nHX/7MPRcfaX/AKM5vS5Po8+Tw9Jj6V6cPTY+i49R/kz5Lj8z4fow9Fx/6Mr0XH/ow9Hx9r96+jD0UAzXkQnD0XH/AKMPSR25M+S4/wDRj6OGf/Zj6Pj3uf8Aow9Hx+a/6MfRceo/yZ8tHtl+ihx9JDuF2necfRx5nw/Rh6Lj/wBGPo4739/ox9Fx/wCjPk+P6/HV9GV6bPgZ8DPgZ8DPgZ8DLhn/AG+t+TWfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOfIcOTxTx/4SB+wPL25YbI6h17ONnU/oeX+gobXwH3fwewB5cuiWzUpWcdj9BtRdZ9hNuPN0k67kmxWky66DimvHU9+77ALn691f0kXHn8R5+LvXjPj/596xdakFp/kGfELSePStSHYTuaXB6m4/w09xxoG6jJ5zkFrbrtJ+3PJro1LS605fL9cninYa3nxDb1eO2/f2x5AZDypvsYWJI+FyLEn9Uc+LOqJ8p37h751m6n7hvuZ1mr26Nd++FiuvPYfbGyJk0u10uvbsLnJzkzx9Kitd9inZDKdPY3pcjk6zbPVp7CYWJO/G3fbGzVa86d6xsEXwJv3xvwVZLhYyJ5F8GPIBodLlWRMALtdL38Ghy+QnRPndac7Hw2rI7r9nL9QcROsXbCm0X9ga7Xw1ZVS41+bjmWFP6HL26AH889nue34TSPZy+f8uqJGntooJw5+vfVKNVWjCvESDaJjysVxcp9DluniuZZaz/v8tBt/Uk/z58U18s+xWu9CFZ8U+jee3W6q/8A8TDn28fEiSTPSYVv4r23X8DU5Fldrw7groP0JA/bqrTqoqctG7hEh0zqVO1Zst6+PJvRrk7RkWG+aL2D7aaz1RDO600wy1Oyc/8Am61hY0cFbKrPFPRYoqKaMLFrrfes+L8NI99Vpw5+0Sb6dOs8F0ymTfU1yHGwYer9orYyk63L3NmRZx+b37TpaO1IGXzf8NxrtZGuveF+0AI0GcnKW9fJrHl+G/D9wrTk8nYmgAK17axvoNnIW4vfon8zJ+qA/tb5B86fxDW37v4nuvdf7Go2J+mRJIfyPw0dWt71vzrft/YMK7JzrNQPhc6zs14xs3N14l+y/guuqneg/dVapIr2DRncC1e4E9pzv543NrUkzK/hyi6nTtnXivt+BdS9nfkRxVQDR3f7rNurCgcaBqnwA5xiGotn8V1uneg+7+Js5bDzf4D5pzxd3rd59iXTnK30Zy9uv7Jj7xndJK+l/Cc5N7iDO7yHPVamUyNg82hZz2Wc5bZ4gfenIt1yQIF5pV5r7yCfh4b4oy/fkp1IP9vfaW5cdnaKKopJVVzj3rVW1+PKLqQds6/3vGvw5KZkXttQfGfq7o351WfNXbfHDs4xqcLrvek4z9Jziap5ue/F1+mfNclajzehNG89zlqdFIHic7muT3rbLhyu44I70DrzTnu8HH2jjwtA5KEm9E5FqlLu6x5aXh4p/lpax5XXKHaI8ZeyDmo1DPZdThba8tu7vvJnN54a96zm5746mJ8yk5toiaA42BPM4XX9by0I8trORaNwd7F171kW9+OO0cf6T/8AVbW03IuexyoUku9ak7uVND1V3k0nuYDss8z063swlvZOaUNn7q0KGxMI88oAckGT10JOTsGGSSJfJonGfvV0P+fNOizzWHjevb+66t95PIaziFq+S3v/ACMqWUaqhy96aDYOs9uOkWD+bN5BT/VzWik7uXNJu7qc5FB3ZOV54jS6A8u5TAfzkOr1lFJRdIAh5dIYw+00oVlb1sFwVItBIlyoXc1YZ7PT9QPhT3N7MBO1eHuGxDsmTvaVBWd9ck32nf8AMRyBGmtj5daP3IB6e3L159maXOMe06Oyvl3vP+DJW5nPfaalzv54r6svetTZTl/ZlnpnXgFzuNHK57URWad1Ntuv1OrK8CibcgQqydHJObt3MI4y66LlmZzjaZ4k+rTXcFdhncRmCc/4gJUf+lZ7jLX90MjSSoU6O7ozjPMVUzRRvsmSa3O9Im3uOadJPR1FH3CsJemKrwVR2FyoqY+Eb/PtNay+KtzNuppNb0uXwXC9NEtFOjHtd8tVUmRw0QVTqfvoa7G3Pg1qKJa7utAgpjw1sq+84cddbym9zrDhrbM/UhrejDjpCeT6K3rWnDirY1rp1nwq1XQboHWlDGKkooURQyDRqbZ/chnZooUT7IJhx6g6NaAyJ1MQO9BtVXJjvxvYzm4S6lDTpyOub9NG9zx6ozi9NpSLKzX3orA08dTTU0P3HDiSS4oWtFad5Ed+F6CRNqZcVSvEvu0uEIcO9rtFc42mbFSeTazWHF7cWXDM83JDuKN+SdqOXChPJLJOt6NZ9yTRhOvqtr+6Jy+B3PWCifuz9qmijZnQ6l4v7tXuCHbAUm6BJMYrsW6hx4qOnTpU1sN4xWpb+nbrWnDjpG5N1IhrYZ8Ovol00du4OEV9FupowinvRuQ7d1MIpb6fqADezPuJs/dBNgprbhxduUDUV+pll134r6ti1nfUXvfX2TeaXp+E511NcO51fKSmkXLHzaJYb1OvtIZwcNNKLqs8PP8AeVlExhGjk/Wn/wCrd8AeXP8Ak0binGK2dINdtb0CK5pTdup2+Db43kR5mKfY34MB78fvWd+0V4csdRNJS/znxiPmfOl7OlNm8IQ3bozodhaDjFG7N7BTCHdsKVrD3jelMPPL1iA/zz3hrwf4XgJ5BNbO4iOzPflb2H+bGGmbR6D9Fm80najtQPs/uPe9M77nbv3M5YaLodyuLbXpdmp+He9pOWIdUUNNEoPVkFz1nJ3O47NPsIOd/Nf7+TLsZ98gTpYlHKKBmzUmt6NZqg3DjvxADLjKvwtUEU/cHO418VcgRpqhz/ie85Xjjh7TP6SYTvr1OjpfbTn/ABPecTtxRQkx+k5xCGh20/3Qps2SuE6ePkp0R/PPhrRdG5EM+D3jdaChzoWetNkta1tMONZdV0pKeXePE7nkQQceL8xbdwga2rjxui67kr4HK40Fh1WffogTK4aOgqtS127bc/jUFE4cSoQ6sfsmcUdaQGytImTHaCg71hxUktT1C140mRx1X5B0omQKTxL2t+w48dE9ethvCNw9nc5HCl7heoJ7/T7ufBN0xSZEbnvO3adhceKtl2bkypZZqXSI+EcuEvoNk1L74cXSg8vQVOlxO9RM1VaciO9lRFSH3XeRDVRMOraDxpzS/DinU1X2HPlqqeOoN1jPuSophx+eSgrQb3IY8VbHk+jK4aOnddI1nQ6LvTOEPTTDqwrHirvx7TZnw3fTfeUMONRb8BhwU0vG6s15Ezm3rRK+DP4oLkRtGbBcBSIt1FU+w48VahDq7utdzPg11KBShruaR2ZHFTviPLjxu6ivFHbuOXLLNGnuP8ERw4KEqRelx4n8zHahXwjjrT0xsy+GjoKrUtdu23PhVougZNhlcDtsKWHxpwB6pZpwh6yo+sZzj4qRm3UuRHR9Ivhx40Hp7UimlHPhJuPejNdmbyONSq1tJTzr3w4qVI8595TZ+zs70UaXWzI4QJIosUV3nF6XRbOwE6s+EWjOPE6noNMBvQOcHD0G+re6CtNa7Y8HhkDR3z4W64bilKKx4lJY8yHVoKz4Om/iqvu5Ub6+s1jwe8UO9FaWs+B2ipjo0iuxMj02pXk81orPg9qg8UG+yZzemKLI2TQNdsOPXR0ySG9u8eBs229h2Ameo4fijMeHyd8+B9jXR5yuHbtVZXZnweycyLKbx4O9fEfLqsjhBqztPL57UZ8Fd1xyz5awjp01W08uPCTa0JpduTw9LyV1thS04xvq3LOt7Ndqy4KIWCOjD01nHRWv6vXHXUbzk4TkmGTxLsw4foLzo7GzW9byOHvZJqL89qnHgXdcSp5vGGyKmt7DZhweHjJA84xc8nporzYr0d2cvh21tXrd1nD6bdanZ93Y9SueXkDt0VpO2gOzjwf8WhTe8Y69FCPbZhx6eUJ1ICusri6zVUO/JnqOA5PoNCOzHgE49yTpFSjJhOPVSBMzvckvcNuPB7UjvzkcOqCHfVveEJ0JJKCrnwPehNec+CnX8RV93KjfX1zrHh8MVvet6WvFYcHj4YBP1Z8D3ROnzjPUKFGk2Zy8BXHbyeTp2JrU6Ssjg6CGN6TTnwu1lCJl8TtOT+etmHD/AJs+W9oVEN5PDqdpqp2UPS07x4PJyP64T07DsOtv746K9wRETDh7wWyIrvZQ7EDGK2clG5HDipXknezKllBN9x7jjxur61J6X3VMuDyQ1p33Ez4VddE+UnzrCDzVVOzRvHi76XuacZZYqXSI+EyIaJ6lJ2njeTKlVJupH3oM+Df/AGVeLzSzDX07rwbwh6Vh1QVrWxwlUgBXI46oZv6K2GtOaVaUJAO6qhnwq6uud7kNb2ZHFVPQUS0gbAc6UGpN0CmthmteKTPu0L/IAFx4qHkaNz0/fZkxS7j6zLlmXi8NC5UJS26jQ4wzrVE6ZTZhDrkmHVM5UMlFGxFytteFzQMsa6pc/W5MYdXb4mfu4cVNfkQoTXkyIqgj3p0ZMbnVUblcYSbZN0Fa1sMs2bmV7mzOLjRjkp+jQu9mPHWzl+zhxU1NT3oTXsYR79eWaY4w3Sj4daP1ciPPG1Gx/leMOzko3I5UspUuk05MKcUroaTDipISeruhrTPcyuJGW9aUz4ddTya6tBjLPh09nAU8omdD3468Jlyyw6K7j/BHOhCODaTnQp0VvTlx7VFVnw2E6bTpRzorUlujHjoS71rDje3KZycKJJWmms5IJYa3lQ0cKB0j+rnFx7lZ0yzt8I5HitaSj9R/Z2OrDg+h4sOH8p8M1M63nwPPXvc+fwjg1xnJCvWxtFrJ9NqeXcM5ycGy+KkQo2KjJ3Ewj84ltNY8C6ZA0LWEdPR1O08uTxa5NHmGxNzXuI5fph5ONva99mwXDg/z/VnL6Yr80STuXfbYGcXF8Mvbsq9KNSdhAzm9PubgRO29iZPF0kEb1pHOhsiuo1SHfU5xFs+rE7rtKw4Nco9e9zRW5yuL2qWejzhHR5V8bcI6ji5JlnSYcH9TD0MskNPZyPTamqsBdFYcOlHPkjXJJ7UbzkjbyTSOPpj4klbO9DgaK0a3rblcTbTqjSlGjVYmvicl6/8AUAxjr7FD42ZXDueKu+5895VU8JnH6R+EVVRjG9r55M+B/vTpTz4pnGPaxOjz2DGd6KNLrZnDxMama27NqtZHp1OI0jsFcOPU8jZo7Ds6QNCuHDpm5oqeUd50MFvZuiVdC6w4foZYQ/yYcX5D4faZ1vCOlnqdpjG3UjO5dmlMiNVwJBJ3V3onHhXTxfre3efBrXp9RpVmmu85x70yU6oK7m817bURyOHTJKpXnIhPgzom3aqmpyQ1yQn0P8NkuE9+GfaA+058F8xHRoWnCOn6qaTW3PgqwRSjveHD2h49aEXHg/kz5yY7/UUOTw9HeGkry5ycXXPIOtKblEzk4Wi20V7VOIDd15dH742bNkr3Nma+i+Mh2K9pSxxh3yFupZPKOfDoq6newnOPjqtwOlNGfDYYR10ovdMOLtHGXUgG/KmPFXfjHTRnwq26nq2GtprDjrXw6dFZHHVHXRuZ2GtphFJVE9SDrW9Z8OhqJdKCZ8N10GPFWzkTZLnOVBoF7mt57G525cMjNfSmzvvHi/OUcbZkxSnT52a2JkQ0b5DcH6pjFatk3QOtbDJ4td52n5ayoZ7UBs+41nwq2WT1aT2WcrgRCl0rWHHTLyUbAcJdxUiuEr30vdPHY/t5neijS62ZBovYHXkel1XGSj1KK4w3x8xfmkpKFc4+HonvRSmEMg270m3Pg9uXjtaZZ37OVwq0VZW/OEuubc6N6oTT3DbnwPaWkp75UckNWSS8spuOxXmnL4miZZZ6O1Bo2ogOcXA9XLtHGPz+n4trM6ceFXcx0aKay5aNJpdCZPHrwaFFc4+HXFFw72Qqd3OP02ptqOjSbzo7c1ciq6E1pxjkh57jscgm5WT7uVHtUskefBhGu1byeHRJFlbRXapnF6Zn4m4Y0vVjwtMaVw4e0cku6P0rHh89QjOMFfEnSa37d9O/7mL2h+oYGjtSYZHFO5BTYGjCR1toayY2PxddNGPH+WXWxXHy8TRGRqqbbqfyGMbOJvDh+mqDQa7o48e+SYK1VE+6HcMiA5O0jUUdgoSsnj8QUy/zMuNNdf0awga6yer/ABnK7MG3ezCNrx26KM54Y667BhB8Qo13mZNs5c6W7BnHhR4xdGzL8QMbcZ1HKTrThwgpC7EHQmMaa4zP4VhG566NguXJCMv0GuyHgXOKOvQZPH2+HTnHAhyUbBVM6Dpr4bqsZ6cYK0ea0PYde7sMuQ3xTtOWpNZHtTIuXEzoqmSpQMY+qBJWcqAqyTanfSZHFs+EZ7s3lTppv6dZHHtOSDbLnJxjUz5TT2H+LsM+EFPFH0Vp7DQzn8XlIdZHDFvJa6nvQ52BrRt+x3wRzjhAKFRV2uEGym7nHj79XIbnGO88m9aTCO/VOTwruKrWXBLLPsa8h7LnwiCOTzEifcxgkhaSpQwjtMTzMNBlSTe0WpQD9u1vT99YSDc7Vl/A45N9eEHeJwJQeJGcZAl0GVxC8U66QM6JK46itzY/fDinzxuEHZjI/g70j5HwmMmlYITp8a1kcUgSqrhAdFcTuaMPTzqtmj9Ee44+eSMOGStS/hETFRO97EMrhioemenSJ3HL1WmXe8ZJJ6Z6cqTvFaAf0Jxie/xXbnQG4c6J8SqOMiQx7T9hxA027QxDSUaFPG5wg7SZy8ClgarlLlCaBTVDjwMX6aHwxe9U1QPYxg2m16371kxKoedj205fDAwUa0AazoBdGhQxgkp76aTPgw1MiaFTGBgL8yR4AyeOfC7XCZEIdlGdAEfD3oJyOIkpqWcmCtg70j2Rx45NFGk0Gs6CmmbK/wAXAA5GR0P6VqsTY5MhjxgHQIZCde91TRhE964sINxexzRq2jTLjB4KXGQ01jwxPW35WgypDpid6wklm2+vCALiCgjXsLSr+/Fx1kv3TZvGSXkCTr8aEK2D+HQEsO9KH26Lw4vylXoF8AbyY7DANGB5ms0dFOhSXffXh+zn3ubkOk8tOMd+GadDWELq6Bn+TjHuTWxMOJahh1QmEl/DORTtLkun9RxBmO6BSZfago2PZRHOKOt6eoneTO5jko7C55mCV70mXGmoazoenrTYby59oqklQO5OfxUkxj8/TDqqyIWXjvxWXCU1YMB+uEbvqieqjL+zLTnRuum3U1nLHS9nzr7Ps58LRxcIdq3531aNrp/CY2xOa7R1xCYz45JFzk4kGodUZJt7oZcovV9Ov1exj9zj6jHg29Hc6plw43W70TvHhdFQ6Zz4fc5cs1RU0zQmfCF5baxj6yukqt+RG8mXey5kVw9PITwngaxNjyX9AnlAFQTGCD4ulZxnpBpqaEMI7RHxWGwxgilTdSyBn60mPG7eNU3OMJVPJ9GjPhfnbgWwMOPaQeacfENdeHEW1ficPBSGzIg2ASmHE7HidWJhHZmQUw416ZpAXJ4NnEzO8TTWwdp+zxpqd+5k8MQEzsZ0Hh/DXcm1XGBIeN8ye3VkwSs37UnnRnRPicIJmL0DU/rraYQblUosryIgmXwynIxvVa9nCDswAThB5oRlM7a5b5fNa8DOHFIhxZrw/fWEB8YHcU+xU5HFMePvrIgalK3ua8yp2cvim2b0Ci4QHzE1vS/apzonZOxVPCvSYyIMmuk+0uMB1NiJjBQgijL2ROzlwXLNVvQPgHJ4p1JKuaNRXHk8EaVNFHvNT7I59+k0VvJ4JK1FFG/uqfg8MeA0T1edGHkEwg13/wB8PBWfCnzxkgfokYyOqZZZxgOt5M4p3W950EvH8PvOj+DnHwzJTUs4z3tpUpzQzx1xI6nftTkAMtX1UZ0T5DXTiBputphwzYbdtC+Fz2rkjWnPhmmWhe+OtRIqh+q7ce9ca9pNPvMk48ZHnzWzurlSBMzvWEAxZbeISBJoAMA8baEfIi4wLXderDhH4XRX1gd6TOP0t94YVajfWNNIo5xQdEioWS4yAdAgjhxTZSmhR9zEBp91DsZHvHYJvHint8VGsJPzzQGdE+zsvOgOhqCHE0ujX7563pzlI0eozlgWWmyhDWTB1sfFYemfDWcxrlNyNCaH91jOWp93a7JFcrShIkga122uKFRRTQiGXrb0mpAAAD+641sSVM5NUxRHUIgZPRTrkrUpqsonU8ibPFOc9kmpi1l12QZxIZokH82lR0iZqWmeSukQ37OWG96ERFERzjBSqdGxTLZHqkLXbWtdOEz4h1Uu6NUZ9Ky5Qb5S3plO/lrIJWbZ6juVpEz1HSTHXWAJHU6N9+6PnW8tKmHRUzlE/lrjUStLiTuuMUc5ANzQomlzg6VN0BvfsqDmhDk1szl6dSkjqde2kca1DJx1WqzUbmZ7qvVradwyZPHKblwJ7Wgg7UZR/DU+ZrTOt7mnyFBmpaLoEe1aZcsnU8kCo6pyyddcLs7UvfTrJI6+hU3O1BMLOrdFNMlIZqelvTQed6QTes5Q8SgnZc+wG3K7ru0EkUHNSu78PZy9bKn9FMa1KHHSZqOqIPK99ZJKf1oM0G9pgT2v2K2u4r8O2viRvZveSBQTXStKgZPiN3YulM49a4praLtHNSz10CP4Ni8HDNdmtu9Z23fEZRHTyBJXfuuUBubtkwJ2TPl3vXcNmnCD6OT6XOzSoMpp1pHNTueQMrQTXGpQ981O4KdC9/w6xeHiJlV2iSOGn4sR5TKDdxTrZpc7beQnbjpuSbdSq4mnSb/Z48hRpytBxymlNB3cJgdxRRvU527WABlks1ViUOdhJszUbs472ZetS6J7aM4tbmZra90x0rVBKJrWtABmpBm0Vzt9BnYYON3OsCZFBnSB4057T0OxM/StiCaFc/L3vNnnkHZm52wuXrWo3p7Bka3uaKDuOdtdZl61KgaM1LN7llKEcux/Lf8ApOw527fC+nO31yAH4CFdPX1aUDeBIFxnbvVCM5uezbWkZ33JvPy95M7b4nj8M46UrXTr9Mv3KRzws0acKCugd7EOzkkgVx61l67KA60Galm+qWUoRy0dRefk88QE52+s/Dt3q97zttm66ke2nTgnaBXBNcpOGu1RJIH4e6cn1w9u8uakqZzYO6kk1moS5mlNjOeyXn5fHF9OSk94AA0eEAcOn63G5NLasaNPcclNQTWxkR/DRs2aqPHeUzt7iGNBUTKUz27O07521FsdCBrwmNGl5MrW9H74/ZJXLjXJ8Sn/AHTQs5qGp5E2eKwJ3NSb7u9aTPe7bkgBddWOtxxz3oUU7ujsuTr6GJWc1O45Hw1qsvW+qXTrSmWHfglK7AaNxU5BKGgFHt2a33XPFlcdgpKs7MsBRB2h2HGDfRup7h2enoVzR3lyCdBYsves1E3MyptRAd5XvbOc3FIl+18b4ZMvXm5aKlAz25omWqD3HDXjomgF0ZBO4mK1S7Q7Odn4RfjZvb93W8iIWGHT5pMNbHXn7bxgK56s2eT6TNSqcldMoFZ0TuZHW/OkfOhXCRkOX6XDoJbk3XmhxmT4ky6dd9jjM7eKlBMglmqo3od+xra6MJNlceyhzljqY2e8ibT7b/Dt5sGUxk18UGvas40f99gXLQEJprL1uj2o0vZyal6mV911ihUPHRLmp7nH9WWGriv0XSPkciROmlMufau8ppcOh1TLZ3K0iZyQeeN1culxmd1x0vcN5zaDXImkRyQTjbok3376Xvredu1WCVnGCjToUUxJ3N9PXO+/ip7mOhh48JlC4/Rz1CHdBERREc4wZeOhSjDXnkBlzi1ShjME8pBKu97HvmjdcTkaZpvuA79juroDOQCippEdKZ4jmJi85iShmkQ12Q/Z48hRpydSzE0OQSfCA1Wdk5as0tTrXY7Ge5OxKENlGXrdh3qnXbbWdjoqQAztt5Jy9bGnaGgz+AJnZ+Jr2s1pHNT45K25XlJNGOuuDlVrDW6487aCBJQDO2646prHtWg0OXrZNeadHdcoAntreg7uAarqnpd7M+1UaoDwjjwzZdqaip3ORqpak1s2Oe407cNb8a3rxk63fH9nO3f4d9c1hpnzvYI6rO3b4XjL4TtxdOm5srY1l6aubdrSm1M3IfCF74O/i0e1b8mOu7yK0YAp/J7ZYCGGu18egzt9ekztvjvr68dI3rTPjxl9+gXZI/Y8Gdnput9R3O8uSAVN62JrFN28p7ucujokydakmlEzx0seGcnjmRrTPcDP/m+c7bQVKzsaePwmdnoYooZw0hcZx63MzYtd0z3aJ6Q/Tpx0r1q1OQk1DXklDECRJDsBk61em9mRrtXFrpw09NYa0lgL3M/IdXG1g7aR7S/cDRla7NO0NBnZDrKKnL0I07Q0H748YNaUOwplRJ1kAuu/uVhE9uPOnvyRW9Mg/hyAVNS6R0pnHomVudj327lw1qLv6R2jnLAC8bqjsuMzqoKZ2BWanzyfR4c5NbLl0nZTJTdcjyVOoFNrhpOFvwV33+rI5JHeJUakawidkaFa7hkapsYKOwqr7AZqVLqdy9q0ymcgFFTTNDpTyZJO+TrdHQNZRPTVSdSHfecszpnjdX4XOXo1q+8+KXOMEEGu+01swAqOiumt7QyJO0Vetoo9nyApnL0TcMFKGkHTOflGZQUjOWuqn9X3c934ldMZx6qr6hcsketOo79XTpnuLWck6qd+yZZOp5cCdcxG96N7P5hiAc8Xx3WTJ1ywojtyddSXK4xIhSA0bD9Qxk6TkD8L0ovXtklc1KlVO5eznIBRU0iOlMmpaqmrHoFM7ai68S5ETqWZ3sWsYCW7NzvNCLFaZ0KlfYQUzUqPJ3h7OXIBySKilOWTpeN1RspzUrxkOnYUu17AbzlDxRuURTODzEmwpVMCdUPlPDvGPMU6DQrvOmWkAVO+sgjvxv60ZCbrrL2wKZqejrZaDzvuCbBM5YASXVHZc2HaqBzszx8leJe+8iIelmiXyphMpvk10uaGiu32U1pHe85AKmppEdKfs8+TSOTr8xQDvO3eKydCEOEyBcgaO3hygNNO0MnSFTjE0NATsU2bA3iSb+I7ocmCOXgapSZopENvicmTzxfTj7NZGh4maam5dbKHGJeqpCdnbspnwSb4SlZmaKSjP/6sNaLyEllmSSpQ2bDCJDriejL+9U0mWjPP4ZoB8TusrT9Ussqm9Bl6Dk+J3SseE47eQ7I6p2Bk6SBijOy8nXXVTQms7bKlXZWtiudu1Vsqc7drkAM5NdXelDsHY8GMnXGwNDkaFY8I62bx4pqV6elnSa1lu1DLJ1f2zolZms7btmWTeOtU8qtKa1sXO3eYEms7diHYmanvetZDpyNSyTvSYROuqJ0ZX3qmnIDfCzSly4wVuiSVFNmwzsaqp6c7JNRIdMqdhydRWl3tZDdHgXJidNcQk5271QjOdht5FUcKC/TlHclNg7VxANwIVka0wq4gbgz7RLuXNSTXUBtNa2ezmpydb4Ti+lnHTqiddJ9pyg/MXQuT5GaKMqZd0AKKbNh3ztsarqcA88efxAOhA7yhlAaadoaD98ZzjJY5Xy9L1eDwro3kQdepdV2Xuj7GaBN3Aya3sSs5AKKAfZREREf3WnIA429ar+VPdnNSUPXsZrW5TCTsyzqf8mGgpY6dP92HH5SUaMPLyxrafq3jc7aqid90XSm87N7lRGd7Fyeaamp1vr6h0Tn3qOQnsZFym+Q3NZTOp5IFzsk3GXZvvW0N51+9FbTbqTC5Y6oDe8eU3xTPJZItZ94c3PaeR0VghNTYyNfpWRoqPT33/wAJisanpGQRO+0cEJZsZGv0rI7VHBSv+BFY3s4iiCf3G+4mnG/fOz0sO5TY42Fk732oM9rtNOzWkw5fEW7rI6ZGuM1LonO3fkoRwo0XQjk+0rvZsdObPPH9ObnRVmqxvvZyK1NY2L8IwqfHG7nDPYlRwsKiq7LKGDqkM9iVxs6XkP70r7PNJdSEutMmCLUx/jKmdl5JrYaws1e+Kqy9JqaBEHY5EF8X8Yv7NORYJyQK9h2GxBworfKhuPO1wv2qwEqc2eZNvYdgnhy6HrIr90NnV+dWpe2kc3OpxoLmZdkmjJ6Sesjo8E5aPWVRTt1txs6FnWjxsOwoOGtDW+rSigquNB1k/RToPzz7Obkrpl2Gbna2JQutplp4pF/++jFMokqImPPeqvHmt+MR+fJtjxm7urZ8Cm0KrOLmYmFs3nLz0nFu9ZfNb8VlqiXOWl6DQs56Pm7ejr364zgqxuXPV8qXw+m8QPlb/Bpv4XLFa0KujV5y8+r5Dr7hWeo9Tc36Pk1/Prh/cu/AV2c4LeO6jIvUg5wX0TPG5fNvUHgnPjZw8zFsPkXOO8nxd4cycXMz2nc4c3i6zhtjr413qsvxMiucl7qLoZqoc+Ydx33jzLd8h4V/uihZRIcq6/ruNIYpPFIKOHLa2a5HOSuq542lkp+4Zf0xTdnYxrc2TCqe2itaz4p08XZSSRzm9TfIPLRsWaU3JjfWxdVUqLvWycXd6LoMK/rLjq/PnKqysboFVzk8RVF5z31y8hBqvKGry+VamxkLFXVZy22xvs8oq+cOVahooQpV76zlpt4TpPdXZWcvK0cnKoh9tzkUmrW3eSpO6Cq2eHas5MUfHUV/WTE0n7iCm5TSbMaWiHWzPjU9K5t7yKhrx7uPNWmRUNb1oXLpa1vejfg37GjG6Z4l89Mro3jt62jTvG6a0GpCl2AeAz41KK7UV2bceWlk+wrszytPlV7q481IhvRpdZVNUzvZIvg/gdsq1q50mml3oHtpzlurbkNAtK6DHkpu9AG631dg7d8u6uqda2tKvYDK9RdyNO1CqTCmeqd76a0mzCkm+l3PUDqtPc3hdTqjw9k//mJf/8QAFBEBAAAAAAAAAAAAAAAAAAAA0P/aAAgBAgEBPwAvy//EABQRAQAAAAAAAAAAAAAAAAAAAND/2gAIAQMBAT8AL8v/2Q==" style="width: 720px;" data-filename="Contoh-Pengumuman-Libur-Sekolah.jpg"></p><p style="text-align: left;"><br></p><p style="text-align: left;">Dimohon untuk membaca pengumuman ini</p>                    ', 41, CAST(N'2021-09-01T09:46:12.087' AS DateTime), N'Kelinci.png', 1, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_Pengumuman] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Pertanyaan] ON 

INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1013, N'Proses dari menghirup oksigen kemudian mengeluarkan CO2 disebut sebagai ?', N'Pencernaan', N'Pendarahan', N'Pernafasan', N'Pengeluaran', N'C', 19)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1014, N'Alat pernapasan dari manusia yang ada di dalam rongga dada yaitu ?', N'Mulut', N'Hidung', N'Kerongkongan', N'Paru-paru', N'D', 19)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1015, N'Fungsi dari akar yaitu, kecuali ?', N'Menyerap air dari udara', N'Menyerap air dari dalam tanah', N'Menopang bunga', N'Melindungi buah', N'A', 19)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1016, N'Berikut adalah indera yang dimiliki manusia, kecuali ?', N'Penglihatan', N'Perasa', N'Pendengar', N'Peraba', N'B', 19)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1017, N'Perubahan dari cair ke padat adalah?', N'Menyublim', N'Mencair', N'Membeku', N'Memuai', N'C', 19)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1020, N'2+2 = ?', N'1', N'2', N'3', N'4', N'D', 1023)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1021, N'1 + 3 x 2 = ?', N'5', N'6', N'7', N'8', N'C', 1023)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1022, N'2 + 4 / 2 = ?', N'4', N'3', N'2', N'1', N'A', 1023)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1023, N'6 + 3 = ?', N'8', N'9', N'10', N'11', N'B', 1023)
INSERT [dbo].[Tbl_Pertanyaan] ([Pk_Id_Pertanyaan], [Pertanyaan], [OptA], [OptB], [OptC], [OptD], [CorrectOpt], [Fk_Id_Kategori]) VALUES (1024, N'8/2 =?', N'4', N'3', N'2', N'1', N'A', 1023)
SET IDENTITY_INSERT [dbo].[Tbl_Pertanyaan] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_QuizNilai] ON 

INSERT [dbo].[Tbl_QuizNilai] ([Pk_Id_QuizNilai], [Tanggal], [NilaiQuiz], [Fk_Id_Kategori], [Fk_Id_Siswa], [Eval_Per_Hari]) VALUES (11, CAST(N'2021-08-30T20:41:21.343' AS DateTime), 80, 1023, 4, NULL)
INSERT [dbo].[Tbl_QuizNilai] ([Pk_Id_QuizNilai], [Tanggal], [NilaiQuiz], [Fk_Id_Kategori], [Fk_Id_Siswa], [Eval_Per_Hari]) VALUES (12, CAST(N'2021-08-30T20:42:30.947' AS DateTime), 80, 19, 4, NULL)
INSERT [dbo].[Tbl_QuizNilai] ([Pk_Id_QuizNilai], [Tanggal], [NilaiQuiz], [Fk_Id_Kategori], [Fk_Id_Siswa], [Eval_Per_Hari]) VALUES (13, CAST(N'2021-08-31T10:01:39.990' AS DateTime), 100, 19, 3, NULL)
INSERT [dbo].[Tbl_QuizNilai] ([Pk_Id_QuizNilai], [Tanggal], [NilaiQuiz], [Fk_Id_Kategori], [Fk_Id_Siswa], [Eval_Per_Hari]) VALUES (14, CAST(N'2021-09-01T09:56:38.163' AS DateTime), 80, 19, 3, NULL)
INSERT [dbo].[Tbl_QuizNilai] ([Pk_Id_QuizNilai], [Tanggal], [NilaiQuiz], [Fk_Id_Kategori], [Fk_Id_Siswa], [Eval_Per_Hari]) VALUES (15, CAST(N'2021-09-01T10:27:55.840' AS DateTime), 100, 19, 3, NULL)
INSERT [dbo].[Tbl_QuizNilai] ([Pk_Id_QuizNilai], [Tanggal], [NilaiQuiz], [Fk_Id_Kategori], [Fk_Id_Siswa], [Eval_Per_Hari]) VALUES (23, CAST(N'2021-09-01T15:11:25.027' AS DateTime), 100, 19, 9, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_QuizNilai] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Role] ON 

INSERT [dbo].[Tbl_Role] ([Pk_Id_Role], [Nama_Role]) VALUES (1, N'Super Admin')
INSERT [dbo].[Tbl_Role] ([Pk_Id_Role], [Nama_Role]) VALUES (2, N'Admin Sekolah')
INSERT [dbo].[Tbl_Role] ([Pk_Id_Role], [Nama_Role]) VALUES (3, N'Guru')
INSERT [dbo].[Tbl_Role] ([Pk_Id_Role], [Nama_Role]) VALUES (4, N'Siswa')
SET IDENTITY_INSERT [dbo].[Tbl_Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Sekolah] ON 

INSERT [dbo].[Tbl_Sekolah] ([Pk_Id_Sekolah], [Nama_Sekolah], [Logo], [No_Telp], [Email], [Alamat], [Password], [Role], [Activated], [ActivationCode], [ResetPasswordCode], [Tanggal_Registrasi]) VALUES (41, N'SDN Imam', N'41_sekolah.jpeg', N'081623554125', N'ahmadimamfadhila@gmail.com', N'Jakarta Barat', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 2, 1, N'c898a25f-f7cc-46e6-a3a0-017d9f675966', N'bd2998ae-ace2-46fc-b754-d89de0e3044c', CAST(N'2021-07-26T01:08:30.463' AS DateTime))
INSERT [dbo].[Tbl_Sekolah] ([Pk_Id_Sekolah], [Nama_Sekolah], [Logo], [No_Telp], [Email], [Alamat], [Password], [Role], [Activated], [ActivationCode], [ResetPasswordCode], [Tanggal_Registrasi]) VALUES (43, N'WiaClass', N'default.jpg', N'1', N'wiaclassteam@gmail.com', N'Tangerang', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 1, 1, N'00000000-0000-0000-0000-000000000000', NULL, CAST(N'2021-08-26T01:08:30.463' AS DateTime))
INSERT [dbo].[Tbl_Sekolah] ([Pk_Id_Sekolah], [Nama_Sekolah], [Logo], [No_Telp], [Email], [Alamat], [Password], [Role], [Activated], [ActivationCode], [ResetPasswordCode], [Tanggal_Registrasi]) VALUES (44, N'WiaSchool', N'default.jpg', N'0212371675', N'wiaschoolteam@gmail.com', N'Tangerang', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 2, 1, N'00000000-0000-0000-0000-000000000000', NULL, CAST(N'2021-08-26T01:08:30.463' AS DateTime))
INSERT [dbo].[Tbl_Sekolah] ([Pk_Id_Sekolah], [Nama_Sekolah], [Logo], [No_Telp], [Email], [Alamat], [Password], [Role], [Activated], [ActivationCode], [ResetPasswordCode], [Tanggal_Registrasi]) VALUES (45, N'SD 01 Jakarta', N'default.jpg', N'08126312536', N'sd01@gmail.com', N'Jakarta Barat', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 2, 1, N'00000000-0000-0000-0000-000000000000', NULL, CAST(N'2021-07-26T01:08:30.463' AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_Sekolah] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Siswa] ON 

INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (3, N'846562346123', N'daniel hendra', N'daniel@daniel.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 41, 13, 1, 4, 1, N'3_siswa.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (4, N'66663543', N'Elang', N'elang@elang.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 41, 13, 1, 4, 1, N'default.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (5, N'2346346', N'Fatur', N'fatur@fatur.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 41, 4, 1, 4, 1, N'default.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (6, N'84673658348', N'Henry', N'henry@henry.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 41, 6, 1, 4, 1, N'default.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (8, N'9128371284712', N'ilman adi', N'ilman@ilman.com', N'CjkpKUbzyB0IPmKhN2wdIDbYvL1Z4xgg5ny9nde84b0=', 41, 4, 1, 4, 1, N'default.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (9, N'82374623674', N'Lisa', N'lisa@lisa.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 41, 13, 2, 4, 1, N'9_siswi.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (10, N'078234662347', N'Okta', N'okta@okta.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 41, 5, 1, 4, 1, NULL)
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (12, N'02347283748239', N'Pamungkas', N'pamungkas@pamungkas.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 45, 20, 1, 4, 1, N'default.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (13, N'234985728347', N'Rara', N'rara@rara.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 45, 21, 2, 4, 1, N'default.jpg')
INSERT [dbo].[Tbl_Siswa] ([Pk_Id_Siswa], [Nomor_Induk], [Nama], [Email], [Password], [Fk_Id_Sekolah], [Fk_Id_Kelas], [Fk_Id_Jenis_Kelamin], [Role], [Activated], [Foto_Profil]) VALUES (14, N'4673658348', N'WiaStudent', N'wiastudent@wiastudent.com', N'a3iKR0YI6dvkg0dKnsweAKYG9JvcBo3Jl3/UtQJcHC8=', 44, 27, 1, 4, 1, N'default.jpg')
SET IDENTITY_INSERT [dbo].[Tbl_Siswa] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Testimonial] ON 

INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1, N'I Made Agung', N'default.jpg', N'Siswa', N'TERIMA KASIH WIA CLASS, MEMPERMUDAH BELAJAR', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (2, N'Solihin, S.Pd', N'default.jpg', N'Guru', N'Dengan Wia Class saya menjadi lebih mudah mengajar murid-murid saya yang masih SD karena tombol-tombolnya mudah dimengerti', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (3, N'Erika Putri', N'default.jpg', N'Siswa', N'Lebih mudah daripada menggunakan banyak aplikasi', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (4, N'Eti Haryani, S.Pd', N'default.jpg', N'Guru', N'Aplikasi yang sangat simpel namun mempermudah dalam mengajar', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (6, N'daniel', N'profile1.png', N'Siswa', N'Sangat membantu saya untuk belajar sehari-hari, terima kasih WiaClass', 0)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1005, N'Dadang', N'8_hungry_sumo_logo.jpg', N'Guru', N'Sangat membantu untuk kegiatan kelas virtual', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1006, N'daniel', N'3_apple.png', N'Siswa', N'Membantu saya dalam belajar selama pandemi', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1007, N'daniel', N'3_apple.png', N'Siswa', N'Sangat membantu saya belajar sehari-hari selama pandemi, terima kasih', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1008, N'daniel', N'3_apple.png', N'Siswa', N'Terima kasih sangat membantu saya dalam belajar selama pandemi!', 0)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1009, N'daniel', N'3_apple.png', N'Siswa', N'Sangat bagus saya menyukai aplikasi ini!', 0)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1010, N'daniel hendra', N'3_apple.png', N'Siswa', N'Testimonial 30-08-2021', 0)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1011, N'daniel hendra', N'3_apple.png', N'Siswa', N'Sangat membantu, terima kasih wiaclass', 1)
INSERT [dbo].[Tbl_Testimonial] ([Pk_Id_Testimonial], [Nama], [Foto], [Posisi_Role], [Testimoni], [Status]) VALUES (1012, N'Lisa', N'default.jpg', N'Siswa', N'Terima kasih wiaclass membantu saya dalam belajar online !', 0)
SET IDENTITY_INSERT [dbo].[Tbl_Testimonial] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Tugas] ON 

INSERT [dbo].[Tbl_Tugas] ([Id_Tugas], [Nama_Tugas], [Waktu_Pemberian], [Batas_Pengumpulan], [Fk_Id_Kelas], [Fk_Id_Guru], [Fk_Id_Sekolah]) VALUES (1, N'Tugas LKS', CAST(N'2021-08-15T00:00:00.000' AS DateTime), CAST(N'2021-08-16T00:00:00.000' AS DateTime), 13, 6, 41)
INSERT [dbo].[Tbl_Tugas] ([Id_Tugas], [Nama_Tugas], [Waktu_Pemberian], [Batas_Pengumpulan], [Fk_Id_Kelas], [Fk_Id_Guru], [Fk_Id_Sekolah]) VALUES (3, N'Tugas latihan mengerjakan buku cetak ipa halaman 45', CAST(N'2021-08-20T00:00:00.000' AS DateTime), CAST(N'2021-08-20T00:00:00.000' AS DateTime), 13, 8, 41)
INSERT [dbo].[Tbl_Tugas] ([Id_Tugas], [Nama_Tugas], [Waktu_Pemberian], [Batas_Pengumpulan], [Fk_Id_Kelas], [Fk_Id_Guru], [Fk_Id_Sekolah]) VALUES (1003, N'Mengerjakan latihan LKS halaman 32', CAST(N'2021-08-24T00:00:00.000' AS DateTime), CAST(N'2021-08-24T00:00:00.000' AS DateTime), 13, 8, 41)
INSERT [dbo].[Tbl_Tugas] ([Id_Tugas], [Nama_Tugas], [Waktu_Pemberian], [Batas_Pengumpulan], [Fk_Id_Kelas], [Fk_Id_Guru], [Fk_Id_Sekolah]) VALUES (1004, N'Mengerjakan latihan UTS buku cetak halaman 50', CAST(N'2021-08-25T00:00:00.000' AS DateTime), CAST(N'2021-08-25T00:00:00.000' AS DateTime), 13, 8, 41)
INSERT [dbo].[Tbl_Tugas] ([Id_Tugas], [Nama_Tugas], [Waktu_Pemberian], [Batas_Pengumpulan], [Fk_Id_Kelas], [Fk_Id_Guru], [Fk_Id_Sekolah]) VALUES (1005, N'Latihan sebelum UAS buku cetak halaman 90', CAST(N'2021-08-26T00:00:00.000' AS DateTime), CAST(N'2021-08-26T00:00:00.000' AS DateTime), 13, 8, 41)
SET IDENTITY_INSERT [dbo].[Tbl_Tugas] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Tbl_Seko__A9D105345E28E0EC]    Script Date: 04/09/2021 15.25.07 ******/
ALTER TABLE [dbo].[Tbl_Sekolah] ADD UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_Guru] ADD  DEFAULT ((1)) FOR [Activated]
GO
ALTER TABLE [dbo].[Tbl_Sekolah] ADD  DEFAULT ((0)) FOR [Activated]
GO
ALTER TABLE [dbo].[Tbl_Sekolah] ADD  DEFAULT (NULL) FOR [ResetPasswordCode]
GO
ALTER TABLE [dbo].[Tbl_Siswa] ADD  DEFAULT ((1)) FOR [Activated]
GO
ALTER TABLE [dbo].[Tbl_Guru]  WITH CHECK ADD FOREIGN KEY([Fk_Id_Jenis_Kelamin])
REFERENCES [dbo].[Tbl_Jenis_Kelamin] ([Pk_Id_Jenis_Kelamin])
GO
ALTER TABLE [dbo].[Tbl_Pertanyaan]  WITH CHECK ADD FOREIGN KEY([Fk_Id_Kategori])
REFERENCES [dbo].[Tbl_Kategori] ([Pk_Id_Kategori])
GO
ALTER TABLE [dbo].[Tbl_Siswa]  WITH CHECK ADD FOREIGN KEY([Fk_Id_Jenis_Kelamin])
REFERENCES [dbo].[Tbl_Jenis_Kelamin] ([Pk_Id_Jenis_Kelamin])
GO
