﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace WIAService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                tmrExecutor.Elapsed += new ElapsedEventHandler(Working); // adding Event
                tmrExecutor.Interval = 10000; // Set your time here 1 sec = 1000 
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Working(object sender, System.Timers.ElapsedEventArgs e)
        {
            WiaClassEntities db = new WiaClassEntities();

            //Do your work here
            List<Tbl_Sekolah> sekolah = db.Tbl_Sekolah.Where(x => x.Activated == false).ToList();

            foreach(var item in sekolah as List<WIAService.Tbl_Sekolah>)
            {               
                var toMail = new MailAddress(item.Email);
                var fromMail = new MailAddress("wiaclass@gmail.com", "WIA Class");
                string subject = "Reminder Aktivasi Akun";
                string body = "Halo Sahabat WIA Class";
                body += "<br /><br /> Selamat datang di WIA Class! Terima kasih sudah mendaftar di WIA Class. Kami ingin memberitahukan, jangan lupa aktivasi akun anda ya :)";
                body += "<br /><br /> WIA Class Team";

                var wiaEmail = ConfigurationManager.AppSettings["wiaEmail"];
                var wiaPassword = ConfigurationManager.AppSettings["wiaPassword"];

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(wiaEmail, wiaPassword)

                };

                using (var message = new MailMessage(fromMail, toMail)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                smtp.Send(message);
                Thread.Sleep(ScheduleTime * 20 * 1000);
            }
            
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
